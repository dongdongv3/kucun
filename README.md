# 库存管理系统源码

#### 介绍
库存管理系统源码,使用到net6 asp.net api,efcore,ids4,react,antd等技术

#### 功能
1 库位管理
2 商品管理
3 出库和出库类型管理
4 入库和入库类型管理
5 授权模型：自动角色权限模型


#### 安装教程

本目录所有代码文件均来自[PTBlack](https://www.ptblack.cloud)生成，
如需访问此项目***预览地址***请跳转至 [模板库](https://www.ptblack.cloud/AppGroup/AppTemplateListPage)

