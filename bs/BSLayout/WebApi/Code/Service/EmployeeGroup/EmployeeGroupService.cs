using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.EmployeeGroup;
using WebApi.Code.Interface.EmployeeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.EmployeeGroup
{
  public class EmployeeGroupService : IEmployeeGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public EmployeeGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1AddEmployee(Employee_AccountAndPasswordAuth1AddEmployeeRequest request, UserInfo loginUser)
    {
      var employee_AccountAndPasswordAuth1 = new Employee_AccountAndPasswordAuth1
      {
        Id = Guid.NewGuid(),
        LoginID = request.LoginID,
        Pwd = request.Pwd,
        RefEmployeeId = request.PageParament.RefEmployeeId,
      };
      _inventoryManagementSystemDB.Add(employee_AccountAndPasswordAuth1);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_AccountAndPasswordAuth1AddEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue(Employee_AccountAndPasswordAuth1AddEmployeePageparameter request, UserInfo loginUser)
    {
      return new Employee_AccountAndPasswordAuth1AddEmployeeDefaultValueResponse
      {
        LoginID = default(string?),
        Pwd = default(string?),
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1AddEmployeeRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_AccountAndPasswordAuth1EditEmployeeResponse Employee_AccountAndPasswordAuth1EditEmployee_GetValue(Employee_AccountAndPasswordAuth1EditEmployeeQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_AccountAndPasswordAuth1EditEmployeeResponse
      {
        Id = entity.Id,
        LoginID = entity.LoginID,
        Pwd = entity.Pwd,
        RefEmployeeId = entity.RefEmployeeId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1EditEmployee(Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_AccountAndPasswordAuth1 = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.LoginID = request.LoginID;
      one.Pwd = request.Pwd;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1Delete(Employee_AccountAndPasswordAuth1DeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_AccountAndPasswordAuth1 = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_AccountAndPasswordAuth1DetailResponse Employee_AccountAndPasswordAuth1Detail_GetValue(Employee_AccountAndPasswordAuth1DetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_AccountAndPasswordAuth1DetailResponse
      {
        Id = entity.Id,
        LoginID = entity.LoginID,
        Pwd = entity.Pwd,
        RefEmployeeId = entity.RefEmployeeId,
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1DetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_AccountAndPasswordAuth1SearchEmployeeResponse> Employee_AccountAndPasswordAuth1SearchEmployee(Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_AccountAndPasswordAuth1_LoginID";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_AccountAndPasswordAuth1_LoginID", "Employee_AccountAndPasswordAuth1_Pwd", "Employee_AccountAndPasswordAuth1_Id", "Employee_AccountAndPasswordAuth1_RefEmployeeId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_AccountAndPasswordAuth1 in _inventoryManagementSystemDB.Employee_AccountAndPasswordAuth1
      select new { Employee_AccountAndPasswordAuth1 };

      // where
      if (request.PageParament.RefEmployeeId != null)
      {
        select = select.Where(x => x.Employee_AccountAndPasswordAuth1.RefEmployeeId == request.PageParament.RefEmployeeId);
      }

      // query
      var queryoutput = select.Select(x => new Employee_AccountAndPasswordAuth1SearchEmployeeResponse
      {
        Employee_AccountAndPasswordAuth1_LoginID = x.Employee_AccountAndPasswordAuth1.LoginID,
        Employee_AccountAndPasswordAuth1_Pwd = x.Employee_AccountAndPasswordAuth1.Pwd,
        Employee_AccountAndPasswordAuth1_Id = x.Employee_AccountAndPasswordAuth1.Id,
        Employee_AccountAndPasswordAuth1_RefEmployeeId = x.Employee_AccountAndPasswordAuth1.RefEmployeeId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_AccountAndPasswordAuth1SearchEmployeeResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_AccountAndPasswordAuth1SearchEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue(Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request, UserInfo loginUser)
    {
      return new Employee_AccountAndPasswordAuth1SearchEmployeeDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource(Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource(Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_UserRoleMapAddEmployee(Employee_UserRoleMapAddEmployeeRequest request, UserInfo loginUser)
    {
      var employee_UserRoleMap = new Employee_UserRoleMap
      {
        Id = Guid.NewGuid(),
        RefEmployeeId = request.PageParament.RefEmployeeId,
        RefEmployee_RoleId = request.RefEmployee_RoleId,
      };
      _inventoryManagementSystemDB.Add(employee_UserRoleMap);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_UserRoleMapAddEmployeeDefaultValueResponse Employee_UserRoleMapAddEmployee_GetDefaultValue(Employee_UserRoleMapAddEmployeePageparameter request, UserInfo loginUser)
    {
      return new Employee_UserRoleMapAddEmployeeDefaultValueResponse
      {
        RefEmployee_RoleId = default(Guid),
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource(Employee_UserRoleMapAddEmployeeRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource(Employee_UserRoleMapAddEmployeeRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_UserRoleMapAddEmployee_Role(Employee_UserRoleMapAddEmployee_RoleRequest request, UserInfo loginUser)
    {
      var employee_UserRoleMap = new Employee_UserRoleMap
      {
        Id = Guid.NewGuid(),
        RefEmployeeId = request.RefEmployeeId,
        RefEmployee_RoleId = request.PageParament.RefEmployee_RoleId,
      };
      _inventoryManagementSystemDB.Add(employee_UserRoleMap);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_UserRoleMapAddEmployee_RoleDefaultValueResponse Employee_UserRoleMapAddEmployee_Role_GetDefaultValue(Employee_UserRoleMapAddEmployee_RolePageparameter request, UserInfo loginUser)
    {
      return new Employee_UserRoleMapAddEmployee_RoleDefaultValueResponse
      {
        RefEmployeeId = default(string),
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource(Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource(Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_UserRoleMapEditEmployeeResponse Employee_UserRoleMapEditEmployee_GetValue(Employee_UserRoleMapEditEmployeeQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_UserRoleMapEditEmployeeResponse
      {
        Id = entity.Id,
        RefEmployeeId = entity.RefEmployeeId,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_UserRoleMapEditEmployee(Employee_UserRoleMapEditEmployeeUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_UserRoleMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefEmployee_RoleId = request.RefEmployee_RoleId;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource(Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource(Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_UserRoleMapEditEmployee_RoleResponse Employee_UserRoleMapEditEmployee_Role_GetValue(Employee_UserRoleMapEditEmployee_RoleQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_UserRoleMapEditEmployee_RoleResponse
      {
        Id = entity.Id,
        RefEmployeeId = entity.RefEmployeeId,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_UserRoleMapEditEmployee_Role(Employee_UserRoleMapEditEmployee_RoleUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_UserRoleMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefEmployeeId = request.RefEmployeeId;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource(Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource(Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_UserRoleMapDelete(Employee_UserRoleMapDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_UserRoleMap = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_UserRoleMapDetailResponse Employee_UserRoleMapDetail_GetValue(Employee_UserRoleMapDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_UserRoleMapDetailResponse
      {
        Id = entity.Id,
        RefEmployeeId = entity.RefEmployeeId,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployeeId_DataSource(Employee_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource(Employee_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_UserRoleMapSearchEmployeeResponse> Employee_UserRoleMapSearchEmployee(Employee_UserRoleMapSearchEmployeeQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_UserRoleMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_UserRoleMap_Id", "Employee_UserRoleMap_RefEmployeeId", "Employee_UserRoleMap_RefEmployee_RoleId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_UserRoleMap in _inventoryManagementSystemDB.Employee_UserRoleMap
      select new { Employee_UserRoleMap };

      // where
      if (request.PageParament.RefEmployeeId != null)
      {
        select = select.Where(x => x.Employee_UserRoleMap.RefEmployeeId == request.PageParament.RefEmployeeId);
      }

      // query
      var queryoutput = select.Select(x => new Employee_UserRoleMapSearchEmployeeResponse
      {
        Employee_UserRoleMap_Id = x.Employee_UserRoleMap.Id,
        Employee_UserRoleMap_RefEmployeeId = x.Employee_UserRoleMap.RefEmployeeId,
        Employee_UserRoleMap_RefEmployee_RoleId = x.Employee_UserRoleMap.RefEmployee_RoleId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_UserRoleMapSearchEmployeeResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_UserRoleMapSearchEmployeeDefaultValueResponse Employee_UserRoleMapSearchEmployee_GetDefaultValue(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser)
    {
      return new Employee_UserRoleMapSearchEmployeeDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource(Employee_UserRoleMapSearchEmployeeQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_UserRoleMapSearchEmployee_RoleResponse> Employee_UserRoleMapSearchEmployee_Role(Employee_UserRoleMapSearchEmployee_RoleQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_UserRoleMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_UserRoleMap_Id", "Employee_UserRoleMap_RefEmployeeId", "Employee_UserRoleMap_RefEmployee_RoleId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_UserRoleMap in _inventoryManagementSystemDB.Employee_UserRoleMap
      select new { Employee_UserRoleMap };

      // where
      if (request.PageParament.RefEmployee_RoleId != null)
      {
        select = select.Where(x => x.Employee_UserRoleMap.RefEmployee_RoleId == request.PageParament.RefEmployee_RoleId);
      }

      // query
      var queryoutput = select.Select(x => new Employee_UserRoleMapSearchEmployee_RoleResponse
      {
        Employee_UserRoleMap_Id = x.Employee_UserRoleMap.Id,
        Employee_UserRoleMap_RefEmployeeId = x.Employee_UserRoleMap.RefEmployeeId,
        Employee_UserRoleMap_RefEmployee_RoleId = x.Employee_UserRoleMap.RefEmployee_RoleId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_UserRoleMapSearchEmployee_RoleResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_UserRoleMapSearchEmployee_RoleDefaultValueResponse Employee_UserRoleMapSearchEmployee_Role_GetDefaultValue(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      return new Employee_UserRoleMapSearchEmployee_RoleDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee.Id, x.Employee.DisplayName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(Employee_UserRoleMapSearchEmployee_RoleQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Role(Employee_RoleAuthMapAddEmployee_RoleRequest request, UserInfo loginUser)
    {
      var employee_RoleAuthMap = new Employee_RoleAuthMap
      {
        Id = Guid.NewGuid(),
        RefEmployee_RoleId = request.PageParament.RefEmployee_RoleId,
        RefEmployee_AuthId = request.RefEmployee_AuthId,
      };
      _inventoryManagementSystemDB.Add(employee_RoleAuthMap);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAuthMapAddEmployee_RoleDefaultValueResponse Employee_RoleAuthMapAddEmployee_Role_GetDefaultValue(Employee_RoleAuthMapAddEmployee_RolePageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleAuthMapAddEmployee_RoleDefaultValueResponse
      {
        RefEmployee_AuthId = default(Guid),
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Auth(Employee_RoleAuthMapAddEmployee_AuthRequest request, UserInfo loginUser)
    {
      var employee_RoleAuthMap = new Employee_RoleAuthMap
      {
        Id = Guid.NewGuid(),
        RefEmployee_RoleId = request.RefEmployee_RoleId,
        RefEmployee_AuthId = request.PageParament.RefEmployee_AuthId,
      };
      _inventoryManagementSystemDB.Add(employee_RoleAuthMap);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAuthMapAddEmployee_AuthDefaultValueResponse Employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue(Employee_RoleAuthMapAddEmployee_AuthPageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleAuthMapAddEmployee_AuthDefaultValueResponse
      {
        RefEmployee_RoleId = default(Guid),
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleAuthMapEditEmployee_RoleResponse Employee_RoleAuthMapEditEmployee_Role_GetValue(Employee_RoleAuthMapEditEmployee_RoleQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_RoleAuthMapEditEmployee_RoleResponse
      {
        Id = entity.Id,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
        RefEmployee_AuthId = entity.RefEmployee_AuthId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Role(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_RoleAuthMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefEmployee_AuthId = request.RefEmployee_AuthId;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleAuthMapEditEmployee_AuthResponse Employee_RoleAuthMapEditEmployee_Auth_GetValue(Employee_RoleAuthMapEditEmployee_AuthQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_RoleAuthMapEditEmployee_AuthResponse
      {
        Id = entity.Id,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
        RefEmployee_AuthId = entity.RefEmployee_AuthId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Auth(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_RoleAuthMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefEmployee_RoleId = request.RefEmployee_RoleId;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapDelete(Employee_RoleAuthMapDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_RoleAuthMap = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_RoleAuthMapDetailResponse Employee_RoleAuthMapDetail_GetValue(Employee_RoleAuthMapDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_RoleAuthMapDetailResponse
      {
        Id = entity.Id,
        RefEmployee_RoleId = entity.RefEmployee_RoleId,
        RefEmployee_AuthId = entity.RefEmployee_AuthId,
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_RoleResponse> Employee_RoleAuthMapSearchEmployee_Role(Employee_RoleAuthMapSearchEmployee_RoleQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_RoleAuthMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_RoleAuthMap_Id", "Employee_RoleAuthMap_RefEmployee_RoleId", "Employee_RoleAuthMap_RefEmployee_AuthId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_RoleAuthMap in _inventoryManagementSystemDB.Employee_RoleAuthMap
      select new { Employee_RoleAuthMap };

      // where
      if (request.PageParament.RefEmployee_RoleId != null)
      {
        select = select.Where(x => x.Employee_RoleAuthMap.RefEmployee_RoleId == request.PageParament.RefEmployee_RoleId);
      }

      // query
      var queryoutput = select.Select(x => new Employee_RoleAuthMapSearchEmployee_RoleResponse
      {
        Employee_RoleAuthMap_Id = x.Employee_RoleAuthMap.Id,
        Employee_RoleAuthMap_RefEmployee_RoleId = x.Employee_RoleAuthMap.RefEmployee_RoleId,
        Employee_RoleAuthMap_RefEmployee_AuthId = x.Employee_RoleAuthMap.RefEmployee_AuthId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_RoleAuthMapSearchEmployee_RoleResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleAuthMapSearchEmployee_RoleDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleAuthMapSearchEmployee_RoleDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(Employee_RoleAuthMapSearchEmployee_RoleQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_AuthResponse> Employee_RoleAuthMapSearchEmployee_Auth(Employee_RoleAuthMapSearchEmployee_AuthQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_RoleAuthMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_RoleAuthMap_Id", "Employee_RoleAuthMap_RefEmployee_RoleId", "Employee_RoleAuthMap_RefEmployee_AuthId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_RoleAuthMap in _inventoryManagementSystemDB.Employee_RoleAuthMap
      select new { Employee_RoleAuthMap };

      // where
      if (request.PageParament.RefEmployee_AuthId != null)
      {
        select = select.Where(x => x.Employee_RoleAuthMap.RefEmployee_AuthId == request.PageParament.RefEmployee_AuthId);
      }

      // query
      var queryoutput = select.Select(x => new Employee_RoleAuthMapSearchEmployee_AuthResponse
      {
        Employee_RoleAuthMap_Id = x.Employee_RoleAuthMap.Id,
        Employee_RoleAuthMap_RefEmployee_RoleId = x.Employee_RoleAuthMap.RefEmployee_RoleId,
        Employee_RoleAuthMap_RefEmployee_AuthId = x.Employee_RoleAuthMap.RefEmployee_AuthId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_RoleAuthMapSearchEmployee_AuthResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleAuthMapSearchEmployee_AuthDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleAuthMapSearchEmployee_AuthDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Role.Id, x.Employee_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource(Employee_RoleAuthMapSearchEmployee_AuthQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Employee_Auth.Id, x.Employee_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_AuthAdd(Employee_AuthAddRequest request, UserInfo loginUser)
    {
      var employee_Auth = new Employee_Auth
      {
        Id = Guid.NewGuid(),
        AuthName = request.AuthName,
      };
      _inventoryManagementSystemDB.Add(employee_Auth);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_AuthAddDefaultValueResponse Employee_AuthAdd_GetDefaultValue(Employee_AuthAddPageparameter request, UserInfo loginUser)
    {
      return new Employee_AuthAddDefaultValueResponse
      {
        AuthName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_AuthEditResponse Employee_AuthEdit_GetValue(Employee_AuthEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Auth.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_AuthEditResponse
      {
        Id = entity.Id,
        AuthName = entity.AuthName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_AuthEdit(Employee_AuthEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_Auth.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_Auth = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.AuthName = request.AuthName;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_AuthDelete(Employee_AuthDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Auth.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_Auth = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_AuthDetailResponse Employee_AuthDetail_GetValue(Employee_AuthDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Auth.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_AuthDetailResponse
      {
        Id = entity.Id,
        AuthName = entity.AuthName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_AuthSearchResponse> Employee_AuthSearch(Employee_AuthSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_Auth_AuthName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_Auth_AuthName", "Employee_Auth_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_Auth in _inventoryManagementSystemDB.Employee_Auth
      select new { Employee_Auth };

      // where

      // query
      var queryoutput = select.Select(x => new Employee_AuthSearchResponse
      {
        Employee_Auth_AuthName = x.Employee_Auth.AuthName,
        Employee_Auth_Id = x.Employee_Auth.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_AuthSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_AuthSearchDefaultValueResponse Employee_AuthSearch_GetDefaultValue(Employee_AuthSearchPageparameter request, UserInfo loginUser)
    {
      return new Employee_AuthSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAdd(Employee_RoleAddRequest request, UserInfo loginUser)
    {
      var employee_Role = new Employee_Role
      {
        Id = Guid.NewGuid(),
        RoleName = request.RoleName,
      };
      _inventoryManagementSystemDB.Add(employee_Role);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAddDefaultValueResponse Employee_RoleAdd_GetDefaultValue(Employee_RoleAddPageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleAddDefaultValueResponse
      {
        RoleName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleEditResponse Employee_RoleEdit_GetValue(Employee_RoleEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_RoleEditResponse
      {
        Id = entity.Id,
        RoleName = entity.RoleName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleEdit(Employee_RoleEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_Role = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RoleName = request.RoleName;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_RoleDelete(Employee_RoleDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee_Role = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_RoleDetailResponse Employee_RoleDetail_GetValue(Employee_RoleDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.Id == request.Id);
      return new Employee_RoleDetailResponse
      {
        Id = entity.Id,
        RoleName = entity.RoleName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleSearchResponse> Employee_RoleSearch(Employee_RoleSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_Role_RoleName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_Role_RoleName", "Employee_Role_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee_Role in _inventoryManagementSystemDB.Employee_Role
      select new { Employee_Role };

      // where

      // query
      var queryoutput = select.Select(x => new Employee_RoleSearchResponse
      {
        Employee_Role_RoleName = x.Employee_Role.RoleName,
        Employee_Role_Id = x.Employee_Role.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<Employee_RoleSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleSearchDefaultValueResponse Employee_RoleSearch_GetDefaultValue(Employee_RoleSearchPageparameter request, UserInfo loginUser)
    {
      return new Employee_RoleSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult EmployeeAdd(EmployeeAddRequest request, UserInfo loginUser)
    {
      var employee = new Employee
      {
        Id = request.Id,
        DisplayName = request.DisplayName,
      };
      _inventoryManagementSystemDB.Add(employee);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public EmployeeAddDefaultValueResponse EmployeeAdd_GetDefaultValue(EmployeeAddPageparameter request, UserInfo loginUser)
    {
      return new EmployeeAddDefaultValueResponse
      {
        Id = default(string),
        DisplayName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public EmployeeEditResponse EmployeeEdit_GetValue(EmployeeEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee.FirstOrDefault(x => x.Id == request.Id);
      return new EmployeeEditResponse
      {
        Id = entity.Id,
        DisplayName = entity.DisplayName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult EmployeeEdit(EmployeeEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Employee.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Id = request.Id;
      one.DisplayName = request.DisplayName;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult EmployeeDelete(EmployeeDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Employee = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public EmployeeDetailResponse EmployeeDetail_GetValue(EmployeeDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Employee.FirstOrDefault(x => x.Id == request.Id);
      return new EmployeeDetailResponse
      {
        Id = entity.Id,
        DisplayName = entity.DisplayName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<EmployeeSearchResponse> EmployeeSearch(EmployeeSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Employee_DisplayName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Employee_DisplayName", "Employee_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Employee in _inventoryManagementSystemDB.Employee
      select new { Employee };

      // where

      // query
      var queryoutput = select.Select(x => new EmployeeSearchResponse
      {
        Employee_DisplayName = x.Employee.DisplayName,
        Employee_Id = x.Employee.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<EmployeeSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public EmployeeSearchDefaultValueResponse EmployeeSearch_GetDefaultValue(EmployeeSearchPageparameter request, UserInfo loginUser)
    {
      return new EmployeeSearchDefaultValueResponse
      {
      };
    }

  }
}
