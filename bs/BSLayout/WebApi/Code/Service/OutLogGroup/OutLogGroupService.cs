using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.OutLogGroup;
using WebApi.Code.Interface.OutLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.OutLogGroup
{
  public class OutLogGroupService : IOutLogGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public OutLogGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult OutLogAdd(OutLogAddRequest request, UserInfo loginUser)
    {
      var outLog = new OutLog
      {
        Id = Guid.NewGuid(),
        Createtime = request.Createtime,
        Goods = request.Goods,
        Count = request.Count,
        OutTypeInfo = request.OutTypeInfo,
        BinLocation = request.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.SerializeObject(request.BinLocation),
      };
      _inventoryManagementSystemDB.Add(outLog);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OutLogAddDefaultValueResponse OutLogAdd_GetDefaultValue(OutLogAddPageparameter request, UserInfo loginUser)
    {
      return new OutLogAddDefaultValueResponse
      {
        Createtime = default(DateTime?),
        Goods = default(string?),
        Count = default(int?),
        OutTypeInfo = default(string?),
        BinLocation = default(List<string?>),
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_goods_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_OutTypeInfo_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from OutType in _inventoryManagementSystemDB.OutType
      select new { OutType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.OutType.Name, x.OutType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_BinLocation_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public OutLogEditResponse OutLogEdit_GetValue(OutLogEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.OutLog.FirstOrDefault(x => x.Id == request.Id);
      return new OutLogEditResponse
      {
        Id = entity.Id,
        Createtime = entity.Createtime,
        Goods = entity.Goods,
        Count = entity.Count,
        OutTypeInfo = entity.OutTypeInfo,
        BinLocation = entity.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(entity.BinLocation),
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult OutLogEdit(OutLogEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.OutLog.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { OutLog = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Createtime = request.Createtime;
      one.Goods = request.Goods;
      one.Count = request.Count;
      one.OutTypeInfo = request.OutTypeInfo;
      one.BinLocation = request.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.SerializeObject(request.BinLocation);
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_goods_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_OutTypeInfo_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from OutType in _inventoryManagementSystemDB.OutType
      select new { OutType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.OutType.Name, x.OutType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_BinLocation_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public OutLogDetailResponse OutLogDetail_GetValue(OutLogDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.OutLog.FirstOrDefault(x => x.Id == request.Id);
      return new OutLogDetailResponse
      {
        Id = entity.Id,
        Createtime = entity.Createtime,
        Goods = entity.Goods,
        Count = entity.Count,
        OutTypeInfo = entity.OutTypeInfo,
        BinLocation = entity.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(entity.BinLocation),
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_goods_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_OutTypeInfo_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from OutType in _inventoryManagementSystemDB.OutType
      select new { OutType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.OutType.Name, x.OutType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_BinLocation_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult OutLogDelete(OutLogDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.OutLog.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { OutLog = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<OutLogSearchResponse> OutLogSearch(OutLogSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "OutLog_Createtime";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "OutLog_Createtime", "OutLog_Goods", "OutLog_Count", "OutLog_OutTypeInfo", "OutLog_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from OutLog in _inventoryManagementSystemDB.OutLog
      select new { OutLog };

      // where

      // query
      var queryoutput = select.Select(x => new OutLogSearchResponse
      {
        OutLog_Createtime = x.OutLog.Createtime,
        OutLog_Goods = x.OutLog.Goods,
        OutLog_Count = x.OutLog.Count,
        OutLog_OutTypeInfo = x.OutLog.OutTypeInfo,
        OutLog_BinLocation = x.OutLog.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(x.OutLog.BinLocation),
        OutLog_Id = x.OutLog.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<OutLogSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public OutLogSearchDefaultValueResponse OutLogSearch_GetDefaultValue(OutLogSearchPageparameter request, UserInfo loginUser)
    {
      return new OutLogSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_goods_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '出库类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_OutTypeInfo_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from OutType in _inventoryManagementSystemDB.OutType
      select new { OutType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.OutType.Name, x.OutType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_BinLocation_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
