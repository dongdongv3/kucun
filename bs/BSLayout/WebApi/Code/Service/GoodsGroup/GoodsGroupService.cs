using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.GoodsGroup;
using WebApi.Code.Interface.GoodsGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.GoodsGroup
{
  public class GoodsGroupService : IGoodsGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public GoodsGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult GoodsAdd(GoodsAddRequest request, UserInfo loginUser)
    {
      var goods = new Goods
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
        Count = request.Count,
      };
      _inventoryManagementSystemDB.Add(goods);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public GoodsAddDefaultValueResponse GoodsAdd_GetDefaultValue(GoodsAddPageparameter request, UserInfo loginUser)
    {
      return new GoodsAddDefaultValueResponse
      {
        Name = default(string?),
        Count = default(int?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public GoodsEditResponse GoodsEdit_GetValue(GoodsEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Goods.FirstOrDefault(x => x.Id == request.Id);
      return new GoodsEditResponse
      {
        Id = entity.Id,
        Name = entity.Name,
        Count = entity.Count,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult GoodsEdit(GoodsEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.Goods.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Goods = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Name = request.Name;
      one.Count = request.Count;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public GoodsDetailResponse GoodsDetail_GetValue(GoodsDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Goods.FirstOrDefault(x => x.Id == request.Id);
      return new GoodsDetailResponse
      {
        Id = entity.Id,
        Name = entity.Name,
        Count = entity.Count,
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult GoodsDelete(GoodsDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.Goods.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Goods = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<GoodsSearchResponse> GoodsSearch(GoodsSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Goods_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Goods_Name", "Goods_Count", "Goods_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods };

      // where

      // query
      var queryoutput = select.Select(x => new GoodsSearchResponse
      {
        Goods_Name = x.Goods.Name,
        Goods_Count = x.Goods.Count,
        Goods_Id = x.Goods.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<GoodsSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public GoodsSearchDefaultValueResponse GoodsSearch_GetDefaultValue(GoodsSearchPageparameter request, UserInfo loginUser)
    {
      return new GoodsSearchDefaultValueResponse
      {
      };
    }

  }
}
