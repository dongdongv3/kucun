using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.InTypeGroup;
using WebApi.Code.Interface.InTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.InTypeGroup
{
  public class InTypeGroupService : IInTypeGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public InTypeGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult InTypeAdd(InTypeAddRequest request, UserInfo loginUser)
    {
      var inType = new InType
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
      };
      _inventoryManagementSystemDB.Add(inType);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public InTypeAddDefaultValueResponse InTypeAdd_GetDefaultValue(InTypeAddPageparameter request, UserInfo loginUser)
    {
      return new InTypeAddDefaultValueResponse
      {
        Name = default(string?),
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult InTypeDelete(InTypeDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.InType.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { InType = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<InTypeSearchResponse> InTypeSearch(InTypeSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "InType_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "InType_Name", "InType_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from InType in _inventoryManagementSystemDB.InType
      select new { InType };

      // where

      // query
      var queryoutput = select.Select(x => new InTypeSearchResponse
      {
        InType_Name = x.InType.Name,
        InType_Id = x.InType.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<InTypeSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public InTypeSearchDefaultValueResponse InTypeSearch_GetDefaultValue(InTypeSearchPageparameter request, UserInfo loginUser)
    {
      return new InTypeSearchDefaultValueResponse
      {
      };
    }

  }
}
