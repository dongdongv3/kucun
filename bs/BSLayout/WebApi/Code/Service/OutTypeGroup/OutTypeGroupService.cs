using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.OutTypeGroup;
using WebApi.Code.Interface.OutTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.OutTypeGroup
{
  public class OutTypeGroupService : IOutTypeGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public OutTypeGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult OutTypeAdd(OutTypeAddRequest request, UserInfo loginUser)
    {
      var outType = new OutType
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
      };
      _inventoryManagementSystemDB.Add(outType);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OutTypeAddDefaultValueResponse OutTypeAdd_GetDefaultValue(OutTypeAddPageparameter request, UserInfo loginUser)
    {
      return new OutTypeAddDefaultValueResponse
      {
        Name = default(string?),
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult OutTypeDelete(OutTypeDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.OutType.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { OutType = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<OutTypeSearchResponse> OutTypeSearch(OutTypeSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "OutType_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "OutType_Name", "OutType_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from OutType in _inventoryManagementSystemDB.OutType
      select new { OutType };

      // where

      // query
      var queryoutput = select.Select(x => new OutTypeSearchResponse
      {
        OutType_Name = x.OutType.Name,
        OutType_Id = x.OutType.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<OutTypeSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public OutTypeSearchDefaultValueResponse OutTypeSearch_GetDefaultValue(OutTypeSearchPageparameter request, UserInfo loginUser)
    {
      return new OutTypeSearchDefaultValueResponse
      {
      };
    }

  }
}
