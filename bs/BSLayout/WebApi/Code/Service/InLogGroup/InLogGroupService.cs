using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.InLogGroup;
using WebApi.Code.Interface.InLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.InLogGroup
{
  public class InLogGroupService : IInLogGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public InLogGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult InLogAdd(InLogAddRequest request, UserInfo loginUser)
    {
      var inLog = new InLog
      {
        Id = Guid.NewGuid(),
        CreateTime = request.CreateTime,
        Goods = request.Goods,
        BinLocation = request.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.SerializeObject(request.BinLocation),
        Count = request.Count,
        InTypeInfo = request.InTypeInfo,
      };
      _inventoryManagementSystemDB.Add(inLog);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public InLogAddDefaultValueResponse InLogAdd_GetDefaultValue(InLogAddPageparameter request, UserInfo loginUser)
    {
      return new InLogAddDefaultValueResponse
      {
        CreateTime = default(DateTime?),
        Goods = default(string?),
        BinLocation = default(List<string?>),
        Count = default(int?),
        InTypeInfo = default(string?),
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_goods_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_BinLocation_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_InTypeInfo_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from InType in _inventoryManagementSystemDB.InType
      select new { InType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.InType.Name, x.InType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public InLogEditResponse InLogEdit_GetValue(InLogEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.InLog.FirstOrDefault(x => x.Id == request.Id);
      return new InLogEditResponse
      {
        Id = entity.Id,
        CreateTime = entity.CreateTime,
        Goods = entity.Goods,
        BinLocation = entity.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(entity.BinLocation),
        Count = entity.Count,
        InTypeInfo = entity.InTypeInfo,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult InLogEdit(InLogEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.InLog.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { InLog = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.CreateTime = request.CreateTime;
      one.Goods = request.Goods;
      one.BinLocation = request.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.SerializeObject(request.BinLocation);
      one.Count = request.Count;
      one.InTypeInfo = request.InTypeInfo;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_goods_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_BinLocation_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_InTypeInfo_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from InType in _inventoryManagementSystemDB.InType
      select new { InType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.InType.Name, x.InType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public InLogDetailResponse InLogDetail_GetValue(InLogDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.InLog.FirstOrDefault(x => x.Id == request.Id);
      return new InLogDetailResponse
      {
        Id = entity.Id,
        CreateTime = entity.CreateTime,
        Goods = entity.Goods,
        BinLocation = entity.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(entity.BinLocation),
        Count = entity.Count,
        InTypeInfo = entity.InTypeInfo,
      };
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_goods_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_BinLocation_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_InTypeInfo_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from InType in _inventoryManagementSystemDB.InType
      select new { InType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.InType.Name, x.InType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult InLogDelete(InLogDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.InLog.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { InLog = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<InLogSearchResponse> InLogSearch(InLogSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "InLog_CreateTime";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "InLog_CreateTime", "InLog_Goods", "InLog_Count", "InLog_InTypeInfo", "InLog_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from InLog in _inventoryManagementSystemDB.InLog
      select new { InLog };

      // where

      // query
      var queryoutput = select.Select(x => new InLogSearchResponse
      {
        InLog_CreateTime = x.InLog.CreateTime,
        InLog_Goods = x.InLog.Goods,
        InLog_BinLocation = x.InLog.BinLocation == null ? null : Newtonsoft.Json.JsonConvert.DeserializeObject<List<string?>>(x.InLog.BinLocation),
        InLog_Count = x.InLog.Count,
        InLog_InTypeInfo = x.InLog.InTypeInfo,
        InLog_Id = x.InLog.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<InLogSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public InLogSearchDefaultValueResponse InLogSearch_GetDefaultValue(InLogSearchPageparameter request, UserInfo loginUser)
    {
      return new InLogSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_goods_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from Goods in _inventoryManagementSystemDB.Goods
      select new { Goods }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Goods.Name, x.Goods.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_BinLocation_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.BinLocation.Name, x.BinLocation.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_InTypeInfo_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from InType in _inventoryManagementSystemDB.InType
      select new { InType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.InType.Name, x.InType.Name)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
