using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.BinLocationGroup;
using WebApi.Code.Interface.BinLocationGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.BinLocationGroup
{
  public class BinLocationGroupService : IBinLocationGroupInterface
  {
    public readonly InventoryManagementSystemDB _inventoryManagementSystemDB;

    public BinLocationGroupService(InventoryManagementSystemDB _inventoryManagementSystemDB)
    {
      this._inventoryManagementSystemDB = _inventoryManagementSystemDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult BinLocationAdd(BinLocationAddRequest request, UserInfo loginUser)
    {
      var binLocation = new BinLocation
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
      };
      _inventoryManagementSystemDB.Add(binLocation);
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public BinLocationAddDefaultValueResponse BinLocationAdd_GetDefaultValue(BinLocationAddPageparameter request, UserInfo loginUser)
    {
      return new BinLocationAddDefaultValueResponse
      {
        Name = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public BinLocationEditResponse BinLocationEdit_GetValue(BinLocationEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.BinLocation.FirstOrDefault(x => x.Id == request.Id);
      return new BinLocationEditResponse
      {
        Id = entity.Id,
        Name = entity.Name,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult BinLocationEdit(BinLocationEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _inventoryManagementSystemDB.BinLocation.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { BinLocation = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Name = request.Name;
      _inventoryManagementSystemDB.Entry(one).State = EntityState.Modified;
      _inventoryManagementSystemDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public BinLocationDetailResponse BinLocationDetail_GetValue(BinLocationDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.BinLocation.FirstOrDefault(x => x.Id == request.Id);
      return new BinLocationDetailResponse
      {
        Id = entity.Id,
        Name = entity.Name,
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult BinLocationDelete(BinLocationDeleteRequest request, UserInfo loginUser)
    {
      var entity = _inventoryManagementSystemDB.BinLocation.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { BinLocation = entity };
      if (entity != null)
      {
        _inventoryManagementSystemDB.Remove(entity);
        _inventoryManagementSystemDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<BinLocationSearchResponse> BinLocationSearch(BinLocationSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "BinLocation_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "BinLocation_Name", "BinLocation_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from BinLocation in _inventoryManagementSystemDB.BinLocation
      select new { BinLocation };

      // where

      // query
      var queryoutput = select.Select(x => new BinLocationSearchResponse
      {
        BinLocation_Name = x.BinLocation.Name,
        BinLocation_Id = x.BinLocation.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<BinLocationSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public BinLocationSearchDefaultValueResponse BinLocationSearch_GetDefaultValue(BinLocationSearchPageparameter request, UserInfo loginUser)
    {
      return new BinLocationSearchDefaultValueResponse
      {
      };
    }

  }
}
