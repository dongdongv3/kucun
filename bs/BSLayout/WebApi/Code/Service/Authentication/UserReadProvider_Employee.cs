using System;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Claims;
using WebApi.Code.Data;
using WebApi.Code.Common;

namespace WebApi.Code.Service.Authentication
{
  public class UserReadProvider_Employee : IUserReadInterface
  {
    private readonly IEnumerable<IAuthenticationProviderInterface> authenticationProviderInterfaces;
    private readonly InventoryManagementSystemDB inventoryManagementSystemDB;
    public UserReadProvider_Employee(IEnumerable<IAuthenticationProviderInterface> authenticationProviders, InventoryManagementSystemDB inventoryManagementSystemDB)
    {
      this.authenticationProviderInterfaces = authenticationProviders;
      this.inventoryManagementSystemDB = inventoryManagementSystemDB;
    }

    public Task<UserInfo> AutoProvisionUser(string provider, string userId, List<Claim> claims)
    {
      throw new System.NotImplementedException();
    }

    public Task<UserInfo> FindByExternalProvider(string provider, string userId)
    {
      throw new System.NotImplementedException();
    }

    public IAuthenticationProviderInterface GetAuthenticationProvider(LoginType type)
    {
      foreach (var auth in authenticationProviderInterfaces)
      {
        if (auth.Type == type)
        {
          return auth;
        }
      }
      throw new AuthException("无效的登录提供程序");
    }

    public async Task<UserInfo> GetUserByID(string userID)
    {
      var user = await inventoryManagementSystemDB.Employee.FirstOrDefaultAsync(x => x.Id == userID);
      if (user == null)
      {
        throw new AuthException("无法查询到有效用户");
      }
      Dictionary<string, string> Role = new Dictionary<string, string>();
      Dictionary<string, string> Auth = new Dictionary<string, string>();

      //role auth模式
      var roleauth = await inventoryManagementSystemDB.Employee_UserRoleMap.Include(x => x.RefEmployee_Role).ThenInclude(x => x.SubEmployee_RoleAuthMap).ThenInclude(x => x.RefEmployee_Auth).Where(x => x.RefEmployeeId == userID).ToListAsync();
      roleauth.ForEach(x => Role[x.RefEmployee_Role.RoleName] = x.Id.ToString());
      roleauth.SelectMany(x => x.RefEmployee_Role.SubEmployee_RoleAuthMap).ToList().ForEach(x => Auth[x.RefEmployee_Auth.AuthName] = x.RefEmployee_AuthId.ToString());

      return new UserInfo
      {
        DisplayName = user.DisplayName,
        Id = user.Id,
        Roles = Role,
        Auths = Auth,
      };
    }
  }
}
