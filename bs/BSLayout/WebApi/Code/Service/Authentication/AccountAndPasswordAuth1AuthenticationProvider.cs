using System;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WebApi.Code.Data;
using WebApi.Code.Common;
using WebApi.Code.Common.PwdProvider;

namespace WebApi.Code.Service.Authentication
{
  public class AccountAndPasswordAuth1AuthenticationProvider : IAuthenticationProviderInterface
  {
    private readonly InventoryManagementSystemDB db;
    private readonly PWDEncryptProvider.PWDEncryptType encryptType = PWDEncryptProvider.PWDEncryptType.None;
    public AccountAndPasswordAuth1AuthenticationProvider(InventoryManagementSystemDB db)
    {
      this.db = db;
    }

    public LoginType Type => LoginType.AccountAndPasswordAuth1;

    public async Task<string> GetUserID(string Key)
    {
      var user = await this.db.Employee_AccountAndPasswordAuth1.FirstAsync(x => x.LoginID == Key);
      if (user == null)
      {
        throw new AuthException("无效的用户");
      }
      return user.RefEmployeeId;
    }

    public async Task<bool> Validation(params string[] param)
    {
      if (param.Length != 2)
      {
        throw new AuthException("无效的参数数量");
      }
      var account = param[0];
      var pwd = param[1];

      var pwdprivoder = PWDEncryptProvider.GetPWDEncrypt(encryptType);
      var pwdsct = pwdprivoder.Encrypt(pwd);

      var loginInfo = await db.Employee_AccountAndPasswordAuth1.FirstOrDefaultAsync(x => x.LoginID == account && x.Pwd == pwdsct);
      return loginInfo != null;
    }
  }
}
