using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class InLog
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 入库时间
    /// </summary>
    public DateTime? CreateTime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    [MaxLength(50)]
    public string? Goods { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    [MaxLength(500)]
    public string? BinLocation { get; set; }
    /// <summary>
    /// 数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 类型
    /// </summary>
    [MaxLength(50)]
    public string? InTypeInfo { get; set; }
  }
}
