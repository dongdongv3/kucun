using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Employee
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [MaxLength(50)]
    [Required]
    public string Id { get; set; }
    /// <summary>
    /// 显示名
    /// </summary>
    [MaxLength(50)]
    public string? DisplayName { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道
    /// </summary>
    public Employee_AccountAndPasswordAuth1 SubEmployee_AccountAndPasswordAuth1 { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public List<Employee_UserRoleMap> SubEmployee_UserRoleMap { get; set; }
  }
}
