using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Employee_RoleAuthMap
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
    /// <summary>
    /// Employee_Role
    /// </summary>
    public Employee_Role RefEmployee_Role { get; set; }
    /// <summary>
    /// 权限
    /// </summary>
    public Guid RefEmployee_AuthId { get; set; }
    /// <summary>
    /// Employee_Auth
    /// </summary>
    public Employee_Auth RefEmployee_Auth { get; set; }
  }
}
