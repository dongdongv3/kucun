using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Employee_AccountAndPasswordAuth1
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 登录账户
    /// </summary>
    [MaxLength(50)]
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    [MaxLength(100)]
    public string? Pwd { get; set; }
    /// <summary>
    /// 员工-用户信息
    /// </summary>
    public string RefEmployeeId { get; set; }
    /// <summary>
    /// Employee
    /// </summary>
    public Employee RefEmployee { get; set; }
  }
}
