using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Employee_Role
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 角色名称
    /// </summary>
    [MaxLength(50)]
    public string? RoleName { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public List<Employee_UserRoleMap> SubEmployee_UserRoleMap { get; set; }
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    public List<Employee_RoleAuthMap> SubEmployee_RoleAuthMap { get; set; }
  }
}
