using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class FileUpLoad
  {
    /// <summary>
    /// ID
    /// </summary>
    [Key]
    [MaxLength(50)]
    [Required]
    public string ID { get; set; }
    /// <summary>
    /// FileName
    /// </summary>
    [MaxLength(255)]
    public string? FileName { get; set; }
    /// <summary>
    /// Ext
    /// </summary>
    [MaxLength(50)]
    public string? Ext { get; set; }
    /// <summary>
    /// UserID
    /// </summary>
    [MaxLength(50)]
    public string? UserID { get; set; }
    /// <summary>
    /// CreateTime
    /// </summary>
    public DateTime? CreateTime { get; set; }
    /// <summary>
    /// IsUse
    /// </summary>
    public bool? IsUse { get; set; }
  }
}
