using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Employee_UserRoleMap
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 员工-用户信息
    /// </summary>
    public string RefEmployeeId { get; set; }
    /// <summary>
    /// Employee
    /// </summary>
    public Employee RefEmployee { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
    /// <summary>
    /// Employee_Role
    /// </summary>
    public Employee_Role RefEmployee_Role { get; set; }
  }
}
