using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class OutLog
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 出库时间
    /// </summary>
    public DateTime? Createtime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    [MaxLength(50)]
    public string? Goods { get; set; }
    /// <summary>
    /// 出库数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 出库类型
    /// </summary>
    [MaxLength(50)]
    public string? OutTypeInfo { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    [MaxLength(500)]
    public string? BinLocation { get; set; }
  }
}
