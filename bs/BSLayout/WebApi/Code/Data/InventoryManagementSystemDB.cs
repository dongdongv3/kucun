using System;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Data
{
  /// <summary>
  /// 库存管理系统
  /// </summary>
  public class InventoryManagementSystemDB : DbContext
  {
    public InventoryManagementSystemDB(DbContextOptions<InventoryManagementSystemDB> options) : base(options) { }
    /// <summary>
    /// 员工-用户信息
    /// </summary>
    public DbSet<Employee> Employee { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道
    /// </summary>
    public DbSet<Employee_AccountAndPasswordAuth1> Employee_AccountAndPasswordAuth1 { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public DbSet<Employee_Role> Employee_Role { get; set; }
    /// <summary>
    /// 权限
    /// </summary>
    public DbSet<Employee_Auth> Employee_Auth { get; set; }
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    public DbSet<Employee_RoleAuthMap> Employee_RoleAuthMap { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public DbSet<Employee_UserRoleMap> Employee_UserRoleMap { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    public DbSet<Goods> Goods { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    public DbSet<BinLocation> BinLocation { get; set; }
    /// <summary>
    /// 入库类型
    /// </summary>
    public DbSet<InType> InType { get; set; }
    /// <summary>
    /// 出库类型
    /// </summary>
    public DbSet<OutType> OutType { get; set; }
    /// <summary>
    /// 出库记录
    /// </summary>
    public DbSet<OutLog> OutLog { get; set; }
    /// <summary>
    /// 入库记录
    /// </summary>
    public DbSet<InLog> InLog { get; set; }
    /// <summary>
    /// 文件上传
    /// </summary>
    public DbSet<FileUpLoad> FileUpLoad { get; set; }
  }
}
