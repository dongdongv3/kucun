using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Interface.InLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class InLogAddRequest : InLogAddPageparameter
  {
    /// <summary>
    /// 入库时间
    /// </summary>
    public DateTime? CreateTime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    public string? Goods { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    public List<string?> BinLocation { get; set; }
    /// <summary>
    /// 数量
    /// </summary>
    [Range(0, 10000000)]
    public int? Count { get; set; }
    /// <summary>
    /// 类型
    /// </summary>
    public string? InTypeInfo { get; set; }
  }
}
