using System;
using System.Collections.Generic;

namespace WebApi.Code.Interface.InLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class InLogSearchResponse : InLogSearchPageparameter
  {
    /// <summary>
    /// 入库记录 - 入库时间
    /// </summary>
    public DateTime? InLog_CreateTime { get; set; }
    /// <summary>
    /// 入库记录 - 商品
    /// </summary>
    public string? InLog_Goods { get; set; }
    /// <summary>
    /// 入库记录 - 库位
    /// </summary>
    public List<string?> InLog_BinLocation { get; set; }
    /// <summary>
    /// 入库记录 - 数量
    /// </summary>
    public int? InLog_Count { get; set; }
    /// <summary>
    /// 入库记录 - 类型
    /// </summary>
    public string? InLog_InTypeInfo { get; set; }
    /// <summary>
    /// 入库记录 - Id
    /// </summary>
    public Guid InLog_Id { get; set; }
  }
}
