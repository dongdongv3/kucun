using System;

namespace WebApi.Code.Interface.InLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class InLogDeleteRequest : InLogDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
