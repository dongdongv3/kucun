using System;
using System.Collections.Generic;

namespace WebApi.Code.Interface.InLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class InLogAddDefaultValueResponse : InLogAddPageparameter
  {
    /// <summary>
    /// 入库时间
    /// </summary>
    public DateTime? CreateTime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    public string? Goods { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    public List<string?> BinLocation { get; set; }
    /// <summary>
    /// 数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 类型
    /// </summary>
    public string? InTypeInfo { get; set; }
  }
}
