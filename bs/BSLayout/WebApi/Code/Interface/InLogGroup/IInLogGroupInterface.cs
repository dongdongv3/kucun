using System;
using WebApi.Code.Interface.InLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.InLogGroup
{
  public interface IInLogGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult InLogAdd(InLogAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public InLogAddDefaultValueResponse InLogAdd_GetDefaultValue(InLogAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_goods_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_BinLocation_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogAdd_InTypeInfo_DataSource(InLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public InLogEditResponse InLogEdit_GetValue(InLogEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult InLogEdit(InLogEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_goods_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_BinLocation_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogEdit_InTypeInfo_DataSource(InLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public InLogDetailResponse InLogDetail_GetValue(InLogDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_goods_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_BinLocation_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> InLogDetail_InTypeInfo_DataSource(InLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult InLogDelete(InLogDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<InLogSearchResponse> InLogSearch(InLogSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public InLogSearchDefaultValueResponse InLogSearch_GetDefaultValue(InLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_goods_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_BinLocation_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> InLogSearch_InTypeInfo_View_DataSource(InLogSearchPageparameter request, UserInfo loginUser);

  }
}
