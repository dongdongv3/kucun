using System;
using WebApi.Code.Interface.InTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Code.Interface.InTypeGroup
{
  public interface IInTypeGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult InTypeAdd(InTypeAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public InTypeAddDefaultValueResponse InTypeAdd_GetDefaultValue(InTypeAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult InTypeDelete(InTypeDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<InTypeSearchResponse> InTypeSearch(InTypeSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public InTypeSearchDefaultValueResponse InTypeSearch_GetDefaultValue(InTypeSearchPageparameter request, UserInfo loginUser);

  }
}
