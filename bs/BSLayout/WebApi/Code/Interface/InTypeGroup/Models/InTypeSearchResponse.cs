using System;

namespace WebApi.Code.Interface.InTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class InTypeSearchResponse : InTypeSearchPageparameter
  {
    /// <summary>
    /// 入库类型 - 名称
    /// </summary>
    public string? InType_Name { get; set; }
    /// <summary>
    /// 入库类型 - Id
    /// </summary>
    public Guid InType_Id { get; set; }
  }
}
