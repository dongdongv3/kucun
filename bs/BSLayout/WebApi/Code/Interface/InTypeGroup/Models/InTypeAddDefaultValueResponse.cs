using System;

namespace WebApi.Code.Interface.InTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class InTypeAddDefaultValueResponse : InTypeAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
  }
}
