using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.InTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class InTypeAddRequest : InTypeAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    [MaxLength(50)]
    public string? Name { get; set; }
  }
}
