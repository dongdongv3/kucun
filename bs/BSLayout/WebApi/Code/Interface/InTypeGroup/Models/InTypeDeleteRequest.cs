using System;

namespace WebApi.Code.Interface.InTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class InTypeDeleteRequest : InTypeDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
