using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.BinLocationGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class BinLocationSearchQueryRequest_ValidateNever : BinLocationSearchQueryRequest
  {
  }
}
