using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.BinLocationGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class BinLocationEditUpdateRequest : BinLocationEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [MaxLength(50)]
    public string? Name { get; set; }
  }
}
