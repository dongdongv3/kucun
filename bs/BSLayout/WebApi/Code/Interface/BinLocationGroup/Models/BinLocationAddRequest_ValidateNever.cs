using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.BinLocationGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class BinLocationAddRequest_ValidateNever : BinLocationAddRequest
  {
  }
}
