using System;

namespace WebApi.Code.Interface.BinLocationGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class BinLocationAddDefaultValueResponse : BinLocationAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
  }
}
