using System;

namespace WebApi.Code.Interface.BinLocationGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class BinLocationSearchResponse : BinLocationSearchPageparameter
  {
    /// <summary>
    /// 库位 - 名称
    /// </summary>
    public string? BinLocation_Name { get; set; }
    /// <summary>
    /// 库位 - Id
    /// </summary>
    public Guid BinLocation_Id { get; set; }
  }
}
