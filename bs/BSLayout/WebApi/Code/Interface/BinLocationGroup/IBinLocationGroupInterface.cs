using System;
using WebApi.Code.Interface.BinLocationGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WebApi.Code.Interface.BinLocationGroup
{
  public interface IBinLocationGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult BinLocationAdd(BinLocationAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public BinLocationAddDefaultValueResponse BinLocationAdd_GetDefaultValue(BinLocationAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public BinLocationEditResponse BinLocationEdit_GetValue(BinLocationEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult BinLocationEdit(BinLocationEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public BinLocationDetailResponse BinLocationDetail_GetValue(BinLocationDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult BinLocationDelete(BinLocationDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<BinLocationSearchResponse> BinLocationSearch(BinLocationSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public BinLocationSearchDefaultValueResponse BinLocationSearch_GetDefaultValue(BinLocationSearchPageparameter request, UserInfo loginUser);

  }
}
