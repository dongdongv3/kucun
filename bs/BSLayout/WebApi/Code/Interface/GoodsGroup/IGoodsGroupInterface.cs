using System;
using WebApi.Code.Interface.GoodsGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WebApi.Code.Interface.GoodsGroup
{
  public interface IGoodsGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult GoodsAdd(GoodsAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public GoodsAddDefaultValueResponse GoodsAdd_GetDefaultValue(GoodsAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public GoodsEditResponse GoodsEdit_GetValue(GoodsEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult GoodsEdit(GoodsEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public GoodsDetailResponse GoodsDetail_GetValue(GoodsDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult GoodsDelete(GoodsDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<GoodsSearchResponse> GoodsSearch(GoodsSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public GoodsSearchDefaultValueResponse GoodsSearch_GetDefaultValue(GoodsSearchPageparameter request, UserInfo loginUser);

  }
}
