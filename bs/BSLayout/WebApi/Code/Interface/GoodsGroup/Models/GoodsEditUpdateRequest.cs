using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class GoodsEditUpdateRequest : GoodsEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [MaxLength(50)]
    public string? Name { get; set; }
    /// <summary>
    /// 库存数量
    /// </summary>
    [Range(0, 1000000)]
    public int? Count { get; set; }
  }
}
