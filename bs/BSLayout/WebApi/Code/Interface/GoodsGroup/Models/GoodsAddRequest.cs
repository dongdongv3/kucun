using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class GoodsAddRequest : GoodsAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    [MaxLength(50)]
    public string? Name { get; set; }
    /// <summary>
    /// 库存数量
    /// </summary>
    [Range(0, 1000000)]
    public int? Count { get; set; }
  }
}
