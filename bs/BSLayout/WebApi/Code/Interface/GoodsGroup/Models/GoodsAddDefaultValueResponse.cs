using System;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class GoodsAddDefaultValueResponse : GoodsAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 库存数量
    /// </summary>
    public int? Count { get; set; }
  }
}
