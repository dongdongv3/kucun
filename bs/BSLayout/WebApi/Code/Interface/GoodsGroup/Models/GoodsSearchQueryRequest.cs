using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class GoodsSearchQueryRequest : GoodsSearchPageparameter
  {
    /// <summary>
    /// 分页大小
    /// </summary>
    public int PageSize { get; set; }
    /// <summary>
    /// 当前页索引
    /// </summary>
    public int PageIndex { get; set; }
    /// <summary>
    /// 排序方式 asc/desc
    /// </summary>
    public string SortType { get; set; }
    /// <summary>
    /// 排序列名称
    /// </summary>
    public string SortName { get; set; }
  }
}
