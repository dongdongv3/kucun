using System;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class GoodsDetailResponse : GoodsDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 库存数量
    /// </summary>
    public int? Count { get; set; }
  }
}
