using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class GoodsAddRequest_ValidateNever : GoodsAddRequest
  {
  }
}
