using System;

namespace WebApi.Code.Interface.GoodsGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class GoodsSearchResponse : GoodsSearchPageparameter
  {
    /// <summary>
    /// 商品 - 名称
    /// </summary>
    public string? Goods_Name { get; set; }
    /// <summary>
    /// 商品 - 库存数量
    /// </summary>
    public int? Goods_Count { get; set; }
    /// <summary>
    /// 商品 - Id
    /// </summary>
    public Guid Goods_Id { get; set; }
  }
}
