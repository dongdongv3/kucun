using System;

namespace WebApi.Code.Interface.OutTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class OutTypeAddDefaultValueResponse : OutTypeAddPageparameter
  {
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
  }
}
