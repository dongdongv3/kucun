using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.OutTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class OutTypeSearchQueryRequest_ValidateNever : OutTypeSearchQueryRequest
  {
  }
}
