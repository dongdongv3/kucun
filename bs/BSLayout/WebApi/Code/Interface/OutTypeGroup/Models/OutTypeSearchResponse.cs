using System;

namespace WebApi.Code.Interface.OutTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class OutTypeSearchResponse : OutTypeSearchPageparameter
  {
    /// <summary>
    /// 出库类型 - 名称
    /// </summary>
    public string? OutType_Name { get; set; }
    /// <summary>
    /// 出库类型 - Id
    /// </summary>
    public Guid OutType_Id { get; set; }
  }
}
