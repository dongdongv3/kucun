using System;
using WebApi.Code.Interface.OutTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Code.Interface.OutTypeGroup
{
  public interface IOutTypeGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult OutTypeAdd(OutTypeAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OutTypeAddDefaultValueResponse OutTypeAdd_GetDefaultValue(OutTypeAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult OutTypeDelete(OutTypeDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<OutTypeSearchResponse> OutTypeSearch(OutTypeSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public OutTypeSearchDefaultValueResponse OutTypeSearch_GetDefaultValue(OutTypeSearchPageparameter request, UserInfo loginUser);

  }
}
