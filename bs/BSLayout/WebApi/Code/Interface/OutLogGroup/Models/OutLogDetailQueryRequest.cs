using System;

namespace WebApi.Code.Interface.OutLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class OutLogDetailQueryRequest : OutLogDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
