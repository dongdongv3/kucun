using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Interface.OutLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class OutLogEditUpdateRequest : OutLogEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 出库时间
    /// </summary>
    public DateTime? Createtime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    public string? Goods { get; set; }
    /// <summary>
    /// 出库数量
    /// </summary>
    [Range(0, 1000000)]
    public int? Count { get; set; }
    /// <summary>
    /// 出库类型
    /// </summary>
    public string? OutTypeInfo { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    public List<string?> BinLocation { get; set; }
  }
}
