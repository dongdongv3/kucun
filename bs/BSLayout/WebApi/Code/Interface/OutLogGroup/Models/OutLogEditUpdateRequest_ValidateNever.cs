using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.OutLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class OutLogEditUpdateRequest_ValidateNever : OutLogEditUpdateRequest
  {
  }
}
