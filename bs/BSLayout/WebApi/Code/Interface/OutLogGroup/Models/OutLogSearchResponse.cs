using System;
using System.Collections.Generic;

namespace WebApi.Code.Interface.OutLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class OutLogSearchResponse : OutLogSearchPageparameter
  {
    /// <summary>
    /// 出库记录 - 出库时间
    /// </summary>
    public DateTime? OutLog_Createtime { get; set; }
    /// <summary>
    /// 出库记录 - 商品
    /// </summary>
    public string? OutLog_Goods { get; set; }
    /// <summary>
    /// 出库记录 - 出库数量
    /// </summary>
    public int? OutLog_Count { get; set; }
    /// <summary>
    /// 出库记录 - 出库类型
    /// </summary>
    public string? OutLog_OutTypeInfo { get; set; }
    /// <summary>
    /// 出库记录 - 库位
    /// </summary>
    public List<string?> OutLog_BinLocation { get; set; }
    /// <summary>
    /// 出库记录 - Id
    /// </summary>
    public Guid OutLog_Id { get; set; }
  }
}
