using System;
using System.Collections.Generic;

namespace WebApi.Code.Interface.OutLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class OutLogDetailResponse : OutLogDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 出库时间
    /// </summary>
    public DateTime? Createtime { get; set; }
    /// <summary>
    /// 商品
    /// </summary>
    public string? Goods { get; set; }
    /// <summary>
    /// 出库数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 出库类型
    /// </summary>
    public string? OutTypeInfo { get; set; }
    /// <summary>
    /// 库位
    /// </summary>
    public List<string?> BinLocation { get; set; }
  }
}
