using System;
using WebApi.Code.Interface.OutLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.OutLogGroup
{
  public interface IOutLogGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult OutLogAdd(OutLogAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OutLogAddDefaultValueResponse OutLogAdd_GetDefaultValue(OutLogAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_goods_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_OutTypeInfo_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogAdd_BinLocation_DataSource(OutLogAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public OutLogEditResponse OutLogEdit_GetValue(OutLogEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult OutLogEdit(OutLogEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_goods_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_OutTypeInfo_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogEdit_BinLocation_DataSource(OutLogEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public OutLogDetailResponse OutLogDetail_GetValue(OutLogDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_goods_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_OutTypeInfo_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    public List<KVDataInfo> OutLogDetail_BinLocation_DataSource(OutLogDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult OutLogDelete(OutLogDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<OutLogSearchResponse> OutLogSearch(OutLogSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public OutLogSearchDefaultValueResponse OutLogSearch_GetDefaultValue(OutLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_goods_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '出库类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_OutTypeInfo_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> OutLogSearch_BinLocation_View_DataSource(OutLogSearchPageparameter request, UserInfo loginUser);

  }
}
