using System;
using WebApi.Code.Interface.EmployeeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.EmployeeGroup
{
  public interface IEmployeeGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1AddEmployee(Employee_AccountAndPasswordAuth1AddEmployeeRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_AccountAndPasswordAuth1AddEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue(Employee_AccountAndPasswordAuth1AddEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1AddEmployeeRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_AccountAndPasswordAuth1EditEmployeeResponse Employee_AccountAndPasswordAuth1EditEmployee_GetValue(Employee_AccountAndPasswordAuth1EditEmployeeQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1EditEmployee(Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_AccountAndPasswordAuth1Delete(Employee_AccountAndPasswordAuth1DeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_AccountAndPasswordAuth1DetailResponse Employee_AccountAndPasswordAuth1Detail_GetValue(Employee_AccountAndPasswordAuth1DetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource(Employee_AccountAndPasswordAuth1DetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_AccountAndPasswordAuth1SearchEmployeeResponse> Employee_AccountAndPasswordAuth1SearchEmployee(Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_AccountAndPasswordAuth1SearchEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue(Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource(Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource(Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_UserRoleMapAddEmployee(Employee_UserRoleMapAddEmployeeRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_UserRoleMapAddEmployeeDefaultValueResponse Employee_UserRoleMapAddEmployee_GetDefaultValue(Employee_UserRoleMapAddEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource(Employee_UserRoleMapAddEmployeeRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource(Employee_UserRoleMapAddEmployeeRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_UserRoleMapAddEmployee_Role(Employee_UserRoleMapAddEmployee_RoleRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_UserRoleMapAddEmployee_RoleDefaultValueResponse Employee_UserRoleMapAddEmployee_Role_GetDefaultValue(Employee_UserRoleMapAddEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource(Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource(Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_UserRoleMapEditEmployeeResponse Employee_UserRoleMapEditEmployee_GetValue(Employee_UserRoleMapEditEmployeeQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_UserRoleMapEditEmployee(Employee_UserRoleMapEditEmployeeUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource(Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource(Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_UserRoleMapEditEmployee_RoleResponse Employee_UserRoleMapEditEmployee_Role_GetValue(Employee_UserRoleMapEditEmployee_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_UserRoleMapEditEmployee_Role(Employee_UserRoleMapEditEmployee_RoleUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource(Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource(Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_UserRoleMapDelete(Employee_UserRoleMapDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_UserRoleMapDetailResponse Employee_UserRoleMapDetail_GetValue(Employee_UserRoleMapDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployeeId_DataSource(Employee_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource(Employee_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_UserRoleMapSearchEmployeeResponse> Employee_UserRoleMapSearchEmployee(Employee_UserRoleMapSearchEmployeeQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_UserRoleMapSearchEmployeeDefaultValueResponse Employee_UserRoleMapSearchEmployee_GetDefaultValue(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource(Employee_UserRoleMapSearchEmployeePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource(Employee_UserRoleMapSearchEmployeeQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_UserRoleMapSearchEmployee_RoleResponse> Employee_UserRoleMapSearchEmployee_Role(Employee_UserRoleMapSearchEmployee_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_UserRoleMapSearchEmployee_RoleDefaultValueResponse Employee_UserRoleMapSearchEmployee_Role_GetDefaultValue(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(Employee_UserRoleMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(Employee_UserRoleMapSearchEmployee_RoleQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Role(Employee_RoleAuthMapAddEmployee_RoleRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAuthMapAddEmployee_RoleDefaultValueResponse Employee_RoleAuthMapAddEmployee_Role_GetDefaultValue(Employee_RoleAuthMapAddEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Auth(Employee_RoleAuthMapAddEmployee_AuthRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAuthMapAddEmployee_AuthDefaultValueResponse Employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue(Employee_RoleAuthMapAddEmployee_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleAuthMapEditEmployee_RoleResponse Employee_RoleAuthMapEditEmployee_Role_GetValue(Employee_RoleAuthMapEditEmployee_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Role(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleAuthMapEditEmployee_AuthResponse Employee_RoleAuthMapEditEmployee_Auth_GetValue(Employee_RoleAuthMapEditEmployee_AuthQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Auth(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_RoleAuthMapDelete(Employee_RoleAuthMapDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_RoleAuthMapDetailResponse Employee_RoleAuthMapDetail_GetValue(Employee_RoleAuthMapDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource(Employee_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource(Employee_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_RoleResponse> Employee_RoleAuthMapSearchEmployee_Role(Employee_RoleAuthMapSearchEmployee_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleAuthMapSearchEmployee_RoleDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource(Employee_RoleAuthMapSearchEmployee_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(Employee_RoleAuthMapSearchEmployee_RoleQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_AuthResponse> Employee_RoleAuthMapSearchEmployee_Auth(Employee_RoleAuthMapSearchEmployee_AuthQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleAuthMapSearchEmployee_AuthDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource(Employee_RoleAuthMapSearchEmployee_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource(Employee_RoleAuthMapSearchEmployee_AuthQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_AuthAdd(Employee_AuthAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_AuthAddDefaultValueResponse Employee_AuthAdd_GetDefaultValue(Employee_AuthAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_AuthEditResponse Employee_AuthEdit_GetValue(Employee_AuthEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_AuthEdit(Employee_AuthEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_AuthDelete(Employee_AuthDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_AuthDetailResponse Employee_AuthDetail_GetValue(Employee_AuthDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_AuthSearchResponse> Employee_AuthSearch(Employee_AuthSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_AuthSearchDefaultValueResponse Employee_AuthSearch_GetDefaultValue(Employee_AuthSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult Employee_RoleAdd(Employee_RoleAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public Employee_RoleAddDefaultValueResponse Employee_RoleAdd_GetDefaultValue(Employee_RoleAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public Employee_RoleEditResponse Employee_RoleEdit_GetValue(Employee_RoleEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult Employee_RoleEdit(Employee_RoleEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult Employee_RoleDelete(Employee_RoleDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public Employee_RoleDetailResponse Employee_RoleDetail_GetValue(Employee_RoleDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<Employee_RoleSearchResponse> Employee_RoleSearch(Employee_RoleSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public Employee_RoleSearchDefaultValueResponse Employee_RoleSearch_GetDefaultValue(Employee_RoleSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult EmployeeAdd(EmployeeAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public EmployeeAddDefaultValueResponse EmployeeAdd_GetDefaultValue(EmployeeAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public EmployeeEditResponse EmployeeEdit_GetValue(EmployeeEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult EmployeeEdit(EmployeeEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult EmployeeDelete(EmployeeDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public EmployeeDetailResponse EmployeeDetail_GetValue(EmployeeDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<EmployeeSearchResponse> EmployeeSearch(EmployeeSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public EmployeeSearchDefaultValueResponse EmployeeSearch_GetDefaultValue(EmployeeSearchPageparameter request, UserInfo loginUser);

  }
}
