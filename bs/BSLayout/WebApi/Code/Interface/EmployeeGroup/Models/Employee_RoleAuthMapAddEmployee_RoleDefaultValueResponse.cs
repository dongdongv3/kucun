using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapAddEmployee_RoleDefaultValueResponse : Employee_RoleAuthMapAddEmployee_RolePageparameter
  {
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefEmployee_AuthId { get; set; }
  }
}
