using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_AccountAndPasswordAuth1AddEmployeeDefaultValueResponse : Employee_AccountAndPasswordAuth1AddEmployeePageparameter
  {
    /// <summary>
    /// 登录账户
    /// </summary>
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
  }
}
