using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_AccountAndPasswordAuth1EditEmployeeResponse : Employee_AccountAndPasswordAuth1EditEmployeePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 登录账户
    /// </summary>
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
    /// <summary>
    /// 员工-用户信息-显示名
    /// </summary>
    public string RefEmployeeId { get; set; }
  }
}
