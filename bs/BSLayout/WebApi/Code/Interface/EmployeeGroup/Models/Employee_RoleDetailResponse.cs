using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleDetailResponse : Employee_RoleDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色名称
    /// </summary>
    public string? RoleName { get; set; }
  }
}
