using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class EmployeeEditUpdateRequest : EmployeeEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
    /// <summary>
    /// 显示名
    /// </summary>
    [MaxLength(50)]
    public string? DisplayName { get; set; }
  }
}
