using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_UserRoleMapDeleteRequest : Employee_UserRoleMapDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
