using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class Employee_AuthDetailQueryRequest : Employee_AuthDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
