using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_AccountAndPasswordAuth1AddEmployeeRequest : Employee_AccountAndPasswordAuth1AddEmployeePageparameter
  {
    /// <summary>
    /// 登录账户
    /// </summary>
    [MaxLength(50)]
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
  }
}
