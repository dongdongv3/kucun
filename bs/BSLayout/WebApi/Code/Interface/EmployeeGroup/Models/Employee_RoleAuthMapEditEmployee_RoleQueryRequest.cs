using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapEditEmployee_RoleQueryRequest : Employee_RoleAuthMapEditEmployee_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
