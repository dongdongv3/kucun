using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapEditEmployee_RoleResponse : Employee_RoleAuthMapEditEmployee_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefEmployee_AuthId { get; set; }
  }
}
