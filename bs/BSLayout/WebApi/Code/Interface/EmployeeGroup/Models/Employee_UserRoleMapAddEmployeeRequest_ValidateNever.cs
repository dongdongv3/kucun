using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class Employee_UserRoleMapAddEmployeeRequest_ValidateNever : Employee_UserRoleMapAddEmployeeRequest
  {
  }
}
