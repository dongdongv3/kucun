using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleSearchResponse : Employee_RoleSearchPageparameter
  {
    /// <summary>
    /// 角色 - 角色名称
    /// </summary>
    public string? Employee_Role_RoleName { get; set; }
    /// <summary>
    /// 角色 - Id
    /// </summary>
    public Guid Employee_Role_Id { get; set; }
  }
}
