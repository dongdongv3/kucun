using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever : Employee_UserRoleMapEditEmployee_RoleUpdateRequest
  {
  }
}
