using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAddRequest : Employee_RoleAddPageparameter
  {
    /// <summary>
    /// 角色名称
    /// </summary>
    [MaxLength(50)]
    public string? RoleName { get; set; }
  }
}
