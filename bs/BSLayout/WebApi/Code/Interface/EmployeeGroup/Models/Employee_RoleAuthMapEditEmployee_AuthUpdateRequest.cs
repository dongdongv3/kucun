using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapEditEmployee_AuthUpdateRequest : Employee_RoleAuthMapEditEmployee_AuthPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
  }
}
