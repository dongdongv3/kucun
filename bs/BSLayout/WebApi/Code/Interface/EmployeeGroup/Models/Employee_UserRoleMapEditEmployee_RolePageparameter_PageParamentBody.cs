using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// </summary>
  public class Employee_UserRoleMapEditEmployee_RolePageparameter_PageParamentBody
  {
    public Guid Id { get; set; }
    public Guid RefEmployee_RoleId { get; set; }
  }
}
