using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_UserRoleMapSearchEmployee_RoleResponse : Employee_UserRoleMapSearchEmployee_RolePageparameter
  {
    /// <summary>
    /// 用户角色映射表 - Id
    /// </summary>
    public Guid Employee_UserRoleMap_Id { get; set; }
    /// <summary>
    /// 用户角色映射表 - 员工-用户信息-显示名
    /// </summary>
    public string Employee_UserRoleMap_RefEmployeeId { get; set; }
    /// <summary>
    /// 用户角色映射表 - 角色-角色名称
    /// </summary>
    public Guid Employee_UserRoleMap_RefEmployee_RoleId { get; set; }
  }
}
