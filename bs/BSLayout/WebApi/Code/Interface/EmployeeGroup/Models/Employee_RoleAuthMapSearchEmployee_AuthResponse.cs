using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapSearchEmployee_AuthResponse : Employee_RoleAuthMapSearchEmployee_AuthPageparameter
  {
    /// <summary>
    /// 角色权限映射表 - Id
    /// </summary>
    public Guid Employee_RoleAuthMap_Id { get; set; }
    /// <summary>
    /// 角色权限映射表 - 角色-角色名称
    /// </summary>
    public Guid Employee_RoleAuthMap_RefEmployee_RoleId { get; set; }
    /// <summary>
    /// 角色权限映射表 - 权限-权限名称
    /// </summary>
    public Guid Employee_RoleAuthMap_RefEmployee_AuthId { get; set; }
  }
}
