using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// </summary>
  public class Employee_RoleAuthMapEditEmployee_AuthPageparameter_PageParamentBody
  {
    public Guid Id { get; set; }
    public Guid RefEmployee_AuthId { get; set; }
  }
}
