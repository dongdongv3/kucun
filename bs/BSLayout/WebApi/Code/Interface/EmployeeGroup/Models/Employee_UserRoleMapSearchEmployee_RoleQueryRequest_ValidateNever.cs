using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class Employee_UserRoleMapSearchEmployee_RoleQueryRequest_ValidateNever : Employee_UserRoleMapSearchEmployee_RoleQueryRequest
  {
  }
}
