using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class EmployeeDeleteRequest : EmployeeDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
  }
}
