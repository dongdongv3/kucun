using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class Employee_UserRoleMapEditEmployee_RoleUpdateRequest : Employee_UserRoleMapEditEmployee_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 员工-用户信息-显示名
    /// </summary>
    public string RefEmployeeId { get; set; }
  }
}
