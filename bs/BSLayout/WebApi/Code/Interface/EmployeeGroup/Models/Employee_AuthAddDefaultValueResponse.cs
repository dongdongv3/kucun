using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_AuthAddDefaultValueResponse : Employee_AuthAddPageparameter
  {
    /// <summary>
    /// 权限名称
    /// </summary>
    public string? AuthName { get; set; }
  }
}
