using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapEditEmployee_RoleUpdateRequest : Employee_RoleAuthMapEditEmployee_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefEmployee_AuthId { get; set; }
  }
}
