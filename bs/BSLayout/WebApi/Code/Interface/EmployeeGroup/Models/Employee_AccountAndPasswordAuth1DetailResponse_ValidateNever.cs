using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class Employee_AccountAndPasswordAuth1DetailResponse_ValidateNever : Employee_AccountAndPasswordAuth1DetailResponse
  {
  }
}
