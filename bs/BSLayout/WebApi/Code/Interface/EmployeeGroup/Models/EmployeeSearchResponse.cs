using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class EmployeeSearchResponse : EmployeeSearchPageparameter
  {
    /// <summary>
    /// 员工-用户信息 - 显示名
    /// </summary>
    public string? Employee_DisplayName { get; set; }
    /// <summary>
    /// 员工-用户信息 - Id
    /// </summary>
    public string Employee_Id { get; set; }
  }
}
