using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_AccountAndPasswordAuth1SearchEmployeeResponse : Employee_AccountAndPasswordAuth1SearchEmployeePageparameter
  {
    /// <summary>
    /// 账户密码认证1-认证通道 - 登录账户
    /// </summary>
    public string? Employee_AccountAndPasswordAuth1_LoginID { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - 密码
    /// </summary>
    public string? Employee_AccountAndPasswordAuth1_Pwd { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - Id
    /// </summary>
    public Guid Employee_AccountAndPasswordAuth1_Id { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - 员工-用户信息-显示名
    /// </summary>
    public string Employee_AccountAndPasswordAuth1_RefEmployeeId { get; set; }
  }
}
