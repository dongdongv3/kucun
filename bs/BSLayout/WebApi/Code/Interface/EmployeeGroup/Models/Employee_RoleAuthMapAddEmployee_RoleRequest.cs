using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapAddEmployee_RoleRequest : Employee_RoleAuthMapAddEmployee_RolePageparameter
  {
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefEmployee_AuthId { get; set; }
  }
}
