using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_AuthSearchResponse : Employee_AuthSearchPageparameter
  {
    /// <summary>
    /// 权限 - 权限名称
    /// </summary>
    public string? Employee_Auth_AuthName { get; set; }
    /// <summary>
    /// 权限 - Id
    /// </summary>
    public Guid Employee_Auth_Id { get; set; }
  }
}
