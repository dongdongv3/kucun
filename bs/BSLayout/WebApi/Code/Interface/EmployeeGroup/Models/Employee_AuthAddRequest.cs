using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_AuthAddRequest : Employee_AuthAddPageparameter
  {
    /// <summary>
    /// 权限名称
    /// </summary>
    [MaxLength(255)]
    public string? AuthName { get; set; }
  }
}
