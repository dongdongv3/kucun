using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAddDefaultValueResponse : Employee_RoleAddPageparameter
  {
    /// <summary>
    /// 角色名称
    /// </summary>
    public string? RoleName { get; set; }
  }
}
