using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class EmployeeAddRequest : EmployeeAddPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    [MaxLength(50)]
    public string Id { get; set; }
    /// <summary>
    /// 显示名
    /// </summary>
    [MaxLength(50)]
    public string? DisplayName { get; set; }
  }
}
