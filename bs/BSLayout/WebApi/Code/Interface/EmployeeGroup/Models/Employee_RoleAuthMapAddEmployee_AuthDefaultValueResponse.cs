using System;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class Employee_RoleAuthMapAddEmployee_AuthDefaultValueResponse : Employee_RoleAuthMapAddEmployee_AuthPageparameter
  {
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
  }
}
