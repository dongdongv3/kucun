using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_UserRoleMapAddEmployeeRequest : Employee_UserRoleMapAddEmployeePageparameter
  {
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefEmployee_RoleId { get; set; }
  }
}
