using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.EmployeeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class Employee_UserRoleMapAddEmployee_RoleRequest : Employee_UserRoleMapAddEmployee_RolePageparameter
  {
    /// <summary>
    /// 员工-用户信息-显示名
    /// </summary>
    public string RefEmployeeId { get; set; }
  }
}
