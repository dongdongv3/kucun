using System;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Code.Common.Authentication;

namespace WebApi.Code.Common
{
  /// <summary>
  /// 认证模块
  /// 认证后可读取user信息
  /// </summary>
  public class AuthenticationFilterAttribute : Microsoft.AspNetCore.Authorization.AuthorizeAttribute, IAsyncAuthorizationFilter
  {
    public AuthenticationFilterAttribute() : base(IdentityServer4.IdentityServerConstants.LocalApi.PolicyName)
    {
    }
    public AuthenticationFilterAttribute(string policy) : base(policy)
    {
    }
    public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
    {
      var iuserService = context.HttpContext.RequestServices.GetService(typeof(IUserReadInterface)) as IUserReadInterface;

      var mainuser = await iuserService.GetUserByID(context.HttpContext.User.Claims.First(x => x.Type == "sub").Value);
      //设置上下文
      context.HttpContext.UserInfoSet(mainuser);
    }
  }
}
