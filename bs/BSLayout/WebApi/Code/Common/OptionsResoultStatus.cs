using System;

namespace WebApi.Code.Common
{
  public enum OptionsResoultStatus
  {

    /// <summary>
    /// 成功
    /// </summary>
    succeed = 1,
    /// <summary>
    /// 失败
    /// </summary>
    failed = 2

  }
}
