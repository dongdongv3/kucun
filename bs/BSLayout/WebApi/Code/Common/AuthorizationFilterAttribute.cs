using System;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Code.Common
{
  /// <summary>
  /// 请求角色或者权限
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
  public class AuthorizationFilterAttribute : Attribute, IAsyncActionFilter
  {
    /// <summary>
    /// 权限验证
    /// </summary>
    /// <param name="Name">权限或角色名称</param>
    /// <param name="type">验证类型</param>
    public AuthorizationFilterAttribute(string Name, Vtype type)
    {
      if (type == Vtype.角色)
      {
        Role = Name;
      }
      else
      {
        Auth = Name;
      }
    }
    /// <summary>
    /// 请求的角色名称
    /// </summary>
    public string Role { get; set; }
    /// <summary>
    /// 请求的权限名称
    /// </summary>
    public string Auth { get; set; }
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
      var user = context.HttpContext.UserInfo();
      if (user == null)
      {
        throw new AuthException("用户未登录");
      }
      if (!string.IsNullOrEmpty(Role))
      {
        if ((user.Roles.All(x => x.Key != Role)))
        {
          throw new AuthException($"请求的角色'{Role}'不存在");
        }
      }
      if (!string.IsNullOrEmpty(Auth))
      {
        if ((user.Auths.All(x => x.Key != Auth)))
        {
          throw new AuthException($"请求的权限'{Auth}'不存在");
        }
      }
      await next.Invoke();
    }

    public enum Vtype
    {
      角色 = 1,
      权限 = 2
    }
  }
}
