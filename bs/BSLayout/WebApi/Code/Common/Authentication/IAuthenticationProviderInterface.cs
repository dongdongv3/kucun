using System;
using System.Threading.Tasks;

namespace WebApi.Code.Common.Authentication
{
  /// <summary>
  /// 认证提供程序
  /// </summary>
  public interface IAuthenticationProviderInterface
  {
    public LoginType Type { get; }
    /// <summary>
    /// 根据登录信息查询用户ID
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public Task<string> GetUserID(string Key);
    /// <summary>
    /// 验证登录
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public Task<bool> Validation(params string[] param);
  }
}
