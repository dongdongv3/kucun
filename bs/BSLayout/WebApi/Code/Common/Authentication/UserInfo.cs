using System;
using System.Collections.Generic;

namespace WebApi.Code.Common.Authentication
{
  public class UserInfo
  {
    /// <summary>
    /// 显示名
    /// </summary>
    public string? DisplayName { get; set; }
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
    public Dictionary<string, string> Roles { get; set; }
    public Dictionary<string, string> Auths { get; set; }
  }
}
