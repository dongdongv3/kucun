using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Code.Common.Authentication
{
  /// <summary>
  /// 账户信息提供程序，负责提供认证和授权相关服务
  /// </summary>
  public interface IUserReadInterface
  {
    public Task<UserInfo> GetUserByID(string userID);


    /// <summary>
    /// 根据类型获取登录提供程序
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public IAuthenticationProviderInterface GetAuthenticationProvider(LoginType type);

    //
    // Summary:
    //     Finds the user by external provider.
    //
    // Parameters:
    //   provider:
    //     The provider.
    //
    //   userId:
    //     The user identifier.
    public Task<UserInfo> FindByExternalProvider(string provider, string userId);

    //
    // Summary:
    //     Automatically provisions a user.
    //
    // Parameters:
    //   provider:
    //     The provider.
    //
    //   userId:
    //     The user identifier.
    //
    //   claims:
    //     The claims.
    public Task<UserInfo> AutoProvisionUser(string provider, string userId, List<Claim> claims);
  }
}
