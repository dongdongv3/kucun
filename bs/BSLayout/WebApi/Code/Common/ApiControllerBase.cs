using System;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Code.Common
{
  [ExceptionFilter]
  [ResultFilter]
  [ApiController]
  [Route("api/[controller]/[action]")]
  public class ApiControllerBase : ControllerBase
  {
  }
}
