using System;

namespace WebApi.Code.Common
{
  public class ViewException : System.Exception
  {
    public int Code = -1;
    public ViewException(string mess) : base(mess)
    {

    }
  }
}
