using System;

namespace WebApi.Code.Common
{
  public class AuthException : System.Exception
  {
    public int Code = -2;
    public AuthException(string mess) : base(mess)
    {

    }
  }
}
