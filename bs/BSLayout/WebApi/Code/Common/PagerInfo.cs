using System;
using System.Collections.Generic;

namespace WebApi.Code.Common
{
  public class PagerInfo<T>
  {
    /// <summary>
    /// 分页大小
    /// </summary>
    public int PageSize { get; set; }
    /// <summary>
    /// 当前页索引
    /// </summary>
    public int PageIndex { get; set; }
    /// <summary>
    /// 分页item数据
    /// </summary>
    public List<T> Data { get; set; }
    /// <summary>
    /// 总条目数
    /// </summary>
    public int Total { get; set; }
  }
}
