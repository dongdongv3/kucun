using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Code.Common
{
  public class TreeDataInfo
  {
    /// <summary>
    /// 存储值
    /// </summary>
    public string Value { get; set; }
    /// <summary>
    /// 显示值
    /// </summary>
    public string Label { get; set; }
    /// <summary>
    /// 子项
    /// </summary>
    public List<TreeDataInfo> Children { get; set; }

    public static List<TreeDataInfo> Read(List<TreeDataItem> treeDataItems, string rid)
    {
      var tmp = new List<TreeDataInfo>();
      treeDataItems.Where(x => x.rid == rid).ToList().ForEach(x =>
      {
        var one = new TreeDataInfo
        {
          Label = x.name,
          Value = x.id,
        };
        one.Children = Read(treeDataItems, x.id);
        tmp.Add(one);
      });
      return tmp;
    }

    public class TreeDataItem
    {
      public string id { get; set; }
      public string rid { get; set; }
      public string name { get; set; }
    }

  }
}
