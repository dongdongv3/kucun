using System;
using System.Collections.Generic;

namespace WebApi.Code.Common
{
  public class CommonDataSource
  {
    /// <summary>
    /// 预设置权限列表-自动生成
    /// </summary>
    public static List<KeyValuePair<string, string>> AuthList_Auto_Employee()
    {
      var output = new List<KeyValuePair<string, string>>();
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/添加", "/EmployeeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/编辑", "/EmployeeGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/删除", "/EmployeeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/详情", "/EmployeeGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/EmployeeGroup/查询", "/EmployeeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/InTypeGroup/添加", "/InTypeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/InTypeGroup/删除", "/InTypeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/InTypeGroup/查询", "/InTypeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/OutTypeGroup/添加", "/OutTypeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/OutTypeGroup/删除", "/OutTypeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/OutTypeGroup/查询", "/OutTypeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/GoodsGroup/添加", "/GoodsGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/GoodsGroup/编辑", "/GoodsGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/GoodsGroup/详情", "/GoodsGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/GoodsGroup/删除", "/GoodsGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/GoodsGroup/查询", "/GoodsGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/BinLocationGroup/添加", "/BinLocationGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/BinLocationGroup/编辑", "/BinLocationGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/BinLocationGroup/详情", "/BinLocationGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/BinLocationGroup/删除", "/BinLocationGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/BinLocationGroup/查询", "/BinLocationGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/OutLogGroup/添加", "/OutLogGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/OutLogGroup/编辑", "/OutLogGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/OutLogGroup/详情", "/OutLogGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/OutLogGroup/删除", "/OutLogGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/OutLogGroup/查询", "/OutLogGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/InLogGroup/添加", "/InLogGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/InLogGroup/编辑", "/InLogGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/InLogGroup/详情", "/InLogGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/InLogGroup/删除", "/InLogGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/InLogGroup/查询", "/InLogGroup/查询"));
      return output;
    }
  }
}
