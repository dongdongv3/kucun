using System;

namespace WebApi.Code.Common.PwdProvider
{
  /// <summary>
  /// 空加密服务
  /// </summary>
  public class NoneEncrypt : IPWDEncryptInterface
  {
    public string Encrypt(string key)
    {
      return key;
    }
  }
}
