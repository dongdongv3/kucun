using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Common.ElModels
{
  public class timeRange_Element
  {
    public string? _begin { get; set; }
    public string? _end { get; set; }
  }
}
