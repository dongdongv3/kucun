using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Code.Data;

namespace WebApi
{
  public class Program
  {
    public static string[] Args = null;
    public static void Main(string[] args)
    {
      Args = args;
      var ihost = CreateHostBuilder(args).Build();
      var scope = ihost.Services.CreateScope();
      {
        var db = scope.ServiceProvider.GetRequiredService<InventoryManagementSystemDB>();
        db.Database.Migrate();
        InitHelper.Run(args, scope);
      }
      ihost.Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder.UseStartup<Startup>();
            });
  }
}
