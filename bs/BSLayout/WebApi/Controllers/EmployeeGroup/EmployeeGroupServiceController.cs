using System;
using WebApi.Code.Interface.EmployeeGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.EmployeeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.EmployeeGroup
{
  [Route("api/[controller]/[action]")]
  public class EmployeeGroupServiceController : ApiControllerBase
  {
    public readonly IEmployeeGroupInterface _EmployeeGroupService;

    public EmployeeGroupServiceController(IEmployeeGroupInterface _EmployeeGroupService)
    {
      this._EmployeeGroupService = _EmployeeGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AccountAndPasswordAuth1AddEmployee([FromBody] Employee_AccountAndPasswordAuth1AddEmployeeRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1AddEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AccountAndPasswordAuth1AddEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue([FromBody] Employee_AccountAndPasswordAuth1AddEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource([FromQuery] Employee_AccountAndPasswordAuth1AddEmployeeRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AccountAndPasswordAuth1EditEmployeeResponse Employee_AccountAndPasswordAuth1EditEmployee_GetValue([FromBody] Employee_AccountAndPasswordAuth1EditEmployeeQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1EditEmployee_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AccountAndPasswordAuth1EditEmployee([FromBody] Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1EditEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource([FromQuery] Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AccountAndPasswordAuth1Delete([FromBody] Employee_AccountAndPasswordAuth1DeleteRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1Delete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AccountAndPasswordAuth1DetailResponse Employee_AccountAndPasswordAuth1Detail_GetValue([FromBody] Employee_AccountAndPasswordAuth1DetailQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1Detail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource([FromQuery] Employee_AccountAndPasswordAuth1DetailResponse_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_AccountAndPasswordAuth1SearchEmployeeResponse> Employee_AccountAndPasswordAuth1SearchEmployee([FromBody] Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1SearchEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AccountAndPasswordAuth1SearchEmployeeDefaultValueResponse Employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue([FromBody] Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource([FromQuery] Employee_AccountAndPasswordAuth1SearchEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource([FromQuery] Employee_AccountAndPasswordAuth1SearchEmployeeQueryRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_UserRoleMapAddEmployee([FromBody] Employee_UserRoleMapAddEmployeeRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapAddEmployeeDefaultValueResponse Employee_UserRoleMapAddEmployee_GetDefaultValue([FromBody] Employee_UserRoleMapAddEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource([FromQuery] Employee_UserRoleMapAddEmployeeRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource([FromQuery] Employee_UserRoleMapAddEmployeeRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_UserRoleMapAddEmployee_Role([FromBody] Employee_UserRoleMapAddEmployee_RoleRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapAddEmployee_RoleDefaultValueResponse Employee_UserRoleMapAddEmployee_Role_GetDefaultValue([FromBody] Employee_UserRoleMapAddEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource([FromQuery] Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource([FromQuery] Employee_UserRoleMapAddEmployee_RoleRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapEditEmployeeResponse Employee_UserRoleMapEditEmployee_GetValue([FromBody] Employee_UserRoleMapEditEmployeeQueryRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_UserRoleMapEditEmployee([FromBody] Employee_UserRoleMapEditEmployeeUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource([FromQuery] Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource([FromQuery] Employee_UserRoleMapEditEmployeeUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapEditEmployee_RoleResponse Employee_UserRoleMapEditEmployee_Role_GetValue([FromBody] Employee_UserRoleMapEditEmployee_RoleQueryRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_Role_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_UserRoleMapEditEmployee_Role([FromBody] Employee_UserRoleMapEditEmployee_RoleUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource([FromQuery] Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource([FromQuery] Employee_UserRoleMapEditEmployee_RoleUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_UserRoleMapDelete([FromBody] Employee_UserRoleMapDeleteRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapDetailResponse Employee_UserRoleMapDetail_GetValue([FromBody] Employee_UserRoleMapDetailQueryRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployeeId_DataSource([FromQuery] Employee_UserRoleMapDetailResponse_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapDetail_RefEmployeeId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource([FromQuery] Employee_UserRoleMapDetailResponse_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_UserRoleMapSearchEmployeeResponse> Employee_UserRoleMapSearchEmployee([FromBody] Employee_UserRoleMapSearchEmployeeQueryRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapSearchEmployeeDefaultValueResponse Employee_UserRoleMapSearchEmployee_GetDefaultValue([FromBody] Employee_UserRoleMapSearchEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource([FromQuery] Employee_UserRoleMapSearchEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource([FromQuery] Employee_UserRoleMapSearchEmployeePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource([FromQuery] Employee_UserRoleMapSearchEmployeeQueryRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_UserRoleMapSearchEmployee_RoleResponse> Employee_UserRoleMapSearchEmployee_Role([FromBody] Employee_UserRoleMapSearchEmployee_RoleQueryRequest request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_UserRoleMapSearchEmployee_RoleDefaultValueResponse Employee_UserRoleMapSearchEmployee_Role_GetDefaultValue([FromBody] Employee_UserRoleMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '员工-用户信息-显示名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource([FromQuery] Employee_UserRoleMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource([FromQuery] Employee_UserRoleMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource([FromQuery] Employee_UserRoleMapSearchEmployee_RoleQueryRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Role([FromBody] Employee_RoleAuthMapAddEmployee_RoleRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapAddEmployee_RoleDefaultValueResponse Employee_RoleAuthMapAddEmployee_Role_GetDefaultValue([FromBody] Employee_RoleAuthMapAddEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource([FromQuery] Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource([FromQuery] Employee_RoleAuthMapAddEmployee_RoleRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAuthMapAddEmployee_Auth([FromBody] Employee_RoleAuthMapAddEmployee_AuthRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapAddEmployee_AuthDefaultValueResponse Employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue([FromBody] Employee_RoleAuthMapAddEmployee_AuthPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource([FromQuery] Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource([FromQuery] Employee_RoleAuthMapAddEmployee_AuthRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapEditEmployee_RoleResponse Employee_RoleAuthMapEditEmployee_Role_GetValue([FromBody] Employee_RoleAuthMapEditEmployee_RoleQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Role_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Role([FromBody] Employee_RoleAuthMapEditEmployee_RoleUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource([FromQuery] Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource([FromQuery] Employee_RoleAuthMapEditEmployee_RoleUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapEditEmployee_AuthResponse Employee_RoleAuthMapEditEmployee_Auth_GetValue([FromBody] Employee_RoleAuthMapEditEmployee_AuthQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Auth_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAuthMapEditEmployee_Auth([FromBody] Employee_RoleAuthMapEditEmployee_AuthUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource([FromQuery] Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource([FromQuery] Employee_RoleAuthMapEditEmployee_AuthUpdateRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAuthMapDelete([FromBody] Employee_RoleAuthMapDeleteRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapDetailResponse Employee_RoleAuthMapDetail_GetValue([FromBody] Employee_RoleAuthMapDetailQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource([FromQuery] Employee_RoleAuthMapDetailResponse_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource([FromQuery] Employee_RoleAuthMapDetailResponse_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_RoleResponse> Employee_RoleAuthMapSearchEmployee_Role([FromBody] Employee_RoleAuthMapSearchEmployee_RoleQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapSearchEmployee_RoleDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue([FromBody] Employee_RoleAuthMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_RolePageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_RoleQueryRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_RoleAuthMapSearchEmployee_AuthResponse> Employee_RoleAuthMapSearchEmployee_Auth([FromBody] Employee_RoleAuthMapSearchEmployee_AuthQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAuthMapSearchEmployee_AuthDefaultValueResponse Employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue([FromBody] Employee_RoleAuthMapSearchEmployee_AuthPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_AuthPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_AuthPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource([FromQuery] Employee_RoleAuthMapSearchEmployee_AuthQueryRequest_ValidateNever request)
    {
      return _EmployeeGroupService.Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AuthAdd([FromBody] Employee_AuthAddRequest request)
    {
      return _EmployeeGroupService.Employee_AuthAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AuthAddDefaultValueResponse Employee_AuthAdd_GetDefaultValue([FromBody] Employee_AuthAddPageparameter request)
    {
      return _EmployeeGroupService.Employee_AuthAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AuthEditResponse Employee_AuthEdit_GetValue([FromBody] Employee_AuthEditQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AuthEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AuthEdit([FromBody] Employee_AuthEditUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_AuthEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_AuthDelete([FromBody] Employee_AuthDeleteRequest request)
    {
      return _EmployeeGroupService.Employee_AuthDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AuthDetailResponse Employee_AuthDetail_GetValue([FromBody] Employee_AuthDetailQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AuthDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_AuthSearchResponse> Employee_AuthSearch([FromBody] Employee_AuthSearchQueryRequest request)
    {
      return _EmployeeGroupService.Employee_AuthSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_AuthSearchDefaultValueResponse Employee_AuthSearch_GetDefaultValue([FromBody] Employee_AuthSearchPageparameter request)
    {
      return _EmployeeGroupService.Employee_AuthSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleAdd([FromBody] Employee_RoleAddRequest request)
    {
      return _EmployeeGroupService.Employee_RoleAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleAddDefaultValueResponse Employee_RoleAdd_GetDefaultValue([FromBody] Employee_RoleAddPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleEditResponse Employee_RoleEdit_GetValue([FromBody] Employee_RoleEditQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleEdit([FromBody] Employee_RoleEditUpdateRequest request)
    {
      return _EmployeeGroupService.Employee_RoleEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult Employee_RoleDelete([FromBody] Employee_RoleDeleteRequest request)
    {
      return _EmployeeGroupService.Employee_RoleDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleDetailResponse Employee_RoleDetail_GetValue([FromBody] Employee_RoleDetailQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<Employee_RoleSearchResponse> Employee_RoleSearch([FromBody] Employee_RoleSearchQueryRequest request)
    {
      return _EmployeeGroupService.Employee_RoleSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public Employee_RoleSearchDefaultValueResponse Employee_RoleSearch_GetDefaultValue([FromBody] Employee_RoleSearchPageparameter request)
    {
      return _EmployeeGroupService.Employee_RoleSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult EmployeeAdd([FromBody] EmployeeAddRequest request)
    {
      return _EmployeeGroupService.EmployeeAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public EmployeeAddDefaultValueResponse EmployeeAdd_GetDefaultValue([FromBody] EmployeeAddPageparameter request)
    {
      return _EmployeeGroupService.EmployeeAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public EmployeeEditResponse EmployeeEdit_GetValue([FromBody] EmployeeEditQueryRequest request)
    {
      return _EmployeeGroupService.EmployeeEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult EmployeeEdit([FromBody] EmployeeEditUpdateRequest request)
    {
      return _EmployeeGroupService.EmployeeEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult EmployeeDelete([FromBody] EmployeeDeleteRequest request)
    {
      return _EmployeeGroupService.EmployeeDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public EmployeeDetailResponse EmployeeDetail_GetValue([FromBody] EmployeeDetailQueryRequest request)
    {
      return _EmployeeGroupService.EmployeeDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<EmployeeSearchResponse> EmployeeSearch([FromBody] EmployeeSearchQueryRequest request)
    {
      return _EmployeeGroupService.EmployeeSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/EmployeeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public EmployeeSearchDefaultValueResponse EmployeeSearch_GetDefaultValue([FromBody] EmployeeSearchPageparameter request)
    {
      return _EmployeeGroupService.EmployeeSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
