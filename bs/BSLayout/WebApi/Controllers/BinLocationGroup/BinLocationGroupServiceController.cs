using System;
using WebApi.Code.Interface.BinLocationGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.BinLocationGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WebApi.Controllers.BinLocationGroup
{
  [Route("api/[controller]/[action]")]
  public class BinLocationGroupServiceController : ApiControllerBase
  {
    public readonly IBinLocationGroupInterface _BinLocationGroupService;

    public BinLocationGroupServiceController(IBinLocationGroupInterface _BinLocationGroupService)
    {
      this._BinLocationGroupService = _BinLocationGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult BinLocationAdd([FromBody] BinLocationAddRequest request)
    {
      return _BinLocationGroupService.BinLocationAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public BinLocationAddDefaultValueResponse BinLocationAdd_GetDefaultValue([FromBody] BinLocationAddPageparameter request)
    {
      return _BinLocationGroupService.BinLocationAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public BinLocationEditResponse BinLocationEdit_GetValue([FromBody] BinLocationEditQueryRequest request)
    {
      return _BinLocationGroupService.BinLocationEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult BinLocationEdit([FromBody] BinLocationEditUpdateRequest request)
    {
      return _BinLocationGroupService.BinLocationEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public BinLocationDetailResponse BinLocationDetail_GetValue([FromBody] BinLocationDetailQueryRequest request)
    {
      return _BinLocationGroupService.BinLocationDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult BinLocationDelete([FromBody] BinLocationDeleteRequest request)
    {
      return _BinLocationGroupService.BinLocationDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<BinLocationSearchResponse> BinLocationSearch([FromBody] BinLocationSearchQueryRequest request)
    {
      return _BinLocationGroupService.BinLocationSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/BinLocationGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public BinLocationSearchDefaultValueResponse BinLocationSearch_GetDefaultValue([FromBody] BinLocationSearchPageparameter request)
    {
      return _BinLocationGroupService.BinLocationSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
