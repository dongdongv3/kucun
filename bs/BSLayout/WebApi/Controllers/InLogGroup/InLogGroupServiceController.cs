using System;
using WebApi.Code.Interface.InLogGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.InLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.InLogGroup
{
  [Route("api/[controller]/[action]")]
  public class InLogGroupServiceController : ApiControllerBase
  {
    public readonly IInLogGroupInterface _InLogGroupService;

    public InLogGroupServiceController(IInLogGroupInterface _InLogGroupService)
    {
      this._InLogGroupService = _InLogGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult InLogAdd([FromBody] InLogAddRequest request)
    {
      return _InLogGroupService.InLogAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public InLogAddDefaultValueResponse InLogAdd_GetDefaultValue([FromBody] InLogAddPageparameter request)
    {
      return _InLogGroupService.InLogAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogAdd_goods_DataSource([FromQuery] InLogAddRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogAdd_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogAdd_BinLocation_DataSource([FromQuery] InLogAddRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogAdd_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogAdd_InTypeInfo_DataSource([FromQuery] InLogAddRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogAdd_InTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public InLogEditResponse InLogEdit_GetValue([FromBody] InLogEditQueryRequest request)
    {
      return _InLogGroupService.InLogEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult InLogEdit([FromBody] InLogEditUpdateRequest request)
    {
      return _InLogGroupService.InLogEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogEdit_goods_DataSource([FromQuery] InLogEditUpdateRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogEdit_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogEdit_BinLocation_DataSource([FromQuery] InLogEditUpdateRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogEdit_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogEdit_InTypeInfo_DataSource([FromQuery] InLogEditUpdateRequest_ValidateNever request)
    {
      return _InLogGroupService.InLogEdit_InTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public InLogDetailResponse InLogDetail_GetValue([FromBody] InLogDetailQueryRequest request)
    {
      return _InLogGroupService.InLogDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogDetail_goods_DataSource([FromQuery] InLogDetailResponse_ValidateNever request)
    {
      return _InLogGroupService.InLogDetail_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogDetail_BinLocation_DataSource([FromQuery] InLogDetailResponse_ValidateNever request)
    {
      return _InLogGroupService.InLogDetail_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogDetail_InTypeInfo_DataSource([FromQuery] InLogDetailResponse_ValidateNever request)
    {
      return _InLogGroupService.InLogDetail_InTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult InLogDelete([FromBody] InLogDeleteRequest request)
    {
      return _InLogGroupService.InLogDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<InLogSearchResponse> InLogSearch([FromBody] InLogSearchQueryRequest request)
    {
      return _InLogGroupService.InLogSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public InLogSearchDefaultValueResponse InLogSearch_GetDefaultValue([FromBody] InLogSearchPageparameter request)
    {
      return _InLogGroupService.InLogSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogSearch_goods_View_DataSource([FromQuery] InLogSearchPageparameter request)
    {
      return _InLogGroupService.InLogSearch_goods_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogSearch_BinLocation_View_DataSource([FromQuery] InLogSearchPageparameter request)
    {
      return _InLogGroupService.InLogSearch_BinLocation_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> InLogSearch_InTypeInfo_View_DataSource([FromQuery] InLogSearchPageparameter request)
    {
      return _InLogGroupService.InLogSearch_InTypeInfo_View_DataSource(request, HttpContext.UserInfo());
    }

  }
}
