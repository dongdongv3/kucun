using System;
using WebApi.Code.Interface.OutLogGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.OutLogGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.OutLogGroup
{
  [Route("api/[controller]/[action]")]
  public class OutLogGroupServiceController : ApiControllerBase
  {
    public readonly IOutLogGroupInterface _OutLogGroupService;

    public OutLogGroupServiceController(IOutLogGroupInterface _OutLogGroupService)
    {
      this._OutLogGroupService = _OutLogGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult OutLogAdd([FromBody] OutLogAddRequest request)
    {
      return _OutLogGroupService.OutLogAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OutLogAddDefaultValueResponse OutLogAdd_GetDefaultValue([FromBody] OutLogAddPageparameter request)
    {
      return _OutLogGroupService.OutLogAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogAdd_goods_DataSource([FromQuery] OutLogAddRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogAdd_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogAdd_OutTypeInfo_DataSource([FromQuery] OutLogAddRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogAdd_OutTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogAdd_BinLocation_DataSource([FromQuery] OutLogAddRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogAdd_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OutLogEditResponse OutLogEdit_GetValue([FromBody] OutLogEditQueryRequest request)
    {
      return _OutLogGroupService.OutLogEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult OutLogEdit([FromBody] OutLogEditUpdateRequest request)
    {
      return _OutLogGroupService.OutLogEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogEdit_goods_DataSource([FromQuery] OutLogEditUpdateRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogEdit_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogEdit_OutTypeInfo_DataSource([FromQuery] OutLogEditUpdateRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogEdit_OutTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogEdit_BinLocation_DataSource([FromQuery] OutLogEditUpdateRequest_ValidateNever request)
    {
      return _OutLogGroupService.OutLogEdit_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public OutLogDetailResponse OutLogDetail_GetValue([FromBody] OutLogDetailQueryRequest request)
    {
      return _OutLogGroupService.OutLogDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogDetail_goods_DataSource([FromQuery] OutLogDetailResponse_ValidateNever request)
    {
      return _OutLogGroupService.OutLogDetail_goods_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '出库类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogDetail_OutTypeInfo_DataSource([FromQuery] OutLogDetailResponse_ValidateNever request)
    {
      return _OutLogGroupService.OutLogDetail_OutTypeInfo_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogDetail_BinLocation_DataSource([FromQuery] OutLogDetailResponse_ValidateNever request)
    {
      return _OutLogGroupService.OutLogDetail_BinLocation_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult OutLogDelete([FromBody] OutLogDeleteRequest request)
    {
      return _OutLogGroupService.OutLogDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<OutLogSearchResponse> OutLogSearch([FromBody] OutLogSearchQueryRequest request)
    {
      return _OutLogGroupService.OutLogSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public OutLogSearchDefaultValueResponse OutLogSearch_GetDefaultValue([FromBody] OutLogSearchPageparameter request)
    {
      return _OutLogGroupService.OutLogSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '商品' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogSearch_goods_View_DataSource([FromQuery] OutLogSearchPageparameter request)
    {
      return _OutLogGroupService.OutLogSearch_goods_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '出库类型' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogSearch_OutTypeInfo_View_DataSource([FromQuery] OutLogSearchPageparameter request)
    {
      return _OutLogGroupService.OutLogSearch_OutTypeInfo_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '库位' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> OutLogSearch_BinLocation_View_DataSource([FromQuery] OutLogSearchPageparameter request)
    {
      return _OutLogGroupService.OutLogSearch_BinLocation_View_DataSource(request, HttpContext.UserInfo());
    }

  }
}
