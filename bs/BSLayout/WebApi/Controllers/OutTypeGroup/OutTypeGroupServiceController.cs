using System;
using WebApi.Code.Interface.OutTypeGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.OutTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Controllers.OutTypeGroup
{
  [Route("api/[controller]/[action]")]
  public class OutTypeGroupServiceController : ApiControllerBase
  {
    public readonly IOutTypeGroupInterface _OutTypeGroupService;

    public OutTypeGroupServiceController(IOutTypeGroupInterface _OutTypeGroupService)
    {
      this._OutTypeGroupService = _OutTypeGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult OutTypeAdd([FromBody] OutTypeAddRequest request)
    {
      return _OutTypeGroupService.OutTypeAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OutTypeAddDefaultValueResponse OutTypeAdd_GetDefaultValue([FromBody] OutTypeAddPageparameter request)
    {
      return _OutTypeGroupService.OutTypeAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutTypeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult OutTypeDelete([FromBody] OutTypeDeleteRequest request)
    {
      return _OutTypeGroupService.OutTypeDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<OutTypeSearchResponse> OutTypeSearch([FromBody] OutTypeSearchQueryRequest request)
    {
      return _OutTypeGroupService.OutTypeSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/OutTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public OutTypeSearchDefaultValueResponse OutTypeSearch_GetDefaultValue([FromBody] OutTypeSearchPageparameter request)
    {
      return _OutTypeGroupService.OutTypeSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
