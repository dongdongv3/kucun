using Microsoft.AspNetCore.Mvc;
using WebApi.Code;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;

namespace WebApi.Controllers
{
  /// <summary>
  /// 应用程序基础数据服务
  /// </summary>
  public class AppDataController : ApiControllerBase
  {
    /// <summary>
    /// 获取当前登录用户的信息
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [AuthenticationFilter]
    public UserInfo GetUser()
    {
      return HttpContext.UserInfo();
    }
  }
}
