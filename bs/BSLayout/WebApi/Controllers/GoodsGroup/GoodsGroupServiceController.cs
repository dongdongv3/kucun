using System;
using WebApi.Code.Interface.GoodsGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.GoodsGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WebApi.Controllers.GoodsGroup
{
  [Route("api/[controller]/[action]")]
  public class GoodsGroupServiceController : ApiControllerBase
  {
    public readonly IGoodsGroupInterface _GoodsGroupService;

    public GoodsGroupServiceController(IGoodsGroupInterface _GoodsGroupService)
    {
      this._GoodsGroupService = _GoodsGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult GoodsAdd([FromBody] GoodsAddRequest request)
    {
      return _GoodsGroupService.GoodsAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public GoodsAddDefaultValueResponse GoodsAdd_GetDefaultValue([FromBody] GoodsAddPageparameter request)
    {
      return _GoodsGroupService.GoodsAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public GoodsEditResponse GoodsEdit_GetValue([FromBody] GoodsEditQueryRequest request)
    {
      return _GoodsGroupService.GoodsEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult GoodsEdit([FromBody] GoodsEditUpdateRequest request)
    {
      return _GoodsGroupService.GoodsEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public GoodsDetailResponse GoodsDetail_GetValue([FromBody] GoodsDetailQueryRequest request)
    {
      return _GoodsGroupService.GoodsDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult GoodsDelete([FromBody] GoodsDeleteRequest request)
    {
      return _GoodsGroupService.GoodsDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<GoodsSearchResponse> GoodsSearch([FromBody] GoodsSearchQueryRequest request)
    {
      return _GoodsGroupService.GoodsSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GoodsGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public GoodsSearchDefaultValueResponse GoodsSearch_GetDefaultValue([FromBody] GoodsSearchPageparameter request)
    {
      return _GoodsGroupService.GoodsSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
