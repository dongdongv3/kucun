using System;
using WebApi.Code.Interface.InTypeGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.InTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Controllers.InTypeGroup
{
  [Route("api/[controller]/[action]")]
  public class InTypeGroupServiceController : ApiControllerBase
  {
    public readonly IInTypeGroupInterface _InTypeGroupService;

    public InTypeGroupServiceController(IInTypeGroupInterface _InTypeGroupService)
    {
      this._InTypeGroupService = _InTypeGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult InTypeAdd([FromBody] InTypeAddRequest request)
    {
      return _InTypeGroupService.InTypeAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public InTypeAddDefaultValueResponse InTypeAdd_GetDefaultValue([FromBody] InTypeAddPageparameter request)
    {
      return _InTypeGroupService.InTypeAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InTypeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult InTypeDelete([FromBody] InTypeDeleteRequest request)
    {
      return _InTypeGroupService.InTypeDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<InTypeSearchResponse> InTypeSearch([FromBody] InTypeSearchQueryRequest request)
    {
      return _InTypeGroupService.InTypeSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/InTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public InTypeSearchDefaultValueResponse InTypeSearch_GetDefaultValue([FromBody] InTypeSearchPageparameter request)
    {
      return _InTypeGroupService.InTypeSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
