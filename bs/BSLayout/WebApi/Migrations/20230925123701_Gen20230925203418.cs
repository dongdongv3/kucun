﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class Gen20230925203418 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BinLocation",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinLocation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    DisplayName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee_Auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    AuthName = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_Auth", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee_Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    RoleName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileUpLoad",
                columns: table => new
                {
                    ID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    FileName = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
                    Ext = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    UserID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    CreateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    IsUse = table.Column<bool>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileUpLoad", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Goods",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Count = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Goods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    CreateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    Goods = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    BinLocation = table.Column<string>(type: "TEXT", maxLength: 500, nullable: true),
                    Count = table.Column<int>(type: "INTEGER", nullable: true),
                    InTypeInfo = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Createtime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    Goods = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Count = table.Column<int>(type: "INTEGER", nullable: true),
                    OutTypeInfo = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    BinLocation = table.Column<string>(type: "TEXT", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee_AccountAndPasswordAuth1",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    LoginID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
                    Pwd = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    RefEmployeeId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_AccountAndPasswordAuth1", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee_AccountAndPasswordAuth1_Employee_RefEmployeeId",
                        column: x => x.RefEmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee_RoleAuthMap",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    RefEmployee_RoleId = table.Column<Guid>(type: "TEXT", nullable: false),
                    RefEmployee_AuthId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_RoleAuthMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee_RoleAuthMap_Employee_Auth_RefEmployee_AuthId",
                        column: x => x.RefEmployee_AuthId,
                        principalTable: "Employee_Auth",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_RoleAuthMap_Employee_Role_RefEmployee_RoleId",
                        column: x => x.RefEmployee_RoleId,
                        principalTable: "Employee_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employee_UserRoleMap",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    RefEmployeeId = table.Column<string>(type: "TEXT", nullable: true),
                    RefEmployee_RoleId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee_UserRoleMap", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employee_UserRoleMap_Employee_RefEmployeeId",
                        column: x => x.RefEmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee_UserRoleMap_Employee_Role_RefEmployee_RoleId",
                        column: x => x.RefEmployee_RoleId,
                        principalTable: "Employee_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employee_AccountAndPasswordAuth1_RefEmployeeId",
                table: "Employee_AccountAndPasswordAuth1",
                column: "RefEmployeeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_RoleAuthMap_RefEmployee_AuthId",
                table: "Employee_RoleAuthMap",
                column: "RefEmployee_AuthId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_RoleAuthMap_RefEmployee_RoleId",
                table: "Employee_RoleAuthMap",
                column: "RefEmployee_RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_UserRoleMap_RefEmployee_RoleId",
                table: "Employee_UserRoleMap",
                column: "RefEmployee_RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_UserRoleMap_RefEmployeeId",
                table: "Employee_UserRoleMap",
                column: "RefEmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BinLocation");

            migrationBuilder.DropTable(
                name: "Employee_AccountAndPasswordAuth1");

            migrationBuilder.DropTable(
                name: "Employee_RoleAuthMap");

            migrationBuilder.DropTable(
                name: "Employee_UserRoleMap");

            migrationBuilder.DropTable(
                name: "FileUpLoad");

            migrationBuilder.DropTable(
                name: "Goods");

            migrationBuilder.DropTable(
                name: "InLog");

            migrationBuilder.DropTable(
                name: "InType");

            migrationBuilder.DropTable(
                name: "OutLog");

            migrationBuilder.DropTable(
                name: "OutType");

            migrationBuilder.DropTable(
                name: "Employee_Auth");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Employee_Role");
        }
    }
}
