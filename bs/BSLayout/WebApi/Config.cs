// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace WebApi
{
  public static class Config
  {
    public static IEnumerable<IdentityResource> IdentityResources =>
               new IdentityResource[]
               {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
               };

    public static IEnumerable<ApiScope> ApiScopes =>
        new ApiScope[]
        {
                new ApiScope("api1"),
                new ApiScope(IdentityServerConstants.LocalApi.ScopeName)
        };

    public static string[] GetUrls(IConfiguration configuration)
    {
      string[] output;
      var urlstr = "";
      if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ASPNETCORE_AuthEndpoint")))
      {
        urlstr = Environment.GetEnvironmentVariable("ASPNETCORE_AuthEndpoint");
      }
      else if (!string.IsNullOrEmpty(configuration["urls"]))
      {
        urlstr = configuration["urls"];
      }
      else if (Program.Args.Any(x => x == "--urls"))
      {
        int index = 0;
        for (int i = 0; i < Program.Args.Length; i++)
        {
          if (Program.Args[i] == "--urls")
          {
            index = i;
            break;
          }
        }
        urlstr = Program.Args[index + 1];
      }
      else if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ASPNETCORE_URLS")))
      {
        urlstr = Environment.GetEnvironmentVariable("ASPNETCORE_URLS");
      }
      else
      {
        urlstr = "http://*:5000;https://*:5001";
      }
      return urlstr.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
    }

    public static IEnumerable<Client> Clients(IConfiguration configuration)
    {
      return new Client[]
      {
                new Client
                {
                    ClientId = "js",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,

#if(DEBUG)
                    
                    RedirectUris =           { $"https://localhost:5011/callback.html" },
                    PostLogoutRedirectUris = { $"https://localhost:5011/index.html" },
                    AllowedCorsOrigins =     { $"https://localhost:5011" },
#else

                    RedirectUris =  GetUrls(configuration).Select(x=> $"{x}/callback.html").ToList(),
                    PostLogoutRedirectUris =  GetUrls(configuration).Select(x=> $"{x}/index.html").ToList(),
                    AllowedCorsOrigins = GetUrls(configuration).Select(x=> $"{x}").ToList(),
#endif

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1",
                        IdentityServerConstants.LocalApi.ScopeName
                    }
                },
                
#if (DEBUG)
                new Client
                {
                    ClientId = "js2",
                    ClientName = "JavaScript Client2",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,

                    RedirectUris =           { "http://localhost:8001/callback.html" },
                    PostLogoutRedirectUris = { "http://localhost:8001/index.html" },
                    AllowedCorsOrigins =     { "http://localhost:8001", "https://localhost:5001", },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1",
                        IdentityServerConstants.LocalApi.ScopeName
                    }
                },
                 new Client
                {
                    ClientId = "js3",
                    ClientName = "JavaScript Client3",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    ClientSecrets= { new Secret("abcd".Sha256()) },

                    RedirectUris =           { "https://localhost:5011/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { "https://localhost:5011" },
                    AllowedCorsOrigins =     { "https://localhost:5011" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1",
                        IdentityServerConstants.LocalApi.ScopeName
                    }
                },
#endif
      };
    }
  }
}
