using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;
using WebApi.Code.Common;

namespace WebApi
{
  public class InitData
  {
    public static void DatInit(IServiceScope serviceScope)
    {
      var _inventoryManagementSystemDB = serviceScope.ServiceProvider.GetRequiredService<InventoryManagementSystemDB>();
      Console.WriteLine("初始化单元：权限或角色列表初始化");
      {
        var tmp = CommonDataSource.AuthList_Auto_Employee();
        tmp.ForEach(one =>
        {

          var auth = _inventoryManagementSystemDB.Employee_Auth.FirstOrDefault(x => x.AuthName == one.Key);
          if (auth == null)
          {
            auth = new Employee_Auth
            {
              Id = Guid.NewGuid(),
              AuthName = one.Key,
            };
            _inventoryManagementSystemDB.Add(auth);
          }
        });

        _inventoryManagementSystemDB.SaveChanges();
        //初始化超管角色
        var authList = _inventoryManagementSystemDB.Employee_Auth.ToList();
        var autoCreateAdminRole = ReadSetting.GetValue<string>("角色初始化", "是否需要初始化超级管理员角色?", new string[] { "是", "否" });
        if (autoCreateAdminRole == "是")
        {
          var adminName = ReadSetting.GetValue<string>("角色初始化", "请输入超级管理员角色名称");
          var adminrole = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.RoleName == adminName);
          if (adminrole == null)
          {
            adminrole = new Employee_Role
            {
              Id = Guid.NewGuid(),
              RoleName = adminName,
              SubEmployee_RoleAuthMap = authList.Select(x => new Employee_RoleAuthMap { Id = Guid.NewGuid(), RefEmployee_Auth = x }).ToList()
            };
            _inventoryManagementSystemDB.Add(adminrole);
          }
        }
        _inventoryManagementSystemDB.SaveChanges();
      }
      Console.WriteLine("初始化单元：初始化用户");

      //初始化用户
      var autoCreateUser = ReadSetting.GetValue<string>("用户初始化", "是否需要初始化超级管理员?.", new string[] { "是", "否" });
      if (autoCreateUser == "是")
      {
        var adminDisplayName = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员显示名");

        var adminRoleName = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员拥有的角色名称", _inventoryManagementSystemDB.Employee_Role.Select(x => x.RoleName).ToList().ToArray());
        var adminRole = _inventoryManagementSystemDB.Employee_Role.FirstOrDefault(x => x.RoleName == adminRoleName);
        if (adminRole == null)
        {
          throw new Exception($"无法找到角色‘{adminRoleName}’");
        }
        var adminUser = _inventoryManagementSystemDB.Employee.Include(e => e.SubEmployee_AccountAndPasswordAuth1).FirstOrDefault(x => x.DisplayName == adminDisplayName);
        if (adminUser == null)
        {
          adminUser = new Employee
          {
            Id = Guid.NewGuid().ToString(),
            DisplayName = adminDisplayName,

            SubEmployee_AccountAndPasswordAuth1 = new Employee_AccountAndPasswordAuth1
            {
              Id = Guid.NewGuid(),
              LoginID = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员登录名"),
              Pwd = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员登录密码")
            },
            SubEmployee_UserRoleMap = new List<Employee_UserRoleMap>{
              new Employee_UserRoleMap{
                Id=Guid.NewGuid(),
                RefEmployee_Role=adminRole
              }
            }
          };
          _inventoryManagementSystemDB.Add(adminUser);
        }
      }
      _inventoryManagementSystemDB.SaveChanges();

    }
  }
}
