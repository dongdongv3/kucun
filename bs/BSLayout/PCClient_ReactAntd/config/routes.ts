
export default 
[
{
name:"查询权限",
icon:"AppstoreOutlined",
path:"/EmployeeGroup/Employee_AuthSearchPage",
component:"./EmployeeGroup/Employee_AuthSearchPage",
access:"_EmployeeGroup_Employee_AuthSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/EmployeeGroup/查询"],
roles:[],
},
{
name:"查询角色",
icon:"AppstoreOutlined",
path:"/EmployeeGroup/Employee_RoleSearchPage",
component:"./EmployeeGroup/Employee_RoleSearchPage",
access:"_EmployeeGroup_Employee_RoleSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/EmployeeGroup/查询"],
roles:[],
},
{
name:"查询员工-用户信息",
icon:"AppstoreOutlined",
path:"/EmployeeGroup/EmployeeSearchPage",
component:"./EmployeeGroup/EmployeeSearchPage",
access:"_EmployeeGroup_EmployeeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/EmployeeGroup/查询"],
roles:[],
},
{
name:"查询入库类型",
icon:"AppstoreOutlined",
path:"/InTypeGroup/InTypeSearchPage",
component:"./InTypeGroup/InTypeSearchPage",
access:"_InTypeGroup_InTypeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/InTypeGroup/查询"],
roles:[],
},
{
name:"查询出库类型",
icon:"AppstoreOutlined",
path:"/OutTypeGroup/OutTypeSearchPage",
component:"./OutTypeGroup/OutTypeSearchPage",
access:"_OutTypeGroup_OutTypeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/OutTypeGroup/查询"],
roles:[],
},
{
name:"查询商品",
icon:"AppstoreOutlined",
path:"/GoodsGroup/GoodsSearchPage",
component:"./GoodsGroup/GoodsSearchPage",
access:"_GoodsGroup_GoodsSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/GoodsGroup/查询"],
roles:[],
},
{
name:"查询库位",
icon:"AppstoreOutlined",
path:"/BinLocationGroup/BinLocationSearchPage",
component:"./BinLocationGroup/BinLocationSearchPage",
access:"_BinLocationGroup_BinLocationSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/BinLocationGroup/查询"],
roles:[],
},
{
name:"查询出库记录",
icon:"AppstoreOutlined",
path:"/OutLogGroup/OutLogSearchPage",
component:"./OutLogGroup/OutLogSearchPage",
access:"_OutLogGroup_OutLogSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/OutLogGroup/查询"],
roles:[],
},
{
name:"查询入库记录",
icon:"AppstoreOutlined",
path:"/InLogGroup/InLogSearchPage",
component:"./InLogGroup/InLogSearchPage",
access:"_InLogGroup_InLogSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
auths:["/InLogGroup/查询"],
roles:[],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AccountAndPasswordAuth1AddEmployeePage",
component:"./EmployeeGroup/Employee_AccountAndPasswordAuth1AddEmployeePage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_AccountAndPasswordAuth1AddEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AccountAndPasswordAuth1EditEmployeePage",
component:"./EmployeeGroup/Employee_AccountAndPasswordAuth1EditEmployeePage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_AccountAndPasswordAuth1EditEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AccountAndPasswordAuth1DeletePage",
component:"./EmployeeGroup/Employee_AccountAndPasswordAuth1DeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_Employee_AccountAndPasswordAuth1DeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AccountAndPasswordAuth1DetailPage",
component:"./EmployeeGroup/Employee_AccountAndPasswordAuth1DetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_Employee_AccountAndPasswordAuth1DetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AccountAndPasswordAuth1SearchEmployeePage",
component:"./EmployeeGroup/Employee_AccountAndPasswordAuth1SearchEmployeePage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_AccountAndPasswordAuth1SearchEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapAddEmployeePage",
component:"./EmployeeGroup/Employee_UserRoleMapAddEmployeePage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapAddEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapAddEmployee_RolePage",
component:"./EmployeeGroup/Employee_UserRoleMapAddEmployee_RolePage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapAddEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapEditEmployeePage",
component:"./EmployeeGroup/Employee_UserRoleMapEditEmployeePage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapEditEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapEditEmployee_RolePage",
component:"./EmployeeGroup/Employee_UserRoleMapEditEmployee_RolePage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapEditEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapDeletePage",
component:"./EmployeeGroup/Employee_UserRoleMapDeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapDetailPage",
component:"./EmployeeGroup/Employee_UserRoleMapDetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapSearchEmployeePage",
component:"./EmployeeGroup/Employee_UserRoleMapSearchEmployeePage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapSearchEmployeePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_UserRoleMapSearchEmployee_RolePage",
component:"./EmployeeGroup/Employee_UserRoleMapSearchEmployee_RolePage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_UserRoleMapSearchEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapAddEmployee_RolePage",
component:"./EmployeeGroup/Employee_RoleAuthMapAddEmployee_RolePage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapAddEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapAddEmployee_AuthPage",
component:"./EmployeeGroup/Employee_RoleAuthMapAddEmployee_AuthPage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapAddEmployee_AuthPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapEditEmployee_RolePage",
component:"./EmployeeGroup/Employee_RoleAuthMapEditEmployee_RolePage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapEditEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapEditEmployee_AuthPage",
component:"./EmployeeGroup/Employee_RoleAuthMapEditEmployee_AuthPage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapEditEmployee_AuthPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapDeletePage",
component:"./EmployeeGroup/Employee_RoleAuthMapDeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapDetailPage",
component:"./EmployeeGroup/Employee_RoleAuthMapDetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapSearchEmployee_RolePage",
component:"./EmployeeGroup/Employee_RoleAuthMapSearchEmployee_RolePage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapSearchEmployee_RolePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAuthMapSearchEmployee_AuthPage",
component:"./EmployeeGroup/Employee_RoleAuthMapSearchEmployee_AuthPage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAuthMapSearchEmployee_AuthPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AuthAddPage",
component:"./EmployeeGroup/Employee_AuthAddPage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_AuthAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AuthEditPage",
component:"./EmployeeGroup/Employee_AuthEditPage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_AuthEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AuthDeletePage",
component:"./EmployeeGroup/Employee_AuthDeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_Employee_AuthDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AuthDetailPage",
component:"./EmployeeGroup/Employee_AuthDetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_Employee_AuthDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_AuthSearchPage",
component:"./EmployeeGroup/Employee_AuthSearchPage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_AuthSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleAddPage",
component:"./EmployeeGroup/Employee_RoleAddPage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_Employee_RoleAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleEditPage",
component:"./EmployeeGroup/Employee_RoleEditPage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_Employee_RoleEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleDeletePage",
component:"./EmployeeGroup/Employee_RoleDeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_Employee_RoleDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleDetailPage",
component:"./EmployeeGroup/Employee_RoleDetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_Employee_RoleDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/Employee_RoleSearchPage",
component:"./EmployeeGroup/Employee_RoleSearchPage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_Employee_RoleSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/EmployeeAddPage",
component:"./EmployeeGroup/EmployeeAddPage",
auths:["/EmployeeGroup/添加"],
roles:[],
access:"_EmployeeGroup_EmployeeAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/EmployeeEditPage",
component:"./EmployeeGroup/EmployeeEditPage",
auths:["/EmployeeGroup/编辑"],
roles:[],
access:"_EmployeeGroup_EmployeeEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/EmployeeDeletePage",
component:"./EmployeeGroup/EmployeeDeletePage",
auths:["/EmployeeGroup/删除"],
roles:[],
access:"_EmployeeGroup_EmployeeDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/EmployeeDetailPage",
component:"./EmployeeGroup/EmployeeDetailPage",
auths:["/EmployeeGroup/详情"],
roles:[],
access:"_EmployeeGroup_EmployeeDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/EmployeeGroup/EmployeeSearchPage",
component:"./EmployeeGroup/EmployeeSearchPage",
auths:["/EmployeeGroup/查询"],
roles:[],
access:"_EmployeeGroup_EmployeeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InTypeGroup/InTypeAddPage",
component:"./InTypeGroup/InTypeAddPage",
auths:["/InTypeGroup/添加"],
roles:[],
access:"_InTypeGroup_InTypeAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InTypeGroup/InTypeDeletePage",
component:"./InTypeGroup/InTypeDeletePage",
auths:["/InTypeGroup/删除"],
roles:[],
access:"_InTypeGroup_InTypeDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InTypeGroup/InTypeSearchPage",
component:"./InTypeGroup/InTypeSearchPage",
auths:["/InTypeGroup/查询"],
roles:[],
access:"_InTypeGroup_InTypeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutTypeGroup/OutTypeAddPage",
component:"./OutTypeGroup/OutTypeAddPage",
auths:["/OutTypeGroup/添加"],
roles:[],
access:"_OutTypeGroup_OutTypeAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutTypeGroup/OutTypeDeletePage",
component:"./OutTypeGroup/OutTypeDeletePage",
auths:["/OutTypeGroup/删除"],
roles:[],
access:"_OutTypeGroup_OutTypeDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutTypeGroup/OutTypeSearchPage",
component:"./OutTypeGroup/OutTypeSearchPage",
auths:["/OutTypeGroup/查询"],
roles:[],
access:"_OutTypeGroup_OutTypeSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/GoodsGroup/GoodsAddPage",
component:"./GoodsGroup/GoodsAddPage",
auths:["/GoodsGroup/添加"],
roles:[],
access:"_GoodsGroup_GoodsAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/GoodsGroup/GoodsEditPage",
component:"./GoodsGroup/GoodsEditPage",
auths:["/GoodsGroup/编辑"],
roles:[],
access:"_GoodsGroup_GoodsEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/GoodsGroup/GoodsDetailPage",
component:"./GoodsGroup/GoodsDetailPage",
auths:["/GoodsGroup/详情"],
roles:[],
access:"_GoodsGroup_GoodsDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/GoodsGroup/GoodsDeletePage",
component:"./GoodsGroup/GoodsDeletePage",
auths:["/GoodsGroup/删除"],
roles:[],
access:"_GoodsGroup_GoodsDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/GoodsGroup/GoodsSearchPage",
component:"./GoodsGroup/GoodsSearchPage",
auths:["/GoodsGroup/查询"],
roles:[],
access:"_GoodsGroup_GoodsSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/BinLocationGroup/BinLocationAddPage",
component:"./BinLocationGroup/BinLocationAddPage",
auths:["/BinLocationGroup/添加"],
roles:[],
access:"_BinLocationGroup_BinLocationAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/BinLocationGroup/BinLocationEditPage",
component:"./BinLocationGroup/BinLocationEditPage",
auths:["/BinLocationGroup/编辑"],
roles:[],
access:"_BinLocationGroup_BinLocationEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/BinLocationGroup/BinLocationDetailPage",
component:"./BinLocationGroup/BinLocationDetailPage",
auths:["/BinLocationGroup/详情"],
roles:[],
access:"_BinLocationGroup_BinLocationDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/BinLocationGroup/BinLocationDeletePage",
component:"./BinLocationGroup/BinLocationDeletePage",
auths:["/BinLocationGroup/删除"],
roles:[],
access:"_BinLocationGroup_BinLocationDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/BinLocationGroup/BinLocationSearchPage",
component:"./BinLocationGroup/BinLocationSearchPage",
auths:["/BinLocationGroup/查询"],
roles:[],
access:"_BinLocationGroup_BinLocationSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutLogGroup/OutLogAddPage",
component:"./OutLogGroup/OutLogAddPage",
auths:["/OutLogGroup/添加"],
roles:[],
access:"_OutLogGroup_OutLogAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutLogGroup/OutLogEditPage",
component:"./OutLogGroup/OutLogEditPage",
auths:["/OutLogGroup/编辑"],
roles:[],
access:"_OutLogGroup_OutLogEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutLogGroup/OutLogDetailPage",
component:"./OutLogGroup/OutLogDetailPage",
auths:["/OutLogGroup/详情"],
roles:[],
access:"_OutLogGroup_OutLogDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutLogGroup/OutLogDeletePage",
component:"./OutLogGroup/OutLogDeletePage",
auths:["/OutLogGroup/删除"],
roles:[],
access:"_OutLogGroup_OutLogDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/OutLogGroup/OutLogSearchPage",
component:"./OutLogGroup/OutLogSearchPage",
auths:["/OutLogGroup/查询"],
roles:[],
access:"_OutLogGroup_OutLogSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InLogGroup/InLogAddPage",
component:"./InLogGroup/InLogAddPage",
auths:["/InLogGroup/添加"],
roles:[],
access:"_InLogGroup_InLogAddPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InLogGroup/InLogEditPage",
component:"./InLogGroup/InLogEditPage",
auths:["/InLogGroup/编辑"],
roles:[],
access:"_InLogGroup_InLogEditPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InLogGroup/InLogDetailPage",
component:"./InLogGroup/InLogDetailPage",
auths:["/InLogGroup/详情"],
roles:[],
access:"_InLogGroup_InLogDetailPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InLogGroup/InLogDeletePage",
component:"./InLogGroup/InLogDeletePage",
auths:["/InLogGroup/删除"],
roles:[],
access:"_InLogGroup_InLogDeletePage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
name:"test",
hideInMenu:true,
path:"/InLogGroup/InLogSearchPage",
component:"./InLogGroup/InLogSearchPage",
auths:["/InLogGroup/查询"],
roles:[],
access:"_InLogGroup_InLogSearchPage",
haveAuth: true,
wrappers: ['@/wrappers/auth'],
},
{
path:'/',
redirect:"./GoodsGroup/GoodsSearchPage",
},

    {
      name: "Init",
      hideInMenu: true,
      menuHeaderRender: false,
      menuRender: false,
      headerRender: false,
      path: "/Init/Index",
      component: "./Init/Index",
    },
  {
    component: './404',
  },
]