import Oidc from 'oidc-client'


const isDev = process.env.NODE_ENV === 'development';

let oidcConfig = {
    authority: window.location.origin,
    client_id: "js",
    redirect_uri: window.location.origin+"/callback.html",
    response_type: "code",
    scope: "openid profile api1 IdentityServerApi",
    post_logout_redirect_uri: window.location.origin+"/index.html",
}

if (isDev) {
    oidcConfig = {
        authority: "https://localhost:5011",
        client_id: "js2",
        redirect_uri: "http://localhost:8001/callback.html",
        response_type: "code",
        scope: "openid profile api1 IdentityServerApi",
        post_logout_redirect_uri: "http://localhost:8001/index.html",
    };
}

const mgr = new Oidc.UserManager(oidcConfig);
let access_token = "";
mgr.getUser().then(function (user) {
    if (user) {
        access_token = user.access_token;
    }

});

export { mgr, access_token };
