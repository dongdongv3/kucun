import { Button, Modal } from 'antd';
import { useState } from 'react';

/**
 * 一个具有弹层和按钮的组合，点击按钮自动触发弹层，且可通过设置BodyContent类型信息来控制和弹层内部的ui
 */
interface modelLinkProps {
  modelTitle: string; //面板标题
  buttonName: string; //按钮名称
  BodyContent: any; //模态框中的内容
  [propName: string]: any;
}

const ModelLink: React.FC<modelLinkProps> = ({ buttonName, modelTitle, BodyContent, ...other }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    let tmpvalue = undefined;
    if (other.value) {
      tmpvalue = other.value;
    }
    setIsModalVisible(true);
  };
  const hideModal = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showModal} {...other}>
        {buttonName}
      </Button>
      <Modal
        visible={isModalVisible}
        title={modelTitle}
        footer={null}
        onCancel={hideModal}
        destroyOnClose={true}
        width={1100}
        maskClosable={false}
      >
        <BodyContent onHideModal={hideModal} {...other} />
      </Modal>
    </>
  );
};

export default ModelLink;
