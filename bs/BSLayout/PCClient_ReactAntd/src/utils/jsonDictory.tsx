import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import ModelLink from './ModelLink';
import ProForm, { ProFormInstance } from '@ant-design/pro-form';
import { Button, Space } from 'antd';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import ReactJson from 'react-json-view';
import { ProFormText } from '@ant-design/pro-components';

const readObjToKv = (obj: any) => {
  const kvoutput: { key: string; value: any }[] = [];
  for (let p in obj) {
    kvoutput.push({ key: p, value: obj[p] });
  }
  return kvoutput;
};

const readKvToObj = (kv: { key: string; value: any }[]) => {
  const objoutput = {};
  kv.forEach((x) => {
    objoutput[x.key] = x.value;
  });
  console.log(objoutput);
  return objoutput;
};

const ProFormJsonDictoryView: React.FC<{}> = ({ value = {}, onChange, children, ...other }) => {
  const [thisvalue, setthisvalue] = useState(readObjToKv(value || {}));
  const ref = useRef<ProFormInstance>();
  const deleteItem = async (item) => {
    const newvalue = thisvalue.filter((x) => x.key != item.key);
    setthisvalue(newvalue);
    if (onChange) {
      onChange(readKvToObj(newvalue));
    }
  };

  const addItem = async (itemvalue) => {
    const tmp = [...thisvalue, itemvalue];
    setthisvalue(tmp);
    if (onChange) {
      onChange(readKvToObj(tmp));
    }
  };

  const tryMove = (rec: any, isUp: boolean) => {
    const tmpList = [...thisvalue];
    for (let i = 0; i < tmpList.length; i++) {
      const item = tmpList[i];
      if (item == rec) {
        if (isUp && i != 0) {
          tmpList[i] = tmpList[i - 1];
          tmpList[i - 1] = item;
          break;
        }
        if (!isUp && i != tmpList.length - 1) {
          tmpList[i] = tmpList[i + 1];
          tmpList[i + 1] = item;
          break;
        }
      }
    }
    setthisvalue(tmpList);
    if (onChange) {
      onChange(tmpList.map((x) => x.value));
    }
  };

  const updateItem = async (oldvalue, itemvalue) => {
    const newvalue = [...thisvalue];
    newvalue.forEach((v, index) => {
      if (v == oldvalue) {
        newvalue[index] = itemvalue;
      }
    });
    setthisvalue(newvalue);
    if (onChange) {
      onChange(readKvToObj(newvalue));
    }
  };

  //生成列
  const columns: any[] = [];
  columns.push({
    title: '序号',
    dataIndex: 'index',
    valueType: 'indexBorder',
    width: 48,
  });
  columns.push({
    title: '属性名',
    dataIndex: 'key',
  });
  columns.push({
    title: '属性值',
    dataIndex: 'value',
    render: (text: any, record: any, index: number) => {
      if (record?.value)
        if (typeof record.value == typeof {})
          return (
            <ReactJson
              src={record.value}
              name={null}
              displayDataTypes={false}
              collapsed={0}
              enableClipboard={false}
              iconStyle="square"
            />
          );
        else return record.value;
      else return '';
    },
  });

  columns.push({
    title: '操作',
    valueType: 'option',
    render: (text, record, _, action) => [
      <ModelLink
        type="primary"
        buttonName="编辑"
        modelTitle={`编辑`}
        BodyContent={({ onChange, onHideModal, value }) => {
          return (
            <ProForm
              onFinish={(v) => {
                onChange(v);
                onHideModal();
              }}
              initialValues={value}
            >
              <ProFormText name="key" label="属性名" rules={[{ required: true }]}></ProFormText>
              {children}
            </ProForm>
          );
        }}
        value={record}
        onChange={(v) => updateItem(record, v)}
      ></ModelLink>,
      <Button
        onClick={() => {
          return deleteItem(record);
        }}
        danger
      >
        删除
      </Button>,
      <Button
        disabled={record == thisvalue[0]}
        icon={<UpOutlined />}
        onClick={() => tryMove(record, true)}
      ></Button>,
      <Button
        disabled={record == thisvalue[thisvalue.length - 1]}
        icon={<DownOutlined />}
        onClick={() => tryMove(record, false)}
      ></Button>,
    ],
  });

  return (
    <ProTable
      rowKey="id"
      columns={columns}
      dataSource={thisvalue}
      search={false}
      formRef={ref}
      toolBarRender={() => [
        <ModelLink
          type="primary"
          buttonName="新建"
          modelTitle={'新建:'}
          BodyContent={({ onChange, onHideModal, value }) => {
            return (
              <ProForm
                onFinish={(v) => {
                  onChange(v);
                  onHideModal();
                }}
                initialValues={value}
              >
                <ProFormText name="key" label="属性名" rules={[{ required: true }]}></ProFormText>
                {children}
              </ProForm>
            );
          }}
          onChange={addItem}
        ></ModelLink>,
      ]}
      pagination={false}
    ></ProTable>
  );
};

export const ProFormJsonDictory: React.FC<{}> = ({ children, ...props }) => {
  return (
    <ProForm.Item {...props}>
      <ProFormJsonDictoryView {...props}>{children}</ProFormJsonDictoryView>
    </ProForm.Item>
  );
};
