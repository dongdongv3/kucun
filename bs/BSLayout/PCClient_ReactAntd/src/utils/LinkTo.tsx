import { mgr } from '../../config/oidc';
import { Link } from 'umi';
import { Button } from 'antd';
import routeData from '../../config/routes';
import Auth from './Auth';

interface LinkToProperty {
  path: string;
  successGoto?: number | string;
  query?: any;
  children?: any;
  enable?: boolean;
  buttonProp?: any;
  icon?: any;
}

const getQueryString = (obj: any) => {
  let str = '';
  for (const x in obj) {
    if (obj[x] && obj[x] != null && obj[x] != 'null' && obj[x] != 'undefined') {
      str += `${x}=${obj[x]}&`;
    }
  }
  str = str.substring(0, str.length - 1);
  return str;
};

const LinkTo: React.FC<LinkToProperty> = ({
  path,
  successGoto,
  children,
  query,
  enable,
  buttonProp,
  icon,
}) => {
  const toUrl = path + '?' + getQueryString({ ...query, successGoto });
  console.log('linkto:', toUrl);
  let isenable = true;
  if (enable != undefined) {
    isenable = enable;
  }
  const subbody = (
    <Link to={toUrl}>
      <Button disabled={!isenable} icon={icon} {...buttonProp}>
        {children}
      </Button>
    </Link>
  );
  const routeitem = routeData.filter((x) => x.path == path && x.haveAuth == true);
  if (routeitem.length == 0) {
    return subbody;
  } else {
    return (
      <Auth auths={routeitem[0].auths} roles={routeitem[0].roles}>
        {subbody}
      </Auth>
    );
  }
};

export default LinkTo;
