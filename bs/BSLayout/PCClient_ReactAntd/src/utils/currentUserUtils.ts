import { reqGet } from './requestUtil';

export interface CurrentUser {
  userID: string;
  userName: string;
  roles: any;
  auths: any;
}

export async function queryCurrentUser(): Promise<CurrentUser | undefined> {
  const tmp = await reqGet('/api/AppData/GetUser', {});
  return {
    userID: tmp.id,
    userName: tmp.displayName,
    roles: tmp.roles,
    auths: tmp.auths,
  };
}
