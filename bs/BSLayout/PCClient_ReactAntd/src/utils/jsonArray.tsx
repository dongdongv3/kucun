/**
 *
 * list 类型的form.item
 */

import React, { useRef, useState } from 'react';
import ProTable from '@ant-design/pro-table';
import ModelLink from './ModelLink';
import ProForm, { ProFormInstance } from '@ant-design/pro-form';
import { Button } from 'antd';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import ReactJson from 'react-json-view';

/**
 * 对象类型的集合编辑器，是form.item 组件，直接被ItemConvert使用，用于对象类型构成的集合的编辑操作
 */

interface ListViewItemObjType {
  value?: any[]; //受控值
  onChange?: (value: any[]) => void; //受控函数
  [porpert: string]: any; //其他扩展
}

const ListViewItemObj: React.FC<ListViewItemObjType> = ({
  value = [],
  onChange,
  children,
  ...other
}) => {
  const [thisvalue, setthisvalue] = useState(
    (value || []).map((x) => {
      return { value: x };
    }),
  );
  const ref = useRef<ProFormInstance>();
  const deleteItem = async (item) => {
    const newvalue = thisvalue.filter((x) => x != item);
    setthisvalue(newvalue);
    if (onChange) {
      onChange(newvalue.map((x) => x.value));
    }
  };

  const addItem = async (itemvalue: any) => {
    const tmp = [...thisvalue, { value: itemvalue }];
    setthisvalue(tmp);
    if (onChange) {
      onChange(tmp.map((x) => x.value));
    }
  };

  const tryMove = (rec: any, isUp: boolean) => {
    const tmpList = [...thisvalue];
    for (let i = 0; i < tmpList.length; i++) {
      const item = tmpList[i];
      if (item == rec) {
        if (isUp && i != 0) {
          tmpList[i] = tmpList[i - 1];
          tmpList[i - 1] = item;
          break;
        }
        if (!isUp && i != tmpList.length - 1) {
          tmpList[i] = tmpList[i + 1];
          tmpList[i + 1] = item;
          break;
        }
      }
    }
    setthisvalue(tmpList);
    if (onChange) {
      onChange(tmpList.map((x) => x.value));
    }
  };

  const updateItem = async (oldvalue, itemvalue: any) => {
    const newvalue = [...thisvalue];
    newvalue.forEach((v, index) => {
      if (v == oldvalue) {
        newvalue[index] = { value: itemvalue };
      }
    });
    setthisvalue(newvalue);
    if (onChange) {
      onChange(newvalue.map((x) => x.value));
    }
  };

  //生成列
  const columns: any[] = [];
  columns.push({
    title: '序号',
    dataIndex: 'index',
    valueType: 'indexBorder',
    width: 48,
  });
  columns.push({
    title: '值',
    dataIndex: 'value',
    render: (text: any, record: any, index: number) => {
      if (record?.value)
        if (typeof record.value == typeof {})
          return (
            <ReactJson
              src={record.value}
              name={null}
              displayDataTypes={false}
              collapsed={0}
              enableClipboard={false}
              iconStyle="square"
            />
          );
        else return record.value;
      else return '';
    },
  });

  columns.push({
    title: '操作',
    valueType: 'option',
    render: (text, record, _, action) => [
      <ModelLink
        type="primary"
        buttonName="编辑"
        modelTitle={`编辑`}
        BodyContent={({ onChange, onHideModal, value }) => {
          return (
            <ProForm
              onFinish={(v) => {
                onChange(v.value);
                onHideModal();
              }}
              initialValues={{ value: value }}
            >
              {children}
            </ProForm>
          );
        }}
        value={record.value}
        onChange={(v) => updateItem(record, v)}
      ></ModelLink>,
      <Button
        onClick={() => {
          return deleteItem(record);
        }}
        danger
      >
        删除
      </Button>,
      <Button
        disabled={record == thisvalue[0]}
        icon={<UpOutlined />}
        onClick={() => tryMove(record, true)}
      ></Button>,
      <Button
        disabled={record == thisvalue[thisvalue.length - 1]}
        icon={<DownOutlined />}
        onClick={() => tryMove(record, false)}
      ></Button>,
    ],
  });

  return (
    <ProTable
      rowKey="id"
      columns={columns}
      dataSource={thisvalue}
      search={false}
      formRef={ref}
      toolBarRender={() => [
        <ModelLink
          type="primary"
          buttonName="新建"
          modelTitle={'新建:'}
          BodyContent={({ onChange, onHideModal, value }) => {
            return (
              <ProForm
                onFinish={(v) => {
                  onChange(v.value);
                  onHideModal();
                }}
                initialValues={{ value: value }}
              >
                {children}
              </ProForm>
            );
          }}
          onChange={addItem}
        ></ModelLink>,
      ]}
      pagination={false}
    ></ProTable>
  );
};

export default ListViewItemObj;

export const ProFormJsonArray: React.FC<ListViewItemObjType> = ({ children, ...props }) => {
  return (
    <ProForm.Item {...props}>
      <ListViewItemObj {...props}>{children}</ListViewItemObj>
    </ProForm.Item>
  );
};
