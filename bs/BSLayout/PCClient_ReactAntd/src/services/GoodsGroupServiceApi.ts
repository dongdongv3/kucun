import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type GoodsAddRequest = {
  name: any;
  count: number;
};

export async function goodsAdd(request: GoodsAddRequest): Promise<optionsResoult> {
  return reqPost('/api/GoodsGroupService/goodsAdd', request);
}

export async function goodsAdd_GetDefaultValue(request: any) {
  return reqPost('/api/GoodsGroupService/goodsAdd_GetDefaultValue', request);
}

type GoodsEditUpdateRequest = {
  id: any;
  name: any;
  count: number;
};

type GoodsEditQueryRequest = {
  id: any;
};

type GoodsEditResponse = {
  id: any;
  name: any;
  count: number;
};

export async function goodsEdit(request: GoodsEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/GoodsGroupService/goodsEdit', request);
}

export async function goodsEdit_GetValue(
  request: GoodsEditQueryRequest,
): Promise<GoodsEditResponse> {
  return reqPost('/api/GoodsGroupService/goodsEdit_GetValue', request);
}

type GoodsDetailQueryRequest = {
  id: any;
};

type GoodsDetailResponse = {
  id: any;
  name: any;
  count: number;
};

export async function goodsDetail_GetValue(
  request: GoodsDetailQueryRequest,
): Promise<GoodsDetailResponse> {
  return reqPost('/api/GoodsGroupService/goodsDetail_GetValue', request);
}

type GoodsDeleteRequest = {
  id: any;
};

export async function goodsDelete(request: GoodsDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/GoodsGroupService/goodsDelete', request);
}

type GoodsSearchRequest = {};

export async function goodsSearch(request: GoodsSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/GoodsGroupService/GoodsSearch', request, sort, filter);
}
export async function goodsSearch_GetDefaultValue(request: any) {
  return reqPost('/api/GoodsGroupService/goodsSearch_GetDefaultValue', request);
}
