// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { access_token } from '../../../config/oidc';

/** 获取当前的用户 GET /api/currentUser */
export async function currentUser(options?: { [key: string]: any }) {
  console.log('access_token');
  console.log(access_token);
  console.log(options);
  if (options) {
    options.headers['Authorization'] = 'Bearer ' + access_token;
  } else {
    options = { headers: { Authorization: 'Bearer ' + access_token } };
  }

  return request<{
    data: any;
  }>('/api/AppData/GetUser', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/notices */
export async function getNotices(options?: { [key: string]: any }) {
  return request<API.NoticeIconList>('/api/notices', {
    method: 'GET',
    ...(options || {}),
  });
}
