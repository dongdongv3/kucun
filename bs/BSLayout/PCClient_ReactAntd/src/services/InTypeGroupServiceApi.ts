import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type InTypeAddRequest = {
  name: any;
};

export async function inTypeAdd(request: InTypeAddRequest): Promise<optionsResoult> {
  return reqPost('/api/InTypeGroupService/inTypeAdd', request);
}

export async function inTypeAdd_GetDefaultValue(request: any) {
  return reqPost('/api/InTypeGroupService/inTypeAdd_GetDefaultValue', request);
}

type InTypeDeleteRequest = {
  id: any;
};

export async function inTypeDelete(request: InTypeDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/InTypeGroupService/inTypeDelete', request);
}

type InTypeSearchRequest = {};

export async function inTypeSearch(request: InTypeSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/InTypeGroupService/InTypeSearch', request, sort, filter);
}
export async function inTypeSearch_GetDefaultValue(request: any) {
  return reqPost('/api/InTypeGroupService/inTypeSearch_GetDefaultValue', request);
}
