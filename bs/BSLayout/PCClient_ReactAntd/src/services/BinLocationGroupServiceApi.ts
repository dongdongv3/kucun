import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type BinLocationAddRequest = {
  name: any;
};

export async function binLocationAdd(request: BinLocationAddRequest): Promise<optionsResoult> {
  return reqPost('/api/BinLocationGroupService/binLocationAdd', request);
}

export async function binLocationAdd_GetDefaultValue(request: any) {
  return reqPost('/api/BinLocationGroupService/binLocationAdd_GetDefaultValue', request);
}

type BinLocationEditUpdateRequest = {
  id: any;
  name: any;
};

type BinLocationEditQueryRequest = {
  id: any;
};

type BinLocationEditResponse = {
  id: any;
  name: any;
};

export async function binLocationEdit(
  request: BinLocationEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/BinLocationGroupService/binLocationEdit', request);
}

export async function binLocationEdit_GetValue(
  request: BinLocationEditQueryRequest,
): Promise<BinLocationEditResponse> {
  return reqPost('/api/BinLocationGroupService/binLocationEdit_GetValue', request);
}

type BinLocationDetailQueryRequest = {
  id: any;
};

type BinLocationDetailResponse = {
  id: any;
  name: any;
};

export async function binLocationDetail_GetValue(
  request: BinLocationDetailQueryRequest,
): Promise<BinLocationDetailResponse> {
  return reqPost('/api/BinLocationGroupService/binLocationDetail_GetValue', request);
}

type BinLocationDeleteRequest = {
  id: any;
};

export async function binLocationDelete(
  request: BinLocationDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/BinLocationGroupService/binLocationDelete', request);
}

type BinLocationSearchRequest = {};

export async function binLocationSearch(request: BinLocationSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/BinLocationGroupService/BinLocationSearch', request, sort, filter);
}
export async function binLocationSearch_GetDefaultValue(request: any) {
  return reqPost('/api/BinLocationGroupService/binLocationSearch_GetDefaultValue', request);
}
