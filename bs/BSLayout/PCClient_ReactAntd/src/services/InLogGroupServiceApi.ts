import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type InLogAddRequest = {
  createTime: any;
  goods: any;
  binLocation: any;
  count: number;
  inTypeInfo: any;
};

export async function inLogAdd(request: InLogAddRequest): Promise<optionsResoult> {
  return reqPost('/api/InLogGroupService/inLogAdd', request);
}

export async function inLogAdd_GetDefaultValue(request: any) {
  return reqPost('/api/InLogGroupService/inLogAdd_GetDefaultValue', request);
}

export async function inLogAdd_goods_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogAdd_goods_DataSource', pageParameter);
}

export async function inLogAdd_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogAdd_BinLocation_DataSource', pageParameter);
}

export async function inLogAdd_InTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogAdd_InTypeInfo_DataSource', pageParameter);
}

type InLogEditUpdateRequest = {
  id: any;
  createTime: any;
  goods: any;
  binLocation: any;
  count: number;
  inTypeInfo: any;
};

type InLogEditQueryRequest = {
  id: any;
};

type InLogEditResponse = {
  id: any;
  createTime: any;
  goods: any;
  binLocation: any;
  count: number;
  inTypeInfo: any;
};

export async function inLogEdit(request: InLogEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/InLogGroupService/inLogEdit', request);
}

export async function inLogEdit_GetValue(
  request: InLogEditQueryRequest,
): Promise<InLogEditResponse> {
  return reqPost('/api/InLogGroupService/inLogEdit_GetValue', request);
}

export async function inLogEdit_goods_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogEdit_goods_DataSource', pageParameter);
}

export async function inLogEdit_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogEdit_BinLocation_DataSource', pageParameter);
}

export async function inLogEdit_InTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogEdit_InTypeInfo_DataSource', pageParameter);
}

type InLogDetailQueryRequest = {
  id: any;
};

type InLogDetailResponse = {
  id: any;
  createTime: any;
  goods: any;
  binLocation: any;
  count: number;
  inTypeInfo: any;
};

export async function inLogDetail_GetValue(
  request: InLogDetailQueryRequest,
): Promise<InLogDetailResponse> {
  return reqPost('/api/InLogGroupService/inLogDetail_GetValue', request);
}

export async function inLogDetail_goods_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogDetail_goods_DataSource', pageParameter);
}

export async function inLogDetail_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogDetail_BinLocation_DataSource', pageParameter);
}

export async function inLogDetail_InTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogDetail_InTypeInfo_DataSource', pageParameter);
}

type InLogDeleteRequest = {
  id: any;
};

export async function inLogDelete(request: InLogDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/InLogGroupService/inLogDelete', request);
}

type InLogSearchRequest = {};

export async function inLogSearch(request: InLogSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/InLogGroupService/InLogSearch', request, sort, filter);
}
export async function inLogSearch_GetDefaultValue(request: any) {
  return reqPost('/api/InLogGroupService/inLogSearch_GetDefaultValue', request);
}

export async function inLogSearch_goods_View_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogSearch_goods_View_DataSource', pageParameter);
}

export async function inLogSearch_BinLocation_View_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogSearch_BinLocation_View_DataSource', pageParameter);
}

export async function inLogSearch_InTypeInfo_View_DataSource(pageParameter: any) {
  return reqGet('/api/InLogGroupService/InLogSearch_InTypeInfo_View_DataSource', pageParameter);
}
