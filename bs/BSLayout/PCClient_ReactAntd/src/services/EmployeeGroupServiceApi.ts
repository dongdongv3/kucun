import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type Employee_AccountAndPasswordAuth1AddEmployeeRequest = {
  loginID: any;
  pwd: any;
};

export async function employee_AccountAndPasswordAuth1AddEmployee(
  request: Employee_AccountAndPasswordAuth1AddEmployeeRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AccountAndPasswordAuth1AddEmployee', request);
}

export async function employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue',
    request,
  );
}

export async function employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1AddEmployee_RefEmployeeId_DataSource',
    pageParameter,
  );
}

type Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest = {
  id: any;
  loginID: any;
  pwd: any;
};

type Employee_AccountAndPasswordAuth1EditEmployeeQueryRequest = {
  id: any;
};

type Employee_AccountAndPasswordAuth1EditEmployeeResponse = {
  id: any;
  loginID: any;
  pwd: any;
  refEmployeeId: any;
};

export async function employee_AccountAndPasswordAuth1EditEmployee(
  request: Employee_AccountAndPasswordAuth1EditEmployeeUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AccountAndPasswordAuth1EditEmployee', request);
}

export async function employee_AccountAndPasswordAuth1EditEmployee_GetValue(
  request: Employee_AccountAndPasswordAuth1EditEmployeeQueryRequest,
): Promise<Employee_AccountAndPasswordAuth1EditEmployeeResponse> {
  return reqPost(
    '/api/EmployeeGroupService/employee_AccountAndPasswordAuth1EditEmployee_GetValue',
    request,
  );
}

export async function employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1EditEmployee_RefEmployeeId_DataSource',
    pageParameter,
  );
}

type Employee_AccountAndPasswordAuth1DeleteRequest = {
  id: any;
};

export async function employee_AccountAndPasswordAuth1Delete(
  request: Employee_AccountAndPasswordAuth1DeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AccountAndPasswordAuth1Delete', request);
}

type Employee_AccountAndPasswordAuth1DetailQueryRequest = {
  id: any;
};

type Employee_AccountAndPasswordAuth1DetailResponse = {
  id: any;
  loginID: any;
  pwd: any;
  refEmployeeId: any;
};

export async function employee_AccountAndPasswordAuth1Detail_GetValue(
  request: Employee_AccountAndPasswordAuth1DetailQueryRequest,
): Promise<Employee_AccountAndPasswordAuth1DetailResponse> {
  return reqPost(
    '/api/EmployeeGroupService/employee_AccountAndPasswordAuth1Detail_GetValue',
    request,
  );
}

export async function employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource',
    pageParameter,
  );
}

type Employee_AccountAndPasswordAuth1SearchEmployeeRequest = {};

export async function employee_AccountAndPasswordAuth1SearchEmployee(
  request: Employee_AccountAndPasswordAuth1SearchEmployeeRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1SearchEmployee',
    request,
    sort,
    filter,
  );
}
export async function employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_AccountAndPasswordAuth1SearchEmployee_GetDefaultValue',
    request,
  );
}

export async function employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_Where_DataSource',
    pageParameter,
  );
}

export async function employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_AccountAndPasswordAuth1SearchEmployee_RefEmployeeId_View_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapAddEmployeeRequest = {
  refEmployee_RoleId: any;
};

export async function employee_UserRoleMapAddEmployee(
  request: Employee_UserRoleMapAddEmployeeRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapAddEmployee', request);
}

export async function employee_UserRoleMapAddEmployee_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_UserRoleMapAddEmployee_GetDefaultValue',
    request,
  );
}

export async function employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapAddEmployee_RefEmployeeId_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapAddEmployee_RoleRequest = {
  refEmployeeId: any;
};

export async function employee_UserRoleMapAddEmployee_Role(
  request: Employee_UserRoleMapAddEmployee_RoleRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapAddEmployee_Role', request);
}

export async function employee_UserRoleMapAddEmployee_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_UserRoleMapAddEmployee_Role_GetDefaultValue',
    request,
  );
}

export async function employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapAddEmployee_Role_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapEditEmployeeUpdateRequest = {
  id: any;
  refEmployee_RoleId: any;
};

type Employee_UserRoleMapEditEmployeeQueryRequest = {
  id: any;
};

type Employee_UserRoleMapEditEmployeeResponse = {
  id: any;
  refEmployeeId: any;
  refEmployee_RoleId: any;
};

export async function employee_UserRoleMapEditEmployee(
  request: Employee_UserRoleMapEditEmployeeUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapEditEmployee', request);
}

export async function employee_UserRoleMapEditEmployee_GetValue(
  request: Employee_UserRoleMapEditEmployeeQueryRequest,
): Promise<Employee_UserRoleMapEditEmployeeResponse> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapEditEmployee_GetValue', request);
}

export async function employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapEditEmployee_RefEmployeeId_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapEditEmployee_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapEditEmployee_RoleUpdateRequest = {
  id: any;
  refEmployeeId: any;
};

type Employee_UserRoleMapEditEmployee_RoleQueryRequest = {
  id: any;
};

type Employee_UserRoleMapEditEmployee_RoleResponse = {
  id: any;
  refEmployeeId: any;
  refEmployee_RoleId: any;
};

export async function employee_UserRoleMapEditEmployee_Role(
  request: Employee_UserRoleMapEditEmployee_RoleUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapEditEmployee_Role', request);
}

export async function employee_UserRoleMapEditEmployee_Role_GetValue(
  request: Employee_UserRoleMapEditEmployee_RoleQueryRequest,
): Promise<Employee_UserRoleMapEditEmployee_RoleResponse> {
  return reqPost(
    '/api/EmployeeGroupService/employee_UserRoleMapEditEmployee_Role_GetValue',
    request,
  );
}

export async function employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapDeleteRequest = {
  id: any;
};

export async function employee_UserRoleMapDelete(
  request: Employee_UserRoleMapDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapDelete', request);
}

type Employee_UserRoleMapDetailQueryRequest = {
  id: any;
};

type Employee_UserRoleMapDetailResponse = {
  id: any;
  refEmployeeId: any;
  refEmployee_RoleId: any;
};

export async function employee_UserRoleMapDetail_GetValue(
  request: Employee_UserRoleMapDetailQueryRequest,
): Promise<Employee_UserRoleMapDetailResponse> {
  return reqPost('/api/EmployeeGroupService/employee_UserRoleMapDetail_GetValue', request);
}

export async function employee_UserRoleMapDetail_RefEmployeeId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapDetail_RefEmployeeId_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapSearchEmployeeRequest = {};

export async function employee_UserRoleMapSearchEmployee(
  request: Employee_UserRoleMapSearchEmployeeRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee',
    request,
    sort,
    filter,
  );
}
export async function employee_UserRoleMapSearchEmployee_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_UserRoleMapSearchEmployee_GetDefaultValue',
    request,
  );
}

export async function employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_RefEmployeeId_Where_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_RefEmployeeId_View_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_RefEmployee_RoleId_View_DataSource',
    pageParameter,
  );
}

type Employee_UserRoleMapSearchEmployee_RoleRequest = {};

export async function employee_UserRoleMapSearchEmployee_Role(
  request: Employee_UserRoleMapSearchEmployee_RoleRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_Role',
    request,
    sort,
    filter,
  );
}
export async function employee_UserRoleMapSearchEmployee_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_UserRoleMapSearchEmployee_Role_GetDefaultValue',
    request,
  );
}

export async function employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_Role_RefEmployeeId_View_DataSource',
    pageParameter,
  );
}

export async function employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_UserRoleMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapAddEmployee_RoleRequest = {
  refEmployee_AuthId: any;
};

export async function employee_RoleAuthMapAddEmployee_Role(
  request: Employee_RoleAuthMapAddEmployee_RoleRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapAddEmployee_Role', request);
}

export async function employee_RoleAuthMapAddEmployee_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapAddEmployee_Role_GetDefaultValue',
    request,
  );
}

export async function employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapAddEmployee_Role_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapAddEmployee_Role_RefEmployee_AuthId_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapAddEmployee_AuthRequest = {
  refEmployee_RoleId: any;
};

export async function employee_RoleAuthMapAddEmployee_Auth(
  request: Employee_RoleAuthMapAddEmployee_AuthRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapAddEmployee_Auth', request);
}

export async function employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue',
    request,
  );
}

export async function employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapAddEmployee_Auth_RefEmployee_AuthId_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapEditEmployee_RoleUpdateRequest = {
  id: any;
  refEmployee_AuthId: any;
};

type Employee_RoleAuthMapEditEmployee_RoleQueryRequest = {
  id: any;
};

type Employee_RoleAuthMapEditEmployee_RoleResponse = {
  id: any;
  refEmployee_RoleId: any;
  refEmployee_AuthId: any;
};

export async function employee_RoleAuthMapEditEmployee_Role(
  request: Employee_RoleAuthMapEditEmployee_RoleUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapEditEmployee_Role', request);
}

export async function employee_RoleAuthMapEditEmployee_Role_GetValue(
  request: Employee_RoleAuthMapEditEmployee_RoleQueryRequest,
): Promise<Employee_RoleAuthMapEditEmployee_RoleResponse> {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapEditEmployee_Role_GetValue',
    request,
  );
}

export async function employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapEditEmployee_Role_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapEditEmployee_Role_RefEmployee_AuthId_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapEditEmployee_AuthUpdateRequest = {
  id: any;
  refEmployee_RoleId: any;
};

type Employee_RoleAuthMapEditEmployee_AuthQueryRequest = {
  id: any;
};

type Employee_RoleAuthMapEditEmployee_AuthResponse = {
  id: any;
  refEmployee_RoleId: any;
  refEmployee_AuthId: any;
};

export async function employee_RoleAuthMapEditEmployee_Auth(
  request: Employee_RoleAuthMapEditEmployee_AuthUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapEditEmployee_Auth', request);
}

export async function employee_RoleAuthMapEditEmployee_Auth_GetValue(
  request: Employee_RoleAuthMapEditEmployee_AuthQueryRequest,
): Promise<Employee_RoleAuthMapEditEmployee_AuthResponse> {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapEditEmployee_Auth_GetValue',
    request,
  );
}

export async function employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapEditEmployee_Auth_RefEmployee_AuthId_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapDeleteRequest = {
  id: any;
};

export async function employee_RoleAuthMapDelete(
  request: Employee_RoleAuthMapDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapDelete', request);
}

type Employee_RoleAuthMapDetailQueryRequest = {
  id: any;
};

type Employee_RoleAuthMapDetailResponse = {
  id: any;
  refEmployee_RoleId: any;
  refEmployee_AuthId: any;
};

export async function employee_RoleAuthMapDetail_GetValue(
  request: Employee_RoleAuthMapDetailQueryRequest,
): Promise<Employee_RoleAuthMapDetailResponse> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAuthMapDetail_GetValue', request);
}

export async function employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapSearchEmployee_RoleRequest = {};

export async function employee_RoleAuthMapSearchEmployee_Role(
  request: Employee_RoleAuthMapSearchEmployee_RoleRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Role',
    request,
    sort,
    filter,
  );
}
export async function employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue',
    request,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_Where_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource',
    pageParameter,
  );
}

type Employee_RoleAuthMapSearchEmployee_AuthRequest = {};

export async function employee_RoleAuthMapSearchEmployee_Auth(
  request: Employee_RoleAuthMapSearchEmployee_AuthRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Auth',
    request,
    sort,
    filter,
  );
}
export async function employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue(request: any) {
  return reqPost(
    '/api/EmployeeGroupService/employee_RoleAuthMapSearchEmployee_Auth_GetDefaultValue',
    request,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_Where_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_RoleId_View_DataSource',
    pageParameter,
  );
}

export async function employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/EmployeeGroupService/Employee_RoleAuthMapSearchEmployee_Auth_RefEmployee_AuthId_View_DataSource',
    pageParameter,
  );
}

type Employee_AuthAddRequest = {
  authName: any;
};

export async function employee_AuthAdd(request: Employee_AuthAddRequest): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AuthAdd', request);
}

export async function employee_AuthAdd_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employee_AuthAdd_GetDefaultValue', request);
}

type Employee_AuthEditUpdateRequest = {
  id: any;
  authName: any;
};

type Employee_AuthEditQueryRequest = {
  id: any;
};

type Employee_AuthEditResponse = {
  id: any;
  authName: any;
};

export async function employee_AuthEdit(
  request: Employee_AuthEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AuthEdit', request);
}

export async function employee_AuthEdit_GetValue(
  request: Employee_AuthEditQueryRequest,
): Promise<Employee_AuthEditResponse> {
  return reqPost('/api/EmployeeGroupService/employee_AuthEdit_GetValue', request);
}

type Employee_AuthDeleteRequest = {
  id: any;
};

export async function employee_AuthDelete(
  request: Employee_AuthDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_AuthDelete', request);
}

type Employee_AuthDetailQueryRequest = {
  id: any;
};

type Employee_AuthDetailResponse = {
  id: any;
  authName: any;
};

export async function employee_AuthDetail_GetValue(
  request: Employee_AuthDetailQueryRequest,
): Promise<Employee_AuthDetailResponse> {
  return reqPost('/api/EmployeeGroupService/employee_AuthDetail_GetValue', request);
}

type Employee_AuthSearchRequest = {};

export async function employee_AuthSearch(
  request: Employee_AuthSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/EmployeeGroupService/Employee_AuthSearch', request, sort, filter);
}
export async function employee_AuthSearch_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employee_AuthSearch_GetDefaultValue', request);
}

type Employee_RoleAddRequest = {
  roleName: any;
};

export async function employee_RoleAdd(request: Employee_RoleAddRequest): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleAdd', request);
}

export async function employee_RoleAdd_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employee_RoleAdd_GetDefaultValue', request);
}

type Employee_RoleEditUpdateRequest = {
  id: any;
  roleName: any;
};

type Employee_RoleEditQueryRequest = {
  id: any;
};

type Employee_RoleEditResponse = {
  id: any;
  roleName: any;
};

export async function employee_RoleEdit(
  request: Employee_RoleEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleEdit', request);
}

export async function employee_RoleEdit_GetValue(
  request: Employee_RoleEditQueryRequest,
): Promise<Employee_RoleEditResponse> {
  return reqPost('/api/EmployeeGroupService/employee_RoleEdit_GetValue', request);
}

type Employee_RoleDeleteRequest = {
  id: any;
};

export async function employee_RoleDelete(
  request: Employee_RoleDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employee_RoleDelete', request);
}

type Employee_RoleDetailQueryRequest = {
  id: any;
};

type Employee_RoleDetailResponse = {
  id: any;
  roleName: any;
};

export async function employee_RoleDetail_GetValue(
  request: Employee_RoleDetailQueryRequest,
): Promise<Employee_RoleDetailResponse> {
  return reqPost('/api/EmployeeGroupService/employee_RoleDetail_GetValue', request);
}

type Employee_RoleSearchRequest = {};

export async function employee_RoleSearch(
  request: Employee_RoleSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/EmployeeGroupService/Employee_RoleSearch', request, sort, filter);
}
export async function employee_RoleSearch_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employee_RoleSearch_GetDefaultValue', request);
}

type EmployeeAddRequest = {
  id: any;
  displayName: any;
};

export async function employeeAdd(request: EmployeeAddRequest): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employeeAdd', request);
}

export async function employeeAdd_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employeeAdd_GetDefaultValue', request);
}

type EmployeeEditUpdateRequest = {
  id: any;
  displayName: any;
};

type EmployeeEditQueryRequest = {
  id: any;
};

type EmployeeEditResponse = {
  id: any;
  displayName: any;
};

export async function employeeEdit(request: EmployeeEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employeeEdit', request);
}

export async function employeeEdit_GetValue(
  request: EmployeeEditQueryRequest,
): Promise<EmployeeEditResponse> {
  return reqPost('/api/EmployeeGroupService/employeeEdit_GetValue', request);
}

type EmployeeDeleteRequest = {
  id: any;
};

export async function employeeDelete(request: EmployeeDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/EmployeeGroupService/employeeDelete', request);
}

type EmployeeDetailQueryRequest = {
  id: any;
};

type EmployeeDetailResponse = {
  id: any;
  displayName: any;
};

export async function employeeDetail_GetValue(
  request: EmployeeDetailQueryRequest,
): Promise<EmployeeDetailResponse> {
  return reqPost('/api/EmployeeGroupService/employeeDetail_GetValue', request);
}

type EmployeeSearchRequest = {};

export async function employeeSearch(request: EmployeeSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/EmployeeGroupService/EmployeeSearch', request, sort, filter);
}
export async function employeeSearch_GetDefaultValue(request: any) {
  return reqPost('/api/EmployeeGroupService/employeeSearch_GetDefaultValue', request);
}
