import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type OutLogAddRequest = {
  createtime: any;
  goods: any;
  count: number;
  outTypeInfo: any;
  binLocation: any;
};

export async function outLogAdd(request: OutLogAddRequest): Promise<optionsResoult> {
  return reqPost('/api/OutLogGroupService/outLogAdd', request);
}

export async function outLogAdd_GetDefaultValue(request: any) {
  return reqPost('/api/OutLogGroupService/outLogAdd_GetDefaultValue', request);
}

export async function outLogAdd_goods_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogAdd_goods_DataSource', pageParameter);
}

export async function outLogAdd_OutTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogAdd_OutTypeInfo_DataSource', pageParameter);
}

export async function outLogAdd_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogAdd_BinLocation_DataSource', pageParameter);
}

type OutLogEditUpdateRequest = {
  id: any;
  createtime: any;
  goods: any;
  count: number;
  outTypeInfo: any;
  binLocation: any;
};

type OutLogEditQueryRequest = {
  id: any;
};

type OutLogEditResponse = {
  id: any;
  createtime: any;
  goods: any;
  count: number;
  outTypeInfo: any;
  binLocation: any;
};

export async function outLogEdit(request: OutLogEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/OutLogGroupService/outLogEdit', request);
}

export async function outLogEdit_GetValue(
  request: OutLogEditQueryRequest,
): Promise<OutLogEditResponse> {
  return reqPost('/api/OutLogGroupService/outLogEdit_GetValue', request);
}

export async function outLogEdit_goods_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogEdit_goods_DataSource', pageParameter);
}

export async function outLogEdit_OutTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogEdit_OutTypeInfo_DataSource', pageParameter);
}

export async function outLogEdit_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogEdit_BinLocation_DataSource', pageParameter);
}

type OutLogDetailQueryRequest = {
  id: any;
};

type OutLogDetailResponse = {
  id: any;
  createtime: any;
  goods: any;
  count: number;
  outTypeInfo: any;
  binLocation: any;
};

export async function outLogDetail_GetValue(
  request: OutLogDetailQueryRequest,
): Promise<OutLogDetailResponse> {
  return reqPost('/api/OutLogGroupService/outLogDetail_GetValue', request);
}

export async function outLogDetail_goods_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogDetail_goods_DataSource', pageParameter);
}

export async function outLogDetail_OutTypeInfo_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogDetail_OutTypeInfo_DataSource', pageParameter);
}

export async function outLogDetail_BinLocation_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogDetail_BinLocation_DataSource', pageParameter);
}

type OutLogDeleteRequest = {
  id: any;
};

export async function outLogDelete(request: OutLogDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/OutLogGroupService/outLogDelete', request);
}

type OutLogSearchRequest = {};

export async function outLogSearch(request: OutLogSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/OutLogGroupService/OutLogSearch', request, sort, filter);
}
export async function outLogSearch_GetDefaultValue(request: any) {
  return reqPost('/api/OutLogGroupService/outLogSearch_GetDefaultValue', request);
}

export async function outLogSearch_goods_View_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogSearch_goods_View_DataSource', pageParameter);
}

export async function outLogSearch_OutTypeInfo_View_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogSearch_OutTypeInfo_View_DataSource', pageParameter);
}

export async function outLogSearch_BinLocation_View_DataSource(pageParameter: any) {
  return reqGet('/api/OutLogGroupService/OutLogSearch_BinLocation_View_DataSource', pageParameter);
}
