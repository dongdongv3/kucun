import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type OutTypeAddRequest = {
  name: any;
};

export async function outTypeAdd(request: OutTypeAddRequest): Promise<optionsResoult> {
  return reqPost('/api/OutTypeGroupService/outTypeAdd', request);
}

export async function outTypeAdd_GetDefaultValue(request: any) {
  return reqPost('/api/OutTypeGroupService/outTypeAdd_GetDefaultValue', request);
}

type OutTypeDeleteRequest = {
  id: any;
};

export async function outTypeDelete(request: OutTypeDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/OutTypeGroupService/outTypeDelete', request);
}

type OutTypeSearchRequest = {};

export async function outTypeSearch(request: OutTypeSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/OutTypeGroupService/OutTypeSearch', request, sort, filter);
}
export async function outTypeSearch_GetDefaultValue(request: any) {
  return reqPost('/api/OutTypeGroupService/outTypeSearch_GetDefaultValue', request);
}
