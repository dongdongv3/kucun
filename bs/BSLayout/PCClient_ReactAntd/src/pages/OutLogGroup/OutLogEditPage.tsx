import OutLogEditCom from '@/components/OutLogGroup/OutLogEditCom';
import { useLocation } from 'umi';

const outLogEditCom = () => {
  const location = useLocation();
  return <OutLogEditCom params={location.query}></OutLogEditCom>;
};

export default outLogEditCom;
