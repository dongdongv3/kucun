import OutLogAddCom from '@/components/OutLogGroup/OutLogAddCom';
import { useLocation } from 'umi';

const outLogAddCom = () => {
  const location = useLocation();
  return <OutLogAddCom params={location.query}></OutLogAddCom>;
};

export default outLogAddCom;
