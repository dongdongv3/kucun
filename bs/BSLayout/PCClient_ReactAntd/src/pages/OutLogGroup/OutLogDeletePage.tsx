import OutLogDeleteCom from '@/components/OutLogGroup/OutLogDeleteCom';
import { useLocation } from 'umi';

const outLogDeleteCom = () => {
  const location = useLocation();
  return <OutLogDeleteCom params={location.query}></OutLogDeleteCom>;
};

export default outLogDeleteCom;
