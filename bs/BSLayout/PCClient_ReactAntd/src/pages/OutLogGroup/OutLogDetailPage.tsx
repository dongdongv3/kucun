import OutLogDetailCom from '@/components/OutLogGroup/OutLogDetailCom';
import { useLocation } from 'umi';

const outLogDetailCom = () => {
  const location = useLocation();
  return <OutLogDetailCom params={location.query}></OutLogDetailCom>;
};

export default outLogDetailCom;
