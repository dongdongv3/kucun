import OutLogSearchCom from '@/components/OutLogGroup/OutLogSearchCom';
import { useLocation } from 'umi';

const outLogSearchCom = () => {
  const location = useLocation();
  return <OutLogSearchCom params={location.query}></OutLogSearchCom>;
};

export default outLogSearchCom;
