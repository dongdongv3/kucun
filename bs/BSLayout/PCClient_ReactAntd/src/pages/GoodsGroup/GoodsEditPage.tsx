import GoodsEditCom from '@/components/GoodsGroup/GoodsEditCom';
import { useLocation } from 'umi';

const goodsEditCom = () => {
  const location = useLocation();
  return <GoodsEditCom params={location.query}></GoodsEditCom>;
};

export default goodsEditCom;
