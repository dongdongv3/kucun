import GoodsSearchCom from '@/components/GoodsGroup/GoodsSearchCom';
import { useLocation } from 'umi';

const goodsSearchCom = () => {
  const location = useLocation();
  return <GoodsSearchCom params={location.query}></GoodsSearchCom>;
};

export default goodsSearchCom;
