import GoodsDeleteCom from '@/components/GoodsGroup/GoodsDeleteCom';
import { useLocation } from 'umi';

const goodsDeleteCom = () => {
  const location = useLocation();
  return <GoodsDeleteCom params={location.query}></GoodsDeleteCom>;
};

export default goodsDeleteCom;
