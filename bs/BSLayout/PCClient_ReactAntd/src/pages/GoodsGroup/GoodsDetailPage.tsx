import GoodsDetailCom from '@/components/GoodsGroup/GoodsDetailCom';
import { useLocation } from 'umi';

const goodsDetailCom = () => {
  const location = useLocation();
  return <GoodsDetailCom params={location.query}></GoodsDetailCom>;
};

export default goodsDetailCom;
