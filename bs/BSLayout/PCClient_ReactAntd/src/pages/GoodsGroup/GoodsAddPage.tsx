import GoodsAddCom from '@/components/GoodsGroup/GoodsAddCom';
import { useLocation } from 'umi';

const goodsAddCom = () => {
  const location = useLocation();
  return <GoodsAddCom params={location.query}></GoodsAddCom>;
};

export default goodsAddCom;
