import OutTypeAddCom from '@/components/OutTypeGroup/OutTypeAddCom';
import { useLocation } from 'umi';

const outTypeAddCom = () => {
  const location = useLocation();
  return <OutTypeAddCom params={location.query}></OutTypeAddCom>;
};

export default outTypeAddCom;
