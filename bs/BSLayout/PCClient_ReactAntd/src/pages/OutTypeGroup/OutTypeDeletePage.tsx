import OutTypeDeleteCom from '@/components/OutTypeGroup/OutTypeDeleteCom';
import { useLocation } from 'umi';

const outTypeDeleteCom = () => {
  const location = useLocation();
  return <OutTypeDeleteCom params={location.query}></OutTypeDeleteCom>;
};

export default outTypeDeleteCom;
