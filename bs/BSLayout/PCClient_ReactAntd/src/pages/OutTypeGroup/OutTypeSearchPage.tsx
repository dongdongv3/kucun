import OutTypeSearchCom from '@/components/OutTypeGroup/OutTypeSearchCom';
import { useLocation } from 'umi';

const outTypeSearchCom = () => {
  const location = useLocation();
  return <OutTypeSearchCom params={location.query}></OutTypeSearchCom>;
};

export default outTypeSearchCom;
