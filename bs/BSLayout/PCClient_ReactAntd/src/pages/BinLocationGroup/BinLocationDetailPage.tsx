import BinLocationDetailCom from '@/components/BinLocationGroup/BinLocationDetailCom';
import { useLocation } from 'umi';

const binLocationDetailCom = () => {
  const location = useLocation();
  return <BinLocationDetailCom params={location.query}></BinLocationDetailCom>;
};

export default binLocationDetailCom;
