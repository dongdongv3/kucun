import BinLocationAddCom from '@/components/BinLocationGroup/BinLocationAddCom';
import { useLocation } from 'umi';

const binLocationAddCom = () => {
  const location = useLocation();
  return <BinLocationAddCom params={location.query}></BinLocationAddCom>;
};

export default binLocationAddCom;
