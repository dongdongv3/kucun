import BinLocationDeleteCom from '@/components/BinLocationGroup/BinLocationDeleteCom';
import { useLocation } from 'umi';

const binLocationDeleteCom = () => {
  const location = useLocation();
  return <BinLocationDeleteCom params={location.query}></BinLocationDeleteCom>;
};

export default binLocationDeleteCom;
