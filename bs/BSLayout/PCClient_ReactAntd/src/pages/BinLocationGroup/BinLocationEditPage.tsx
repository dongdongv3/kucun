import BinLocationEditCom from '@/components/BinLocationGroup/BinLocationEditCom';
import { useLocation } from 'umi';

const binLocationEditCom = () => {
  const location = useLocation();
  return <BinLocationEditCom params={location.query}></BinLocationEditCom>;
};

export default binLocationEditCom;
