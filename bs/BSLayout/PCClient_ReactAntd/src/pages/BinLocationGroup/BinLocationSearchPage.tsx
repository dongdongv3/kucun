import BinLocationSearchCom from '@/components/BinLocationGroup/BinLocationSearchCom';
import { useLocation } from 'umi';

const binLocationSearchCom = () => {
  const location = useLocation();
  return <BinLocationSearchCom params={location.query}></BinLocationSearchCom>;
};

export default binLocationSearchCom;
