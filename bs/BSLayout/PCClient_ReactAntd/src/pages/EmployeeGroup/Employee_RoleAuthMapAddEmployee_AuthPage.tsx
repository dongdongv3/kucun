import Employee_RoleAuthMapAddEmployee_AuthCom from '@/components/EmployeeGroup/Employee_RoleAuthMapAddEmployee_AuthCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapAddEmployee_AuthCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapAddEmployee_AuthCom
      params={location.query}
    ></Employee_RoleAuthMapAddEmployee_AuthCom>
  );
};

export default employee_RoleAuthMapAddEmployee_AuthCom;
