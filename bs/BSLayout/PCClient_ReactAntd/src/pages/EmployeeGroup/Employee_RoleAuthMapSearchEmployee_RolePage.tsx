import Employee_RoleAuthMapSearchEmployee_RoleCom from '@/components/EmployeeGroup/Employee_RoleAuthMapSearchEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapSearchEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapSearchEmployee_RoleCom
      params={location.query}
    ></Employee_RoleAuthMapSearchEmployee_RoleCom>
  );
};

export default employee_RoleAuthMapSearchEmployee_RoleCom;
