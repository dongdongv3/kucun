import Employee_UserRoleMapDeleteCom from '@/components/EmployeeGroup/Employee_UserRoleMapDeleteCom';
import { useLocation } from 'umi';

const employee_UserRoleMapDeleteCom = () => {
  const location = useLocation();
  return <Employee_UserRoleMapDeleteCom params={location.query}></Employee_UserRoleMapDeleteCom>;
};

export default employee_UserRoleMapDeleteCom;
