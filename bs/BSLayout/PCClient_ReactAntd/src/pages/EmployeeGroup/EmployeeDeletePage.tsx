import EmployeeDeleteCom from '@/components/EmployeeGroup/EmployeeDeleteCom';
import { useLocation } from 'umi';

const employeeDeleteCom = () => {
  const location = useLocation();
  return <EmployeeDeleteCom params={location.query}></EmployeeDeleteCom>;
};

export default employeeDeleteCom;
