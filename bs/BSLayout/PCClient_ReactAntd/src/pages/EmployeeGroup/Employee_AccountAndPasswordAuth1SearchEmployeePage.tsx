import Employee_AccountAndPasswordAuth1SearchEmployeeCom from '@/components/EmployeeGroup/Employee_AccountAndPasswordAuth1SearchEmployeeCom';
import { useLocation } from 'umi';

const employee_AccountAndPasswordAuth1SearchEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_AccountAndPasswordAuth1SearchEmployeeCom
      params={location.query}
    ></Employee_AccountAndPasswordAuth1SearchEmployeeCom>
  );
};

export default employee_AccountAndPasswordAuth1SearchEmployeeCom;
