import Employee_AccountAndPasswordAuth1EditEmployeeCom from '@/components/EmployeeGroup/Employee_AccountAndPasswordAuth1EditEmployeeCom';
import { useLocation } from 'umi';

const employee_AccountAndPasswordAuth1EditEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_AccountAndPasswordAuth1EditEmployeeCom
      params={location.query}
    ></Employee_AccountAndPasswordAuth1EditEmployeeCom>
  );
};

export default employee_AccountAndPasswordAuth1EditEmployeeCom;
