import Employee_UserRoleMapAddEmployee_RoleCom from '@/components/EmployeeGroup/Employee_UserRoleMapAddEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_UserRoleMapAddEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapAddEmployee_RoleCom
      params={location.query}
    ></Employee_UserRoleMapAddEmployee_RoleCom>
  );
};

export default employee_UserRoleMapAddEmployee_RoleCom;
