import Employee_RoleDeleteCom from '@/components/EmployeeGroup/Employee_RoleDeleteCom';
import { useLocation } from 'umi';

const employee_RoleDeleteCom = () => {
  const location = useLocation();
  return <Employee_RoleDeleteCom params={location.query}></Employee_RoleDeleteCom>;
};

export default employee_RoleDeleteCom;
