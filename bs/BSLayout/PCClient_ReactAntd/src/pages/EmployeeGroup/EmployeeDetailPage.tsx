import EmployeeDetailCom from '@/components/EmployeeGroup/EmployeeDetailCom';
import { useLocation } from 'umi';

const employeeDetailCom = () => {
  const location = useLocation();
  return <EmployeeDetailCom params={location.query}></EmployeeDetailCom>;
};

export default employeeDetailCom;
