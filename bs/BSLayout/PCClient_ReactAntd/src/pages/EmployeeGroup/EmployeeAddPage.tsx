import EmployeeAddCom from '@/components/EmployeeGroup/EmployeeAddCom';
import { useLocation } from 'umi';

const employeeAddCom = () => {
  const location = useLocation();
  return <EmployeeAddCom params={location.query}></EmployeeAddCom>;
};

export default employeeAddCom;
