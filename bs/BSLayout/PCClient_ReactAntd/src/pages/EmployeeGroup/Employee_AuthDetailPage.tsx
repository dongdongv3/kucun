import Employee_AuthDetailCom from '@/components/EmployeeGroup/Employee_AuthDetailCom';
import { useLocation } from 'umi';

const employee_AuthDetailCom = () => {
  const location = useLocation();
  return <Employee_AuthDetailCom params={location.query}></Employee_AuthDetailCom>;
};

export default employee_AuthDetailCom;
