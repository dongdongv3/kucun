import Employee_AccountAndPasswordAuth1DeleteCom from '@/components/EmployeeGroup/Employee_AccountAndPasswordAuth1DeleteCom';
import { useLocation } from 'umi';

const employee_AccountAndPasswordAuth1DeleteCom = () => {
  const location = useLocation();
  return (
    <Employee_AccountAndPasswordAuth1DeleteCom
      params={location.query}
    ></Employee_AccountAndPasswordAuth1DeleteCom>
  );
};

export default employee_AccountAndPasswordAuth1DeleteCom;
