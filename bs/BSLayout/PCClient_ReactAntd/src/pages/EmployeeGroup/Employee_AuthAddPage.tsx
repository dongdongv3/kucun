import Employee_AuthAddCom from '@/components/EmployeeGroup/Employee_AuthAddCom';
import { useLocation } from 'umi';

const employee_AuthAddCom = () => {
  const location = useLocation();
  return <Employee_AuthAddCom params={location.query}></Employee_AuthAddCom>;
};

export default employee_AuthAddCom;
