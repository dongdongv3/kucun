import Employee_RoleAuthMapDeleteCom from '@/components/EmployeeGroup/Employee_RoleAuthMapDeleteCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapDeleteCom = () => {
  const location = useLocation();
  return <Employee_RoleAuthMapDeleteCom params={location.query}></Employee_RoleAuthMapDeleteCom>;
};

export default employee_RoleAuthMapDeleteCom;
