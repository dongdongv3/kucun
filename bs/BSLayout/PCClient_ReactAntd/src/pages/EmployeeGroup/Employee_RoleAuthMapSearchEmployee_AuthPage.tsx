import Employee_RoleAuthMapSearchEmployee_AuthCom from '@/components/EmployeeGroup/Employee_RoleAuthMapSearchEmployee_AuthCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapSearchEmployee_AuthCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapSearchEmployee_AuthCom
      params={location.query}
    ></Employee_RoleAuthMapSearchEmployee_AuthCom>
  );
};

export default employee_RoleAuthMapSearchEmployee_AuthCom;
