import Employee_UserRoleMapSearchEmployeeCom from '@/components/EmployeeGroup/Employee_UserRoleMapSearchEmployeeCom';
import { useLocation } from 'umi';

const employee_UserRoleMapSearchEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapSearchEmployeeCom
      params={location.query}
    ></Employee_UserRoleMapSearchEmployeeCom>
  );
};

export default employee_UserRoleMapSearchEmployeeCom;
