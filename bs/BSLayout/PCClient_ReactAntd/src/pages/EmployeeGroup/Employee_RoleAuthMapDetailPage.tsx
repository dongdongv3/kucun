import Employee_RoleAuthMapDetailCom from '@/components/EmployeeGroup/Employee_RoleAuthMapDetailCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapDetailCom = () => {
  const location = useLocation();
  return <Employee_RoleAuthMapDetailCom params={location.query}></Employee_RoleAuthMapDetailCom>;
};

export default employee_RoleAuthMapDetailCom;
