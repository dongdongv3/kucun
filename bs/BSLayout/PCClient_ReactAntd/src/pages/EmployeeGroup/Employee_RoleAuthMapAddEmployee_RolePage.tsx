import Employee_RoleAuthMapAddEmployee_RoleCom from '@/components/EmployeeGroup/Employee_RoleAuthMapAddEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapAddEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapAddEmployee_RoleCom
      params={location.query}
    ></Employee_RoleAuthMapAddEmployee_RoleCom>
  );
};

export default employee_RoleAuthMapAddEmployee_RoleCom;
