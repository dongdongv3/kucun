import Employee_AuthEditCom from '@/components/EmployeeGroup/Employee_AuthEditCom';
import { useLocation } from 'umi';

const employee_AuthEditCom = () => {
  const location = useLocation();
  return <Employee_AuthEditCom params={location.query}></Employee_AuthEditCom>;
};

export default employee_AuthEditCom;
