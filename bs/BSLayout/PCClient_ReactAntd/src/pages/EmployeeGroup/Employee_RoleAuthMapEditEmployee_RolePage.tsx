import Employee_RoleAuthMapEditEmployee_RoleCom from '@/components/EmployeeGroup/Employee_RoleAuthMapEditEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapEditEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapEditEmployee_RoleCom
      params={location.query}
    ></Employee_RoleAuthMapEditEmployee_RoleCom>
  );
};

export default employee_RoleAuthMapEditEmployee_RoleCom;
