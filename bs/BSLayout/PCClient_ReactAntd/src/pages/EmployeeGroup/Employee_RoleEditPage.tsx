import Employee_RoleEditCom from '@/components/EmployeeGroup/Employee_RoleEditCom';
import { useLocation } from 'umi';

const employee_RoleEditCom = () => {
  const location = useLocation();
  return <Employee_RoleEditCom params={location.query}></Employee_RoleEditCom>;
};

export default employee_RoleEditCom;
