import EmployeeSearchCom from '@/components/EmployeeGroup/EmployeeSearchCom';
import { useLocation } from 'umi';

const employeeSearchCom = () => {
  const location = useLocation();
  return <EmployeeSearchCom params={location.query}></EmployeeSearchCom>;
};

export default employeeSearchCom;
