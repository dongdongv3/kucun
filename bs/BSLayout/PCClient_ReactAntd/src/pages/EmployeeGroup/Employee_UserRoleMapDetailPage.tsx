import Employee_UserRoleMapDetailCom from '@/components/EmployeeGroup/Employee_UserRoleMapDetailCom';
import { useLocation } from 'umi';

const employee_UserRoleMapDetailCom = () => {
  const location = useLocation();
  return <Employee_UserRoleMapDetailCom params={location.query}></Employee_UserRoleMapDetailCom>;
};

export default employee_UserRoleMapDetailCom;
