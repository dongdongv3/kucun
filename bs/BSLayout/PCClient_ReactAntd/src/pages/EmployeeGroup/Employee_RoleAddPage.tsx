import Employee_RoleAddCom from '@/components/EmployeeGroup/Employee_RoleAddCom';
import { useLocation } from 'umi';

const employee_RoleAddCom = () => {
  const location = useLocation();
  return <Employee_RoleAddCom params={location.query}></Employee_RoleAddCom>;
};

export default employee_RoleAddCom;
