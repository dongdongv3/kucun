import Employee_UserRoleMapAddEmployeeCom from '@/components/EmployeeGroup/Employee_UserRoleMapAddEmployeeCom';
import { useLocation } from 'umi';

const employee_UserRoleMapAddEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapAddEmployeeCom
      params={location.query}
    ></Employee_UserRoleMapAddEmployeeCom>
  );
};

export default employee_UserRoleMapAddEmployeeCom;
