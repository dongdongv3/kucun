import Employee_UserRoleMapEditEmployeeCom from '@/components/EmployeeGroup/Employee_UserRoleMapEditEmployeeCom';
import { useLocation } from 'umi';

const employee_UserRoleMapEditEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapEditEmployeeCom
      params={location.query}
    ></Employee_UserRoleMapEditEmployeeCom>
  );
};

export default employee_UserRoleMapEditEmployeeCom;
