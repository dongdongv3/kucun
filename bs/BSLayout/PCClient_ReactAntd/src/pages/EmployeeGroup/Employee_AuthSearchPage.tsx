import Employee_AuthSearchCom from '@/components/EmployeeGroup/Employee_AuthSearchCom';
import { useLocation } from 'umi';

const employee_AuthSearchCom = () => {
  const location = useLocation();
  return <Employee_AuthSearchCom params={location.query}></Employee_AuthSearchCom>;
};

export default employee_AuthSearchCom;
