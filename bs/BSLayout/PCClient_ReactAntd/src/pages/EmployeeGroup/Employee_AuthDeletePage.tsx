import Employee_AuthDeleteCom from '@/components/EmployeeGroup/Employee_AuthDeleteCom';
import { useLocation } from 'umi';

const employee_AuthDeleteCom = () => {
  const location = useLocation();
  return <Employee_AuthDeleteCom params={location.query}></Employee_AuthDeleteCom>;
};

export default employee_AuthDeleteCom;
