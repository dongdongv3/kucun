import EmployeeEditCom from '@/components/EmployeeGroup/EmployeeEditCom';
import { useLocation } from 'umi';

const employeeEditCom = () => {
  const location = useLocation();
  return <EmployeeEditCom params={location.query}></EmployeeEditCom>;
};

export default employeeEditCom;
