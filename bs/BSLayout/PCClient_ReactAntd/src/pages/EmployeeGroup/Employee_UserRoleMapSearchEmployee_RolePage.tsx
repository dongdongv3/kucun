import Employee_UserRoleMapSearchEmployee_RoleCom from '@/components/EmployeeGroup/Employee_UserRoleMapSearchEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_UserRoleMapSearchEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapSearchEmployee_RoleCom
      params={location.query}
    ></Employee_UserRoleMapSearchEmployee_RoleCom>
  );
};

export default employee_UserRoleMapSearchEmployee_RoleCom;
