import Employee_AccountAndPasswordAuth1AddEmployeeCom from '@/components/EmployeeGroup/Employee_AccountAndPasswordAuth1AddEmployeeCom';
import { useLocation } from 'umi';

const employee_AccountAndPasswordAuth1AddEmployeeCom = () => {
  const location = useLocation();
  return (
    <Employee_AccountAndPasswordAuth1AddEmployeeCom
      params={location.query}
    ></Employee_AccountAndPasswordAuth1AddEmployeeCom>
  );
};

export default employee_AccountAndPasswordAuth1AddEmployeeCom;
