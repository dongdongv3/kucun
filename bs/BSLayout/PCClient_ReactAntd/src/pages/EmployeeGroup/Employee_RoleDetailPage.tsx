import Employee_RoleDetailCom from '@/components/EmployeeGroup/Employee_RoleDetailCom';
import { useLocation } from 'umi';

const employee_RoleDetailCom = () => {
  const location = useLocation();
  return <Employee_RoleDetailCom params={location.query}></Employee_RoleDetailCom>;
};

export default employee_RoleDetailCom;
