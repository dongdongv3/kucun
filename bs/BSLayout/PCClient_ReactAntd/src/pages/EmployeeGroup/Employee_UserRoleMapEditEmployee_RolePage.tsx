import Employee_UserRoleMapEditEmployee_RoleCom from '@/components/EmployeeGroup/Employee_UserRoleMapEditEmployee_RoleCom';
import { useLocation } from 'umi';

const employee_UserRoleMapEditEmployee_RoleCom = () => {
  const location = useLocation();
  return (
    <Employee_UserRoleMapEditEmployee_RoleCom
      params={location.query}
    ></Employee_UserRoleMapEditEmployee_RoleCom>
  );
};

export default employee_UserRoleMapEditEmployee_RoleCom;
