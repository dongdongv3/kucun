import Employee_RoleAuthMapEditEmployee_AuthCom from '@/components/EmployeeGroup/Employee_RoleAuthMapEditEmployee_AuthCom';
import { useLocation } from 'umi';

const employee_RoleAuthMapEditEmployee_AuthCom = () => {
  const location = useLocation();
  return (
    <Employee_RoleAuthMapEditEmployee_AuthCom
      params={location.query}
    ></Employee_RoleAuthMapEditEmployee_AuthCom>
  );
};

export default employee_RoleAuthMapEditEmployee_AuthCom;
