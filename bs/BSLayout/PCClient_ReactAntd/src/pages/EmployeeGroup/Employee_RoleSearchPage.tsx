import Employee_RoleSearchCom from '@/components/EmployeeGroup/Employee_RoleSearchCom';
import { useLocation } from 'umi';

const employee_RoleSearchCom = () => {
  const location = useLocation();
  return <Employee_RoleSearchCom params={location.query}></Employee_RoleSearchCom>;
};

export default employee_RoleSearchCom;
