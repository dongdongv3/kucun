import Employee_AccountAndPasswordAuth1DetailCom from '@/components/EmployeeGroup/Employee_AccountAndPasswordAuth1DetailCom';
import { useLocation } from 'umi';

const employee_AccountAndPasswordAuth1DetailCom = () => {
  const location = useLocation();
  return (
    <Employee_AccountAndPasswordAuth1DetailCom
      params={location.query}
    ></Employee_AccountAndPasswordAuth1DetailCom>
  );
};

export default employee_AccountAndPasswordAuth1DetailCom;
