import InTypeSearchCom from '@/components/InTypeGroup/InTypeSearchCom';
import { useLocation } from 'umi';

const inTypeSearchCom = () => {
  const location = useLocation();
  return <InTypeSearchCom params={location.query}></InTypeSearchCom>;
};

export default inTypeSearchCom;
