import InTypeAddCom from '@/components/InTypeGroup/InTypeAddCom';
import { useLocation } from 'umi';

const inTypeAddCom = () => {
  const location = useLocation();
  return <InTypeAddCom params={location.query}></InTypeAddCom>;
};

export default inTypeAddCom;
