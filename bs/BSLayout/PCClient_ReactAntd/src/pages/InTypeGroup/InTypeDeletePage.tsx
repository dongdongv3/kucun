import InTypeDeleteCom from '@/components/InTypeGroup/InTypeDeleteCom';
import { useLocation } from 'umi';

const inTypeDeleteCom = () => {
  const location = useLocation();
  return <InTypeDeleteCom params={location.query}></InTypeDeleteCom>;
};

export default inTypeDeleteCom;
