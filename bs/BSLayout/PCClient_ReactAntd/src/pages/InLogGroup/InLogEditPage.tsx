import InLogEditCom from '@/components/InLogGroup/InLogEditCom';
import { useLocation } from 'umi';

const inLogEditCom = () => {
  const location = useLocation();
  return <InLogEditCom params={location.query}></InLogEditCom>;
};

export default inLogEditCom;
