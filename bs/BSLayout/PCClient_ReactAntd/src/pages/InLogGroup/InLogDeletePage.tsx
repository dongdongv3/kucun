import InLogDeleteCom from '@/components/InLogGroup/InLogDeleteCom';
import { useLocation } from 'umi';

const inLogDeleteCom = () => {
  const location = useLocation();
  return <InLogDeleteCom params={location.query}></InLogDeleteCom>;
};

export default inLogDeleteCom;
