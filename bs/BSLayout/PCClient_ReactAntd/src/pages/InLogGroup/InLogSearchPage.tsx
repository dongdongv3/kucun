import InLogSearchCom from '@/components/InLogGroup/InLogSearchCom';
import { useLocation } from 'umi';

const inLogSearchCom = () => {
  const location = useLocation();
  return <InLogSearchCom params={location.query}></InLogSearchCom>;
};

export default inLogSearchCom;
