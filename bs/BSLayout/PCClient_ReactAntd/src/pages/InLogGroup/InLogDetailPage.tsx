import InLogDetailCom from '@/components/InLogGroup/InLogDetailCom';
import { useLocation } from 'umi';

const inLogDetailCom = () => {
  const location = useLocation();
  return <InLogDetailCom params={location.query}></InLogDetailCom>;
};

export default inLogDetailCom;
