import InLogAddCom from '@/components/InLogGroup/InLogAddCom';
import { useLocation } from 'umi';

const inLogAddCom = () => {
  const location = useLocation();
  return <InLogAddCom params={location.query}></InLogAddCom>;
};

export default inLogAddCom;
