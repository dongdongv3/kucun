import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const EmployeeEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await employeeGroupServiceApi.employeeEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await employeeGroupServiceApi.employeeEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText name={'id'} label={'Id'} rules={[{ type: 'string', max: 50 }]}></ProFormText>
      <ProFormText
        name={'displayName'}
        label={'显示名'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
    </ProForm>
  );
};

export default ResoultWapper(EmployeeEdit, '编辑员工-用户信息');
