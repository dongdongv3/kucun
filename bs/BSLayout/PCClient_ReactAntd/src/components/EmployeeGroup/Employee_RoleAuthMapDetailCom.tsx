import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const Employee_RoleAuthMapDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await employeeGroupServiceApi.employee_RoleAuthMapDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          employeeGroupServiceApi.employee_RoleAuthMapDetail_RefEmployee_RoleId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'角色-角色名称'}
        dataIndex={'refEmployee_RoleId'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          employeeGroupServiceApi.employee_RoleAuthMapDetail_RefEmployee_AuthId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'权限-权限名称'}
        dataIndex={'refEmployee_AuthId'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(Employee_RoleAuthMapDetail, '详情角色权限映射表');
