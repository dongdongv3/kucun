import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    refEmployeeId: any;
  };
  [property: string]: any;
}

const Employee_UserRoleMapAddEmployee: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await employeeGroupServiceApi.employee_UserRoleMapAddEmployee_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await employeeGroupServiceApi.employee_UserRoleMapAddEmployee(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormSelect
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapAddEmployee_RefEmployee_RoleId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refEmployee_RoleId'}
        label={'角色-角色名称'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(Employee_UserRoleMapAddEmployee, '为员工-用户信息添加用户角色映射表');
