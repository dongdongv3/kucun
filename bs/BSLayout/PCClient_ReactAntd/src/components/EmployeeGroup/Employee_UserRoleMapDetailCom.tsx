import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const Employee_UserRoleMapDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await employeeGroupServiceApi.employee_UserRoleMapDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapDetail_RefEmployeeId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'员工-用户信息-显示名'}
        dataIndex={'refEmployeeId'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapDetail_RefEmployee_RoleId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'角色-角色名称'}
        dataIndex={'refEmployee_RoleId'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(Employee_UserRoleMapDetail, '详情用户角色映射表');
