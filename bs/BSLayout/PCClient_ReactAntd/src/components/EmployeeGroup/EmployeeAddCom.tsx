import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const EmployeeAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await employeeGroupServiceApi.employeeAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await employeeGroupServiceApi.employeeAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText name={'id'} label={'Id'} rules={[{ type: 'string', max: 50 }]}></ProFormText>
      <ProFormText
        name={'displayName'}
        label={'显示名'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
    </ProForm>
  );
};

export default ResoultWapper(EmployeeAdd, '添加员工-用户信息');
