import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    refEmployee_RoleId: any;
  };
  [property: string]: any;
}

const Employee_UserRoleMapAddEmployee_Role: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata =
      await employeeGroupServiceApi.employee_UserRoleMapAddEmployee_Role_GetDefaultValue({
        pageParament: params,
      });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await employeeGroupServiceApi.employee_UserRoleMapAddEmployee_Role(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormSelect
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapAddEmployee_Role_RefEmployeeId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refEmployeeId'}
        label={'员工-用户信息-显示名'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(Employee_UserRoleMapAddEmployee_Role, '为角色添加用户角色映射表');
