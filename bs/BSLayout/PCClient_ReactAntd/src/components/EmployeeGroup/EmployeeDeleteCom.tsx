import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { useState } from 'react';
import { Alert } from 'antd';
import { Button } from 'antd';
import { Space } from 'antd';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const EmployeeDelete: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [isloading, setIsloading] = useState(false);

  const deleteValue = async (pageparams: any) => {
    setIsloading(true);
    setResoult(undefined);
    const tmp = await employeeGroupServiceApi
      .employeeDelete({ ...params, pageParament: params })
      .finally(() => setIsloading(false));
    setResoult(tmp);
  };

  const onCancel = () => {
    if (history) history.go(-1);
  };

  return (
    <Space direction="vertical" style={{ display: 'flex', textAlign: 'center' }}>
      <Alert message="确认删除?" type="warning" />
      <Space>
        <Button type="primary" danger loading={isloading} onClick={deleteValue}>
          确认删除
        </Button>
        <Button onClick={onCancel}>取消</Button>
      </Space>
    </Space>
  );
};

export default ResoultWapper(EmployeeDelete, '删除员工-用户信息');
