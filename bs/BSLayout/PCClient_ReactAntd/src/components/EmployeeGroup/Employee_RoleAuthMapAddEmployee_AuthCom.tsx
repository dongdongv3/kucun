import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    refEmployee_AuthId: any;
  };
  [property: string]: any;
}

const Employee_RoleAuthMapAddEmployee_Auth: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata =
      await employeeGroupServiceApi.employee_RoleAuthMapAddEmployee_Auth_GetDefaultValue({
        pageParament: params,
      });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await employeeGroupServiceApi.employee_RoleAuthMapAddEmployee_Auth(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormSelect
        request={() =>
          employeeGroupServiceApi.employee_RoleAuthMapAddEmployee_Auth_RefEmployee_RoleId_DataSource(
            { ...(formref.current?.getFieldsValue() || {}), pageParament: params },
          )
        }
        dependencies={[]}
        name={'refEmployee_RoleId'}
        label={'角色-角色名称'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(Employee_RoleAuthMapAddEmployee_Auth, '为权限添加角色权限映射表');
