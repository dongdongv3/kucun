import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    refEmployeeId: any;
  };
  [property: string]: any;
}

const Employee_AccountAndPasswordAuth1AddEmployee: React.FC<inputProperty> = ({
  params,
  setResoult,
}) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata =
      await employeeGroupServiceApi.employee_AccountAndPasswordAuth1AddEmployee_GetDefaultValue({
        pageParament: params,
      });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await employeeGroupServiceApi.employee_AccountAndPasswordAuth1AddEmployee(
      convertData,
    );
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText
        name={'loginID'}
        label={'登录账户'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText.Password name={'pwd'} label={'密码'} rules={[]}></ProFormText.Password>
    </ProForm>
  );
};

export default ResoultWapper(
  Employee_AccountAndPasswordAuth1AddEmployee,
  '为员工-用户信息添加账户密码认证1-认证通道',
);
