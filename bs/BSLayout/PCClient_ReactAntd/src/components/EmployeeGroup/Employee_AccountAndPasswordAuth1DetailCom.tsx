import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const Employee_AccountAndPasswordAuth1Detail: React.FC<inputProperty> = ({
  params,
  setResoult,
}) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await employeeGroupServiceApi.employee_AccountAndPasswordAuth1Detail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'登录账户'} dataIndex={'loginID'} />
      <ProDescriptions.Item valueType={'password'} title={'密码'} dataIndex={'pwd'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          employeeGroupServiceApi.employee_AccountAndPasswordAuth1Detail_RefEmployeeId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'员工-用户信息-显示名'}
        dataIndex={'refEmployeeId'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(Employee_AccountAndPasswordAuth1Detail, '详情账户密码认证1-认证通道');
