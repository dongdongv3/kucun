import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params: {
    refEmployee_RoleId: any;
  };
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    employeeGroupServiceApi
      .employee_RoleAuthMapSearchEmployee_Role_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const employee_RoleAuthMapSearchEmployee_Role = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await employeeGroupServiceApi.employee_RoleAuthMapSearchEmployee_Role(
      convertData,
      sort,
      filter,
    );
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'employee_RoleAuthMap_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      employeeGroupServiceApi.employee_RoleAuthMapSearchEmployee_Role_RefEmployee_RoleId_View_DataSource(
        { ...{}, pageParament: params },
      ),
    title: '角色-角色名称',
    dataIndex: 'employee_RoleAuthMap_RefEmployee_RoleId',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      employeeGroupServiceApi.employee_RoleAuthMapSearchEmployee_Role_RefEmployee_AuthId_View_DataSource(
        { ...{}, pageParament: params },
      ),
    title: '权限-权限名称',
    dataIndex: 'employee_RoleAuthMap_RefEmployee_AuthId',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="Employee_RoleAuthMapEditEmployee_Role"
        buttonProp={{ type: 'primary' }}
        path="/EmployeeGroup/Employee_RoleAuthMapEditEmployee_RolePage"
        query={{ id: record.employee_RoleAuthMap_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="Employee_RoleAuthMapDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/EmployeeGroup/Employee_RoleAuthMapDeletePage"
        query={{ id: record.employee_RoleAuthMap_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="Employee_RoleAuthMapDetail"
        buttonProp={{ type: 'link' }}
        path="/EmployeeGroup/Employee_RoleAuthMapDetailPage"
        query={{ id: record.employee_RoleAuthMap_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="根据角色查询角色权限映射表">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={employee_RoleAuthMapSearchEmployee_Role}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="Employee_RoleAuthMapAddEmployee_Role"
            buttonProp={{ type: 'primary' }}
            path="/EmployeeGroup/Employee_RoleAuthMapAddEmployee_RolePage"
            query={{ refEmployee_RoleId: params.refEmployee_RoleId }}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
