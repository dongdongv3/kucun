import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
    refEmployee_RoleId: any;
  };
  [property: string]: any;
}

const Employee_UserRoleMapEditEmployee_Role: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await employeeGroupServiceApi.employee_UserRoleMapEditEmployee_Role_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await employeeGroupServiceApi.employee_UserRoleMapEditEmployee_Role({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapEditEmployee_Role_RefEmployeeId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refEmployeeId'}
        label={'员工-用户信息-显示名'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          employeeGroupServiceApi.employee_UserRoleMapEditEmployee_Role_RefEmployee_RoleId_DataSource(
            { ...(formref.current?.getFieldsValue() || {}), pageParament: params },
          )
        }
        dependencies={[]}
        name={'refEmployee_RoleId'}
        label={'角色-角色名称'}
        readonly={true}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(Employee_UserRoleMapEditEmployee_Role, '为角色编辑用户角色映射表');
