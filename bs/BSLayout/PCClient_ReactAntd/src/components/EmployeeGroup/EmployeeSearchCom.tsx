import * as employeeGroupServiceApi from '@/services/EmployeeGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { SubnodeOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    employeeGroupServiceApi
      .employeeSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const employeeSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await employeeGroupServiceApi.employeeSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '显示名',
    dataIndex: 'employee_DisplayName',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'employee_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="EmployeeEdit"
        buttonProp={{ type: 'primary' }}
        path="/EmployeeGroup/EmployeeEditPage"
        query={{ id: record.employee_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="EmployeeDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/EmployeeGroup/EmployeeDeletePage"
        query={{ id: record.employee_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="EmployeeDetail"
        buttonProp={{ type: 'link' }}
        path="/EmployeeGroup/EmployeeDetailPage"
        query={{ id: record.employee_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
      <LinkTo
        key="Employee_AccountAndPasswordAuth1SearchEmployee"
        buttonProp={{ type: 'link' }}
        path="/EmployeeGroup/Employee_AccountAndPasswordAuth1SearchEmployeePage"
        query={{ refEmployeeId: record.employee_Id }}
        icon={<SubnodeOutlined />}
        successGoto={1}
      >
        账户密码认证1-认证通道管理
      </LinkTo>,
      <LinkTo
        key="Employee_UserRoleMapSearchEmployee"
        buttonProp={{ type: 'link' }}
        path="/EmployeeGroup/Employee_UserRoleMapSearchEmployeePage"
        query={{ refEmployeeId: record.employee_Id }}
        icon={<SubnodeOutlined />}
        successGoto={1}
      >
        用户角色映射表管理
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="查询员工-用户信息">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={employeeSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="EmployeeAdd"
            buttonProp={{ type: 'primary' }}
            path="/EmployeeGroup/EmployeeAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
