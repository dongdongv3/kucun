import * as inLogGroupServiceApi from '@/services/InLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDateTimePicker } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const InLogEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await inLogGroupServiceApi.inLogEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await inLogGroupServiceApi.inLogEdit({ ...convertData, pageParament: params });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormDateTimePicker
        name={'createTime'}
        label={'入库时间'}
        rules={[]}
      ></ProFormDateTimePicker>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogEdit_goods_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'goods'}
        label={'商品'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogEdit_BinLocation_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        fieldProps={{ mode: 'multiple' }}
        dependencies={[]}
        name={'binLocation'}
        label={'库位'}
        rules={[]}
      ></ProFormSelect>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'数量'}
        rules={[{ min: 0, max: 10000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogEdit_InTypeInfo_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'inTypeInfo'}
        label={'类型'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(InLogEdit, '编辑入库记录');
