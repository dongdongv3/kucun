import * as inLogGroupServiceApi from '@/services/InLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormDateTimePicker } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const InLogAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await inLogGroupServiceApi.inLogAdd_GetDefaultValue({ pageParament: params });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await inLogGroupServiceApi.inLogAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormDateTimePicker
        name={'createTime'}
        label={'入库时间'}
        rules={[]}
      ></ProFormDateTimePicker>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogAdd_goods_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'goods'}
        label={'商品'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogAdd_BinLocation_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        fieldProps={{ mode: 'multiple' }}
        dependencies={[]}
        name={'binLocation'}
        label={'库位'}
        rules={[]}
      ></ProFormSelect>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'数量'}
        rules={[{ min: 0, max: 10000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          inLogGroupServiceApi.inLogAdd_InTypeInfo_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'inTypeInfo'}
        label={'类型'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(InLogAdd, '添加入库记录');
