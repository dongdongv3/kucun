import * as inLogGroupServiceApi from '@/services/InLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const InLogDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await inLogGroupServiceApi.inLogDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'dateTime'} title={'入库时间'} dataIndex={'createTime'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          inLogGroupServiceApi.inLogDetail_goods_DataSource({ ...{}, pageParament: params })
        }
        title={'商品'}
        dataIndex={'goods'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          inLogGroupServiceApi.inLogDetail_BinLocation_DataSource({ ...{}, pageParament: params })
        }
        fieldProps={{ mode: 'multiple' }}
        title={'库位'}
        dataIndex={'binLocation'}
      />
      <ProDescriptions.Item valueType={'digit'} title={'数量'} dataIndex={'count'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          inLogGroupServiceApi.inLogDetail_InTypeInfo_DataSource({ ...{}, pageParament: params })
        }
        title={'类型'}
        dataIndex={'inTypeInfo'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(InLogDetail, '详情入库记录');
