import * as goodsGroupServiceApi from '@/services/GoodsGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const GoodsAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await goodsGroupServiceApi.goodsAdd_GetDefaultValue({ pageParament: params });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await goodsGroupServiceApi.goodsAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText name={'name'} label={'名称'} rules={[{ type: 'string', max: 50 }]}></ProFormText>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'库存数量'}
        rules={[{ min: 0, max: 1000000, type: 'number' }]}
      ></ProFormDigit>
    </ProForm>
  );
};

export default ResoultWapper(GoodsAdd, '添加商品');
