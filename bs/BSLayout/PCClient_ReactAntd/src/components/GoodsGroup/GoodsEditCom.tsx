import * as goodsGroupServiceApi from '@/services/GoodsGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const GoodsEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await goodsGroupServiceApi.goodsEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await goodsGroupServiceApi.goodsEdit({ ...convertData, pageParament: params });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText name={'name'} label={'名称'} rules={[{ type: 'string', max: 50 }]}></ProFormText>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'库存数量'}
        rules={[{ min: 0, max: 1000000, type: 'number' }]}
      ></ProFormDigit>
    </ProForm>
  );
};

export default ResoultWapper(GoodsEdit, '编辑商品');
