import * as outLogGroupServiceApi from '@/services/OutLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const OutLogDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await outLogGroupServiceApi.outLogDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'dateTime'} title={'出库时间'} dataIndex={'createtime'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          outLogGroupServiceApi.outLogDetail_goods_DataSource({ ...{}, pageParament: params })
        }
        title={'商品'}
        dataIndex={'goods'}
      />
      <ProDescriptions.Item valueType={'digit'} title={'出库数量'} dataIndex={'count'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          outLogGroupServiceApi.outLogDetail_OutTypeInfo_DataSource({ ...{}, pageParament: params })
        }
        title={'出库类型'}
        dataIndex={'outTypeInfo'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          outLogGroupServiceApi.outLogDetail_BinLocation_DataSource({ ...{}, pageParament: params })
        }
        fieldProps={{ mode: 'multiple' }}
        title={'库位'}
        dataIndex={'binLocation'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(OutLogDetail, '详情出库记录');
