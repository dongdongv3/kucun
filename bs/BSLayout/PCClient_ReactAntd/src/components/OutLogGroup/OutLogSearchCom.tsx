import * as outLogGroupServiceApi from '@/services/OutLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    outLogGroupServiceApi.outLogSearch_GetDefaultValue({ pageParament: params }).then((data) => {
      const convertData = { ...data };
      setdefaultvalue(convertData);

      if (formref?.current) {
        formref?.current?.setFieldsValue(convertData);
        formref.current?.submit();
      } else {
        setTimeout(() => {
          ref.current?.reload();
        }, 100);
      }
    });
  }, [params]);

  const outLogSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await outLogGroupServiceApi.outLogSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'dateTime',
    title: '出库时间',
    dataIndex: 'outLog_Createtime',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      outLogGroupServiceApi.outLogSearch_goods_View_DataSource({ ...{}, pageParament: params }),
    title: '商品',
    dataIndex: 'outLog_Goods',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'digit',
    title: '出库数量',
    dataIndex: 'outLog_Count',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      outLogGroupServiceApi.outLogSearch_OutTypeInfo_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '出库类型',
    dataIndex: 'outLog_OutTypeInfo',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      outLogGroupServiceApi.outLogSearch_BinLocation_View_DataSource({
        ...{},
        pageParament: params,
      }),
    fieldProps: { mode: 'multiple' },
    title: '库位',
    dataIndex: 'outLog_BinLocation',
    hideInSearch: true,
    hideInForm: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'outLog_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="OutLogEdit"
        buttonProp={{ type: 'primary' }}
        path="/OutLogGroup/OutLogEditPage"
        query={{ id: record.outLog_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="OutLogDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/OutLogGroup/OutLogDeletePage"
        query={{ id: record.outLog_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="OutLogDetail"
        buttonProp={{ type: 'link' }}
        path="/OutLogGroup/OutLogDetailPage"
        query={{ id: record.outLog_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="查询出库记录">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={outLogSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="OutLogAdd"
            buttonProp={{ type: 'primary' }}
            path="/OutLogGroup/OutLogAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
