import * as outLogGroupServiceApi from '@/services/OutLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormDateTimePicker } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const OutLogAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await outLogGroupServiceApi.outLogAdd_GetDefaultValue({ pageParament: params });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await outLogGroupServiceApi.outLogAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormDateTimePicker
        name={'createtime'}
        label={'出库时间'}
        rules={[]}
      ></ProFormDateTimePicker>
      <ProFormSelect
        request={() =>
          outLogGroupServiceApi.outLogAdd_goods_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'goods'}
        label={'商品'}
        rules={[]}
      ></ProFormSelect>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'出库数量'}
        rules={[{ min: 0, max: 1000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          outLogGroupServiceApi.outLogAdd_OutTypeInfo_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'outTypeInfo'}
        label={'出库类型'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          outLogGroupServiceApi.outLogAdd_BinLocation_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        fieldProps={{ mode: 'multiple' }}
        dependencies={[]}
        name={'binLocation'}
        label={'库位'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(OutLogAdd, '添加出库记录');
