import { access_token } from '../config/oidc';
import { Link, RequestConfig, RunTimeLayoutConfig } from 'umi';
import { mgr } from '../config/oidc';
import { PageLoading } from '@ant-design/pro-layout';
import { Settings as LayoutSettings } from '@ant-design/pro-layout';
import { currentUser as queryCurrentUser } from './services/ant-design-pro/api';
import RightContent from './components/RightContent';
import { Modal, Button } from 'antd';
import React from 'react';
import UnAccessiblePage from './pages/403';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */
export async function getInitialState(): Promise<{
  settings?: Partial<LayoutSettings>;
  currentUser?: { displayName: string };
  fetchUserInfo?: () => Promise<{ displayName: string }>;
  isLogin: boolean;
}> {
  const defaultUser = { userID: '', userName: '', roles: {}, auths: {} };
  const fetchUserInfo = async () => {
    const user = await mgr.getUser();
    if (user) {
      const output = await queryCurrentUser();
      return output.data;
    }
    return defaultUser;
  };
  const tmpuser = await fetchUserInfo();
  console.log(tmpuser);
  const isLogin = tmpuser != defaultUser;

  return {
    fetchUserInfo,
    currentUser: { ...tmpuser, displayName: tmpuser.displayName },
    settings: {},
    isLogin: isLogin,
  };
}

const authHeaderInterceptor = (url: string, options: any) => {
  const authHeader = { Authorization: 'Bearer ' + access_token };
  return {
    url: `${url}`,
    options: { ...options, interceptors: true, headers: authHeader },
  };
};

const errorCode = {
  400: {
    title: '输入的值无效',
    message: async (response: Response) => {
      const data = await response.clone().json();
      return React.createElement('div', {
        dangerouslySetInnerHTML: {
          __html: `${data.message}. ${getErrorMessage(data.data.errors)}`,
        },
      });
    },
  },
  401: {
    title: '无权访问',
    message: async (response: Response) => {
      return (
        <>
          请先登录 ,
          <Button type="link" onClick={() => mgr.signinRedirect()}>
            去登录
          </Button>
        </>
      );
    },
  },
  429: {
    title: '请求太频繁，请稍后再试',
    message: async (response: Response) => {
      return <>你的请求被流量限制器拦截，请在{await response.clone().text()}稍后重试</>;
    },
  },
  500: {
    title: '未知错误',
    message: async (response: Response) => {
      return (await response.clone().json()).message;
    },
  },
  501: {
    title: '操作失败',
    message: async (response: Response) => {
      return React.createElement('div', {
        dangerouslySetInnerHTML: { __html: (await response.clone().json()).message },
      });
    },
  },
};
const errorHeaderInterceptor = async (response: Response, options: any) => {
  const errorEntity = errorCode[response.status];
  if (errorEntity) {
    const errorMessage = await errorEntity.message(response);
    Modal.error({
      title: errorEntity.title,
      content: errorMessage,
      width: 700,
    });
  }
  return response;
};

export const request: RequestConfig = {
  // errorHandler,
  requestInterceptors: [authHeaderInterceptor],
  responseInterceptors: [errorHeaderInterceptor],
  errorConfig: {
    adaptor: (resData) => {
      // console.log("errorConfig.adaptor", resData)
      return {
        ...resData,
        success: resData.isOk,
        errorMessage: resData.message,
        showType: 0,
      };
    },
  },
};

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout: RunTimeLayoutConfig = ({ initialState }) => {
  return {
    //修补：二级图标正常显示2021.7.8
    menuItemRender: (menuItemProps, defaultDom) => {
      if (menuItemProps.isUrl || !menuItemProps.path) {
        return defaultDom;
      }
      // 支持二级菜单显示icon
      let iconEl = menuItemProps.icon;
      if (typeof iconEl === 'string') {
        iconEl = (
          <span className="anticon">
            {' '}
            <svg
              style={{
                width: '1em',
                height: '1em',
                fill: 'currentColor',
                overflow: 'hidden',
              }}
              aria-hidden="true"
            >
              <use href={'#' + menuItemProps.icon}></use>
            </svg>
          </span>
        );
      }
      return (
        <Link to={menuItemProps.path}>
          {menuItemProps.pro_layout_parentKeys &&
            menuItemProps.pro_layout_parentKeys.length > 0 &&
            iconEl}
          {defaultDom}
        </Link>
      );
    },
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    menuHeaderRender: undefined,
    unAccessible: <UnAccessiblePage></UnAccessiblePage>,
  };
};
