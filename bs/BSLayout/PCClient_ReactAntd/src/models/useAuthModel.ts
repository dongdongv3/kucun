import { useState, useCallback, useEffect } from 'react';
import { mgr } from '../../config/oidc';

//提供ids4数据读取服务
//全局用户信息从接口获取用户信息，如果没有获取到则为空，在右上角渲染登录按钮，登陆跳转到ids4登录页面
//并在权限中返回所有需要权限的菜单为false，表示无法访问
//如果登录成功则在初始化时可以加载到用户权限信息，可以正确渲染有权限的菜单和操作
//不去除页面包裹，防止因url直接访问时无法提示正确的错误信息
//页面包裹负责检测登录，如果为登录提示是否需要登录，如果登录则检测是否拥有权限，如果没有权限则提示无权信息，
//基于url的权限数据模型单独生成，用以描述每个url对权限的需求

//包裹实现逻辑
/***
 * 1 读取全局信息，判断是否拥有有效的user信息
 *
 * 2 如果没有则提示用户是否需要跳转到登录页面进行登录
 *
 * 3 如果有有效的用户信息则判断是否拥有访问权限
 *
 * 4 如果没有提示当前用户无效，且提供登出按钮用于切换账户
 *
 * 5 如果有则正确渲染页面
 *
 */
export default function useAuthModel() {
  const [user, setUser] = useState(-100 as any);
  const [error, setError] = useState(null);
  useEffect(() => {
    if (user == -100) {
      mgr
        .getUser()
        .then((data) => {
          //认证信息获取成功
          setUser(data);
        })
        .catch((data) => {
          //认证信息获取失败
          setUser(null);
          setError(data);
        });
    }
  });

  const signin = useCallback(() => {
    mgr.signinRedirect();
  }, []);

  const signout = useCallback(() => {
    mgr.signoutRedirect();
  }, []);

  return {
    user,
    signin,
    signout,
    error,
  };
}
